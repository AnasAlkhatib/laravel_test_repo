# Laravel Test App #

### Installation ###


```sh


git clone git@bitbucket.org:AnasAlkhatib/laravel_test_repo.git

```


```sh

composer install
```


```sh

cp .env.example .env
```


```sh


php artisan key:generate && php artisan jwt:secret 
```


```sh

php artisan migrate
```

```sh

 
```


* Configure your host
* Navigate to http://laraveltest.devs/api/docs for swagger docs.