var riderAppControllers = angular.module('riderAppControllers', ['riderAppServices']);

riderAppControllers.controller('LoginController', ['$scope', '$http', '$location', 'userService',
    function ($scope, $http, $location, userService) {

    $scope.login2 = function() {
        userService.login(
            $scope.email, $scope.password,
            function(response){
                $location.path('/');
            },
            function(response){
                switch (response.status) {

                    case 401:alert('Credentials Incorrect');
                    case 422:alert('Credentials Invalid');
                }
               // alert(response.statusText);
                //alert('Something went wrong with the login process. Try again later!');
            }
        );
    }

    $scope.email = '';
    $scope.password = '';


    if(userService.checkIfLoggedIn())
        $location.path('/home');

}]);

riderAppControllers.controller('SignupController', ['$scope', '$location', 'userService',
    function ($scope, $location, userService) {

       $scope.signup = function() {
        userService.signup(
            $scope.name, $scope.email, $scope.password,$scope.phone_number,$scope.device_token,
            function(response){
                alert('Great! You are now signed in! Welcome, ' + $scope.name + '!');
                $location.path('/');
            },
            function(response){
                alert('Something went wrong with the signup process. Try again later.');
            }
        );
    }

    $scope.name = '';
    $scope.email = '';
    $scope.password = '';
    $scope.phone_number = '';
    $scope.device_token='';

    //if(userService.getCurrentToken()!='') { token=userService.getCurrentToken();alert(token);}



    if(userService.checkIfLoggedIn())
        $location.path('/home');

}]);

riderAppControllers.controller('MainController', ['$scope', '$http', function ($scope, $http) {

}]);

riderAppControllers.controller('HomeController', ['$scope', '$http', 'userService', function ($scope, $http) {


    $scope.logout=function()
    {
        userService.logout();

    }

    if(!userService.checkIfLoggedIn())
        $location.path('/');


}]);
