/**
 * Created by anas on 4/20/17.
 */

// public/scripts/authController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('AuthController',function($scope, $location, $auth, toastr) {

            $scope.login = function() {

                var credentials = {
                    login: $scope.email,
                    password: $scope.password
                }


                $auth.login(credentials)
                    .then(function() {
                        toastr.success('You have successfully signed in!');
                        $location.path('/');
                    })
                    .catch(function(error) {
                        toastr.error(error.data.message, error.status);
                    });
            };
            $scope.authenticate = function(provider) {
                $auth.authenticate(provider)
                    .then(function() {
                        toastr.success('You have successfully signed in with ' + provider + '!');
                        $location.path('/');
                    })
                    .catch(function(error) {
                        if (error.message) {
                            // Satellizer promise reject error.
                            toastr.error(error.message);
                        } else if (error.data) {
                            // HTTP response error from server
                            toastr.error(error.data.message, error.status);
                        } else {
                            toastr.error(error);
                        }
                    });
            };
        });


})();