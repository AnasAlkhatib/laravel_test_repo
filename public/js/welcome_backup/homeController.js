/**
 * Created by anas on 4/20/17.
 */

// public/scripts/authController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('HomeController', HomeController);


    function HomeController($auth, $state,$rootScope) {

        if ($rootScope.authenticated==true) alert('authinicated'); else alert('not-authinicated');
        alert($rootScope.authenticated);
            //$state.go('/', {});

        var vm = this;

      //  vm.users;
        vm.error;

        // We would normally put the logout method in the same
        // spot as the login method, ideally extracted out into
        // a service. For this simpler example we'll leave it here
        vm.logout = function() {

            $auth.logout().then(function() {

                // Remove the authenticated user from local storage
                localStorage.removeItem('user');

                // Flip authenticated to false so that we no longer
                // show UI elements dependant on the user being logged in
                $rootScope.authenticated = false;

                // Remove the current user info from rootscope
                $rootScope.currentUser = null;
            });

            $state.go('/');
        }



    }

})();
