/**
 * Created by anas on 4/20/17.
 */
// public/scripts/app.js

(function() {

    'use strict';

    angular
        .module('fadApp', ['ui.router', 'satellizer','ngMaterial'])
        .config(function($stateProvider, $urlRouterProvider, $authProvider) {

            // Satellizer configuration that specifies which API
            // route the JWT should be retrieved from
            $authProvider.loginUrl = 'api/v1/auth/login';

            // Redirect to the auth state if any other states
            // are requested other than users
            $urlRouterProvider.otherwise('/');

            $stateProvider
                .state('/', {
                    url: '/',
                    templateUrl: '../views/indexView.html',
                    controller: 'IndexController as index'
                })
                .state('home', {
                    url: '/home',
                    templateUrl: '../views/homeView.html',
                    controller: 'HomeController as home'
                })
                .state('register', {
                    url: '/register',
                    templateUrl: '../views/registerView.html',
                    controller: 'RegisterController as register'
                })
                .state('auth', {
                    url: '/auth',
                    templateUrl: '../views/authView.html',
                    controller: 'AuthController as auth'
                })
                .state('users', {
                    url: '/users',
                    templateUrl: '../views/userView.html',
                    controller: 'UserController as user'
                });
        });
})();