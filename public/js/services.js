var riderAppServices = angular.module('riderAppServices', [
    'LocalStorageModule'
]);

riderAppServices.factory('userService', ['$http', 'localStorageService', function($http, localStorageService) {

    function checkIfLoggedIn() {

        if(localStorageService.get('token'))

            return true;

        else
            return false;

    }

    function signup(name, email, password,phone_number,device_token, onSuccess, onError) {

        $http.post('http://107.170.42.16/api/v1/riders/register',
            {
                name: name,
                email: email,
                password: password,
                phone_number:phone_number,
                device_token:device_token
            }).
        then(function(response) {

            localStorageService.set('token', response.data.token);
            onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function login2(email, password, onSuccess, onError){

        $http.post('http://rider.maued.dev/api/v1/register/login',
            {
                login: email,
                password: password
            }).
        then(function(response) {

            localStorageService.set('token', response.data.token);
            onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function logout(){

        $http.post('http://rider.maued.dev/api/v1/auth/logout',
            {
                token: localStorageService.get('token'),

            }).
        then(function(response) {

            localStorageService.remove('token');
            onSuccess(response);

        }, function(response) {
            localStorageService.remove('token');
            onError(response);

        });



    }

    function getCurrentToken(){ alert(localStorageService.get('token'));
        return localStorageService.get('token');
    }

    return {
        checkIfLoggedIn: checkIfLoggedIn,
        signup: signup,
        login2: login2,
        logout: logout,
        getCurrentToken: getCurrentToken
    }

}]);
