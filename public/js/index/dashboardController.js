/**
 * Created by anas on 4/20/17.
 */

// public/scripts/ArticlesController.js

(function() {

    'use strict';

    angular
        .module('fadApp'  )
        .directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function(){
                    scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
        }])
        .controller('DashboardController',['articlesService','$scope','$sce','forumsService','blogsService',
                    'classifiedsService','storeService','groupsService','$stateParams','eventsService','$state',
                    'utilitiesServices','categoriesServices','toastr','accountServices',
            function (articlesService,$scope, sce,forumsService,blogsService,classifiedsService,storeService,
                      groupsService,$stateParams,eventsService,$state,utilitiesServices,categoriesServices,toastr,accountServices) {



        $scope.mylocale = {
                    formatDate: function(date) {
                        var m = moment(date);
                        return m.isValid() ? m.format('YYYY-MM-DD') : '';
                    }
                };

        $scope.dateoptions=function(){

             $scope.myDate = new Date();

             $scope.minDate = new Date(
                 $scope.myDate.getFullYear(),
                 $scope.myDate.getMonth() - 2,
                 $scope.myDate.getDate()
             );


             $scope.isOpen = false;

         }

        $scope.trusthtml = function(text) {

            return sce.trustAsHtml(text);
        };

        $scope.seteditoroptions=function(){
            $scope.htmlEditor = '';
            // setup editor options
            $scope.editorOptions = {
                language: 'en',
                uiColor: '#ffffff'
            };
        };

        $scope.my_forumsposts=function(page)   {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
            $scope.section='forumpost';

                    $scope.myforumsposts=forumsService.getmyforumposts(5,page,
                        function(response){

                            switch (response.status)
                            {
                                case 401:{toastr.error('Credentials Incorrect'); $state.go('dashboard');}break;
                                case 200:{
                                    $scope.myforumsposts=response.data.data;
                                    $scope.pagination=response.data.meta.pagination;
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default: toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboard');
                        }
                    );
                }

        $scope.my_forumstopics=function(page)  {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    $scope.section='forum';

                    $scope.myforumtopics=forumsService.getmyforumtopics(5,page,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401:{toastr.error('Credentials Incorrect'); $state.go('dashboard');} break;
                                case 200:{
                                    $scope.myforumtopics=response.data.data;
                                    $scope.pagination=response.data.meta.pagination;
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default: toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboard');
                        }
                    );
        }

        $scope.my_articles=function(page)      {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                   $scope.section='articles';
                   $scope.myarticles=articlesService.getmyarticles('small',5,page,
                        function(response){
                            switch (response.status)
                            {
                                case 401: {toastr.error('Credentials Incorrect'); $state.go('dashboard');} break;
                                case 200:{
                                    $scope.myarticles=response.data.data;
                                    $scope.pagination=response.data.meta.pagination;
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){

                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default: toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('/');
                        }
                    );
        }

        $scope.my_blogs=function(page)      {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
            $scope.section='blogs';
                    $scope.myblogs=blogsService.getmyblogs('small',5,page,
                        function(response){
                            switch (response.status)
                            {
                                case 401:{toastr.error('Credentials Incorrect'); $state.go('dashboard');} break;
                                case 200:{
                                    $scope.myblogs=response.data.data;
                                    $scope.pagination=response.data.meta.pagination;
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default: toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboard');
                        }
                    );
       }

        $scope.my_classifieds=function(page)    {
            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
            $scope.section='classifieds';

                    $scope.myclassifieds=classifiedsService.getmyclassifieds('small',4,page,
                        function(response){
                            switch (response.status) {

                                case 401:{toastr.error('Credentials Incorrect'); $state.go('dashboard');} break;
                                case 200:{
                                    $scope.myclassifieds=response.data.data;
                                    $scope.pagination=response.data.meta.pagination;

                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboard');
                        }
                    );
        }

        $scope.my_products=function(page)     {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
            $scope.section='products';

                    $scope.myproducts=storeService.getmyproducts('small',9,page,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401:{toastr.error('Credentials Incorrect'); $state.go('dashboard');} break;
                                case 200: {
                                    $scope.myproducts = response.data.data;
                                    $scope.pagination = response.data.meta.pagination;
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                         },
                            function (response) {
                                switch (response.status)
                                {
                                    case 401: toastr.error('Credentials Incorrect'); break;
                                    case 422: toastr.error('Not Found'); break;
                                    default : toastr.error('Server Error Check Again please !'); break;
                                }
                                angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                                $state.go('dashboard');
                            }

                            );

        }

        $scope.my_events=function(page)      {
            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
            $scope.section='events';

                    $scope.myevents=eventsService.getmyevents('small',9,page,
                        function(response){

                            switch (response.status)
                            {
                                case 401:{toastr.error('Credentials Incorrect'); $state.go('dashboard');}break;
                                case 200:{
                                    $scope.myevents  =response.data.data;
                                    $scope.pagination=response.data.meta.pagination;
                                         }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboard');
                        }

                    );
        }

        $scope.my_groups=function(page)     {
            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
            $scope.section='groups';

                    $scope.mygroups=groupsService.getmygroups('small',9,page,
                        function(response){
                            switch (response.status)
                            {
                                case 401:{toastr.error('Credentials Incorrect'); $state.go('dashboard');}break;
                                case 200:{
                                    $scope.mygroups=response.data.data;
                                    $scope.pagination=response.data.meta.pagination;
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboard');
                        }
                    );
         }

        $scope.getarticles = function () {

            // $scope.post='sss';
            $scope.articles=articlesService.getarticles('big',8,0,
                function(response){
                    switch (response.status) {

                        case 401:alert('Credentials Incorrect');break;
                        case 200:{
                            $scope.articles=response.data.data;
                            $scope.pagination=response.data.meta.pagination;
                            angular.element(document).find("#containerpart").removeClass( "spinner" );
                            // alert('Great! Data Loaded');
                            // console.log(response.data.data);
                        }break;

                    }


                }
            );


        }

        $scope.deletearticle = function (id) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    articlesService.deletearticle(id,
                        function(response){

                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:{
                                  //  response.data.data;
                                    toastr.success('article deleted');
                                    angular.element(document).find("#article_"+id).remove();


                                }break;

                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        }
                    );


                }

        $scope.publisharticle = function (id,status) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    articlesService.publisharticle(id,status,
                        function(response){

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('article updated');
                                }break;

                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboardsection',{ section:'articles',page:1 });
                        }
                    );


                }

        $scope.showarticleform = function ()    {
            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

            $scope.htmlEditor = '.asd asd asd ..';
                    // setup editor options
            $scope.editorOptions = {
                        uiColor: '#ffffff'
                    };

            $scope.section='addarticle';
            categoriesServices.getcategories('articles',40,1,
                function(response)
                {
                    switch (response.status)
                    {
                        case 401:toastr.error('Credentials Incorrect');break;
                        case 202:toastr.error('Error Happened');break;
                        case 200:{
                            $scope.articlescategores=response.data.data;
                            $scope.articlescategorespagination=response.data.meta.pagination;

                            var categories_list=[];
                            var main_cat=0;
                            var j=0;
                            var i=0;

                            for(i=0;i<$scope.articlescategores.length;i++)
                            {

                              var category =
                                  {

                                    main_id: '',
                                    main_title:'',
                                  }


                              if(main_cat!=$scope.articlescategores[i]['main_id'])
                               {
                                   var category_main= {

                                       main_id:$scope.articlescategores[i]['main_id'],
                                       main_title:$scope.articlescategores[i]['main_title'],

                                   }

                                   categories_list[j]=category_main;
                                   j++;

                                   if($scope.articlescategores[i]['second_id']!=null) {
                                       category.main_id = $scope.articlescategores[i]['second_id'];
                                       category.main_title = '--' + $scope.articlescategores[i]['second_title'];
                                       categories_list[j] = category;
                                       j++;
                                   }

                                   main_cat=$scope.articlescategores[i]['main_id'];

                               }
                              else
                               {

                                   category.main_id=$scope.articlescategores[i]['second_id'];
                                   category.main_title='--'+$scope.articlescategores[i]['second_title'];

                                   categories_list[j]=category;
                                   j++;

                               }
                            }

                            $scope.categories_list_items=categories_list;

                        }break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                },
                function(response){

                    switch (response.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Not Found'); break;
                        default : toastr.error('Server Error Check Again please !'); break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                }
            );

        }

        $scope.addarticle = function (e) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    var article = {
                        title: angular.element(document).find("#addtitle").val(),
                        tags: angular.element(document).find("#articletags").val(),
                        content: $scope.modeladdarticle,
                        category_id: angular.element(document).find("#addcategory_id").val(),
                        image_url: '',
                    }

                    if($scope.addarticleimages) article.image_url=$scope.addarticleimages;

                    if( article.title=="" || article.content==""||article.category_id=="")
                    {   toastr.error('Missing Information');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }

                    articlesService.addarticle(article,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('article added');
                                    $state.go('dashboardsection',{ section:'articles',page:1 });

                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response){

                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        }
                    );


                }

        $scope.editarticle = function (id) {
            $scope.htmlEditor = '.asd asd asd ..';
            // setup editor options
            $scope.editorOptions = {
                language: 'en'
            };

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
            $scope.section='editarticle';

            categoriesServices.getcategories('articles',40,1,
                function(response)
                {
                    switch (response.status)
                    {
                        case 401:toastr.error('Credentials Incorrect');break;
                        case 202:toastr.error('Error Happened');break;
                        case 200:{
                            $scope.articlescategores=response.data.data;
                            $scope.articlescategorespagination=response.data.meta.pagination;

                            var categories_list=[];
                            var main_cat=0;
                            var j=0;
                            var i=0;

                            for(i=0;i<$scope.articlescategores.length;i++)
                            {

                                var category =
                                    {

                                        main_id: '',
                                        main_title:'',
                                    }


                                if(main_cat!=$scope.articlescategores[i]['main_id'])
                                {
                                    var category_main= {

                                        main_id:$scope.articlescategores[i]['main_id'],
                                        main_title:$scope.articlescategores[i]['main_title'],

                                    }

                                    categories_list[j]=category_main;
                                    j++;

                                    if($scope.articlescategores[i]['second_id']!=null) {
                                        category.main_id = $scope.articlescategores[i]['second_id'];
                                        category.main_title = '--' + $scope.articlescategores[i]['second_title'];
                                        categories_list[j] = category;
                                        j++;
                                    }

                                    main_cat=$scope.articlescategores[i]['main_id'];

                                }
                                else
                                {

                                    category.main_id=$scope.articlescategores[i]['second_id'];
                                    category.main_title='--'+$scope.articlescategores[i]['second_title'];

                                    categories_list[j]=category;
                                    j++;

                                }
                            }

                            $scope.categories_list_items=categories_list;

                        }break;
                    }


                },
                function(response){

                    switch (response.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Not Found'); break;
                        default : toastr.error('Server Error Check Again please !'); break;
                    }


                }
            );

            articlesService.getmyarticle(id,
                  function(response)
                    {

                            switch (response.status)
                            {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:{toastr.error('Error Happened'); $state.go('dashboardsection',{ section:'articles',page:1 });}break;
                                case 200:{
                                    $scope.editarticleval=response.data.data;
                                    $scope.modelName=$scope.trusthtml($scope.editarticleval.content);

                                }break;

                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                    },
                  function(response)
                    {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboardsection',{ section:'articles',page:1 });
                      }
                    );


        }

        $scope.savearticle = function (e)   {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

            var article = {

                id   :angular.element(document).find("#article_id").val(),
                title: angular.element(document).find("#title").val(),
                content: $scope.modelName,
                tags: angular.element(document).find("#editarticletags").val(),
                category_id: angular.element(document).find("#category_id").val(),
                //image_url: angular.element(document).find("#image_url").val(),
                image_url: '',
            }

            if($scope.myFile) article.image_url=$scope.myFile;

            if( article.id =="" || article.title=="" || article.content==""||article.category_id=="") {
                toastr.error('Missing Information');
                angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                return false;
            }

                    articlesService.savearticle(article,
                        function(response)
                        {
                            switch (response.status)
                            {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('article added');
                                    $state.go('dashboardsection',{ section:'articles',page:1 });
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        }
                    );


                }

        $scope.deletepost = function (id) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    blogsService.deleteblogpost(id,
                        function(response){

                            switch (response.status)
                            {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:{
                                    toastr.success('Post deleted');
                                    angular.element(document).find("#post_"+id).remove();
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        }
                    );
                }

        $scope.publishpost = function (id,status) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    blogsService.publishblogpost(id,status,
                        function(response){

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('post updated');
                                }break;

                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboardsection',{ section:'blogs',page:1 });
                        }
                    );
                }

        $scope.showeditpostform = function () {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    $scope.htmlEditor = '.asd asd asd ..';
                    // setup editor options
                    $scope.editorOptions = {
                        language: 'en',
                        uiColor: '#ffffff'
                    };

                    $scope.section='addpost';
            categoriesServices.getcategories('blogs',40,1,
                function(response)
                {
                    switch (response.status)
                    {
                        case 401:toastr.error('Credentials Incorrect');break;
                        case 202:toastr.error('Error Happened');break;
                        case 200:{
                            $scope.blogsscategores=response.data.data;
                            $scope.blogscategorespagination=response.data.meta.pagination;

                            var categories_list=[];
                            var main_cat=0;
                            var j=0;
                            var i=0;

                            for(i=0;i<$scope.blogsscategores.length;i++)
                            {

                                var category =
                                    {

                                        main_id: '',
                                        main_title:'',
                                    }


                                if(main_cat!=$scope.blogsscategores[i]['main_id'])
                                {
                                    var category_main= {

                                        main_id:$scope.blogsscategores[i]['main_id'],
                                        main_title:$scope.blogsscategores[i]['main_title'],

                                    }

                                    categories_list[j]=category_main;
                                    j++;

                                    if($scope.blogsscategores[i]['second_id']!=null) {
                                        category.main_id = $scope.blogsscategores[i]['second_id'];
                                        category.main_title = '--' + $scope.blogsscategores[i]['second_title'];
                                        categories_list[j] = category;
                                        j++;
                                    }

                                    main_cat=$scope.blogsscategores[i]['main_id'];

                                }
                                else
                                {

                                    category.main_id=$scope.blogsscategores[i]['second_id'];
                                    category.main_title='--'+$scope.blogsscategores[i]['second_title'];

                                    categories_list[j]=category;
                                    j++;

                                }
                            }

                            $scope.categories_list_items=categories_list;

                        }break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                },
                function(response){

                    switch (response.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Not Found'); break;
                        default : toastr.error('Server Error Check Again please !'); break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                }
            );





                }

        $scope.addpost = function (e) {

                    $scope.seteditoroptions();


            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    var blogpost = {

                        tags   :angular.element(document).find("#addblogtags").val(),
                        title: angular.element(document).find("#addposttitle").val(),
                        content_text: $scope.modeladdblogpost,
                        category_id: angular.element(document).find("#addpost_category_id").val(),
                        image_url:'',
                    }

                    if($scope.blogaddimages) blogpost.image_url=$scope.blogaddimages;
                    if( blogpost.title=="" || blogpost.content_text==""||blogpost.category_id=="")
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }


                    blogsService.addblogpost(blogpost,
                        function(response){

                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('Blog post added');
                                    $state.go('dashboardsection',{ section:'blogs',page:1 });

                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        }
                    );


                }

        $scope.editpost = function (id) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
            $scope.htmlEditor = '.asd asd asd ..';
            // setup editor options
            $scope.editorOptions = {
                language: 'en'
            };

            $scope.section='editpost';
            categoriesServices.getcategories('blogs',40,1,
                function(response)
                {
                    switch (response.status)
                    {
                        case 401:toastr.error('Credentials Incorrect');break;
                        case 202:toastr.error('Error Happened');break;
                        case 200:{
                            $scope.blogsscategores=response.data.data;


                            var categories_list=[];
                            var main_cat=0;
                            var j=0;
                            var i=0;

                            for(i=0;i<$scope.blogsscategores.length;i++)
                            {

                                var category =
                                    {

                                        main_id: '',
                                        main_title:'',
                                    }


                                if(main_cat!=$scope.blogsscategores[i]['main_id'])
                                {
                                    var category_main= {

                                        main_id:$scope.blogsscategores[i]['main_id'],
                                        main_title:$scope.blogsscategores[i]['main_title'],

                                    }

                                    categories_list[j]=category_main;
                                    j++;

                                    if($scope.blogsscategores[i]['second_id']!=null) {
                                        category.main_id = $scope.blogsscategores[i]['second_id'];
                                        category.main_title = '--' + $scope.blogsscategores[i]['second_title'];
                                        categories_list[j] = category;
                                        j++;
                                    }

                                    main_cat=$scope.blogsscategores[i]['main_id'];

                                }
                                else
                                {

                                    category.main_id=$scope.blogsscategores[i]['second_id'];
                                    category.main_title='--'+$scope.blogsscategores[i]['second_title'];

                                    categories_list[j]=category;
                                    j++;

                                }
                            }

                            $scope.categories_list_items=categories_list;

                        }break;
                    }
                },
                function(response){

                    switch (response.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Not Found'); break;
                        default : toastr.error('Server Error Check Again please !'); break;
                    }


                }
            );

                    blogsService.getmyblog(id,
                        function(response)
                        {

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    $scope.editpostval=response.data.data;
                                    $scope.modelblogpost=$scope.trusthtml($scope.editpostval.content);

                                }break;

                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboardsection',{ section:'blogs',page:1 });

                        }
                    );


                }

        $scope.savepost = function (e) {

            $scope.seteditoroptions();

            $scope.section='editpost';
            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    var blogpost = {

                        id   :angular.element(document).find("#post_id").val(),
                        title: angular.element(document).find("#posttitle").val(),
                        content_text: $scope.modelblogpost,
                        category_id: angular.element(document).find("#post_category_id").val(),
                        //image_url: angular.element(document).find("#image_url").val(),
                        image_url:'',
                    }

                     if($scope.blogimages) blogpost.image_url=$scope.blogimages;
                    if( blogpost.id =="" || blogpost.title=="" || blogpost.content==""||blogpost.category_id=="")
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }

                      blogsService.saveblogpost(blogpost,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('Blog added');
                                    $state.go('dashboardsection',{ section:'blogs',page:1 });
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        }
                    );
                }

        $scope.deleteproduct  = function (id) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    storeService.deleteproduct(id,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:{
                                    toastr.success('product deleted');
                                    angular.element(document).find("#product_"+id).remove();
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        }
                    );
                }

        $scope.publishproduct = function (id,status) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    storeService.publishproduct(id,status,
                        function(response){
                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('product updated');
                                }break;

                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboardsection',{ section:'products',page:1 });
                        }
                    );
                }

        $scope.showeditproductform    = function (id) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
            $scope.htmlEditor = '.asd asd asd ..';
            // setup editor options
            $scope.editorOptions = {
                language: 'en',
                uiColor: '#ffffff'
            };

            $scope.section='addproduct';
            categoriesServices.getcategories('products',40,1,
                function(response)
                {
                    switch (response.status)
                    {
                        case 401:toastr.error('Credentials Incorrect');break;
                        case 202:toastr.error('Error Happened');break;
                        case 200:{
                            $scope.productscategores=response.data.data;

                            var categories_list=[];
                            var main_cat=0;
                            var j=0;
                            var i=0;

                            for(i=0;i<$scope.productscategores.length;i++)
                            {

                                var category =
                                    {

                                        main_id: '',
                                        main_title:'',
                                    }


                                if(main_cat!=$scope.productscategores[i]['main_id'])
                                {
                                    var category_main= {

                                        main_id:$scope.productscategores[i]['main_id'],
                                        main_title:$scope.productscategores[i]['main_title'],

                                    }

                                    categories_list[j]=category_main;
                                    j++;

                                    if($scope.productscategores[i]['second_id']!=null) {
                                        category.main_id = $scope.productscategores[i]['second_id'];
                                        category.main_title = '--' + $scope.productscategores[i]['second_title'];
                                        categories_list[j] = category;
                                        j++;
                                    }

                                    main_cat=$scope.productscategores[i]['main_id'];

                                }
                                else
                                {

                                    category.main_id=$scope.productscategores[i]['second_id'];
                                    category.main_title='--'+$scope.productscategores[i]['second_title'];

                                    categories_list[j]=category;
                                    j++;

                                }
                            }

                            $scope.categories_list_items=categories_list;

                        }break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                },
                function(response){

                    switch (response.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Not Found'); break;
                        default : toastr.error('Server Error Check Again please !'); break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                }
            );
          }

        $scope.editproduct    = function (id)        {

            $scope.section='editproduct';
            $scope.seteditoroptions();
            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

            categoriesServices.getcategories('products',40,1,
                function(response)
                {
                    switch (response.status)
                    {
                        case 401:toastr.error('Credentials Incorrect');break;
                        case 202:toastr.error('Error Happened');break;
                        case 200:{
                            $scope.productscategores=response.data.data;


                            var categories_list=[];
                            var main_cat=0;
                            var j=0;
                            var i=0;

                            for(i=0;i<$scope.productscategores.length;i++)
                            {

                                var category =
                                    {

                                        main_id: '',
                                        main_title:'',
                                    }


                                if(main_cat!=$scope.productscategores[i]['main_id'])
                                {
                                    var category_main= {

                                        main_id:$scope.productscategores[i]['main_id'],
                                        main_title:$scope.productscategores[i]['main_title'],

                                    }

                                    categories_list[j]=category_main;
                                    j++;

                                    if($scope.productscategores[i]['second_id']!=null) {
                                        category.main_id = $scope.productscategores[i]['second_id'];
                                        category.main_title = '--' + $scope.productscategores[i]['second_title'];
                                        categories_list[j] = category;
                                        j++;
                                    }
                                    main_cat=$scope.productscategores[i]['main_id'];

                                }
                                else
                                {

                                    category.main_id=$scope.productscategores[i]['second_id'];
                                    category.main_title='--'+$scope.productscategores[i]['second_title'];

                                    categories_list[j]=category;
                                    j++;

                                }
                            }

                            $scope.categories_list_items=categories_list;

                        }break;
                    }
                },
                function(response){

                    switch (response.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Not Found'); break;
                        default : toastr.error('Server Error Check Again please !'); break;
                    }


                }
            );


             storeService.getmyproduct(id,
                        function(response)
                        {
                            switch (response.status) {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    $scope.editproductdval=response.data.data;
                                    $scope.modelproduct=$scope.trusthtml($scope.editproductdval.content);
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboardsection',{ section:'products',page:1 });

                        }
                    );


                }

        $scope.addproduct    = function (e) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    var product = {
                        title: angular.element(document).find("#addproducttitle").val(),
                        content_text:$scope.modeladdproduct,
                        category_id: angular.element(document).find("#addproduct_category_id").val(),
                        tags: angular.element(document).find("#addproducttags").val(),
                        image_url:'',

                        all_can_see:(angular.element(document).find("#product_all").is(':checked'))? 1:0,
                        attorney_can_see:(angular.element(document).find("#product_attorney").is(':checked'))? 1:0,
                        chiropractor_can_see:(angular.element(document).find("#product_chiropractor").is(':checked'))? 1:0,
                        dentist_can_see:(angular.element(document).find("#product_dentist").is(':checked'))? 1:0,

                        optometrist_can_see:(angular.element(document).find("#product_optometrist").is(':checked'))? 1:0,
                        osteopathist_can_see:(angular.element(document).find("#product_osteopathist").is(':checked'))? 1:0,
                        phd_can_see:(angular.element(document).find("#product_phd").is(':checked'))? 1:0,
                        pharmacist_can_see:(angular.element(document).find("#product_pharmacist").is(':checked'))? 1:0,


                        physician_can_see:(angular.element(document).find("#product_physician").is(':checked'))? 1:0,
                        podiatrist_can_see:(angular.element(document).find("#product_podiatrist").is(':checked'))? 1:0,
                        poi_can_see:(angular.element(document).find("#product_poi").is(':checked'))? 1:0,
                        veterinarian_can_see:(angular.element(document).find("#product_veterinarian").is(':checked'))? 1:0,


                    }


                    if($scope.productaddimages) product.image_url=$scope.productaddimages;
                    if( product.id =="" || product.title=="" || product.content_text==""||product.category_id=="")
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }

                    if( product.all_can_see =="" && product.attorney_can_see=="" && product.chiropractor_can_see==""&&product.dentist_can_see=="" &&
                        product.optometrist_can_see =="" && product.osteopathist_can_see=="" && product.phd_can_see==""&&product.pharmacist_can_see=="" &&
                        product.physician_can_see =="" && product.podiatrist_can_see=="" && product.poi_can_see==""&&product.veterinarian_can_see=="")
                    {
                        toastr.error('Please Choose speciality ');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }
                    storeService.addproduct(product,
                        function(response){


                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('product added');
                                    $state.go('dashboardsection',{ section:'products',page:1 });
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        }
                    );


                }

        $scope.saveproduct    = function (e) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    var product = {

                        id   :angular.element(document).find("#product_id").val(),
                        title: angular.element(document).find("#producttitle").val(),
                        content_text:$scope.modelproduct,
                        category_id: angular.element(document).find("#category_id").val(),
                        tags: angular.element(document).find("#producttags").val(),
                        //image_url: angular.element(document).find("#image_url").val(),
                        image_url:'',

                        all_can_see:(angular.element(document).find("#product_edit_all").is(':checked'))? 1:0,
                        attorney_can_see:(angular.element(document).find("#product_edit_attorney").is(':checked'))? 1:0,
                        chiropractor_can_see:(angular.element(document).find("#product_edit_chiropractor").is(':checked'))? 1:0,
                        dentist_can_see:(angular.element(document).find("#product_edit_dentist").is(':checked'))? 1:0,

                        optometrist_can_see:(angular.element(document).find("#product_edit_optometrist").is(':checked'))? 1:0,
                        osteopathist_can_see:(angular.element(document).find("#product_edit_osteopathist").is(':checked'))? 1:0,
                        phd_can_see:(angular.element(document).find("#product_edit_phd").is(':checked'))? 1:0,
                        pharmacist_can_see:(angular.element(document).find("#product_edit_pharmacist").is(':checked'))? 1:0,


                        physician_can_see:(angular.element(document).find("#product_edit_physician").is(':checked'))? 1:0,
                        podiatrist_can_see:(angular.element(document).find("#product_edit_podiatrist").is(':checked'))? 1:0,
                        poi_can_see:(angular.element(document).find("#product_edit_poi").is(':checked'))? 1:0,
                        veterinarian_can_see:(angular.element(document).find("#product_edit_veterinarian").is(':checked'))? 1:0,



                    }


                    if($scope.productimages) product.image_url=$scope.productimages;
                    if( product.id =="" || product.title=="" || product.content_text==""||product.category_id=="")
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }

                    if( product.all_can_see =="" && product.attorney_can_see=="" && product.chiropractor_can_see==""&&product.dentist_can_see=="" &&
                        product.optometrist_can_see =="" && product.osteopathist_can_see=="" && product.phd_can_see==""&&product.pharmacist_can_see=="" &&
                        product.physician_can_see =="" && product.podiatrist_can_see=="" && product.poi_can_see==""&&product.veterinarian_can_see=="")
                    {
                        toastr.error('Please Choose speciality ');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }

            storeService.saveproduct(product,
                        function(response){


                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('product added');
                                    $state.go('dashboardsection',{ section:'products',page:1 });
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        }
                    );


                }

        $scope.deleteevent = function (id) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    eventsService.deleteevent(id,
                        function(response){

                            switch (response.status) {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:{
                                    toastr.success('event deleted');
                                    angular.element(document).find("#event_"+id).remove();
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        }
                    );
                }

        $scope.publishevent = function (id,status) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    eventsService.publishevent(id,status,
                        function(response) {
                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('event updated');
                                }break;

                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboardsection',{ section:'events',page:1 });
                        }
                    );
                }

        $scope.editevent = function (id) {

            $scope.section='editevent';
            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

            categoriesServices.getcategories('events',40,1,
                function(response)
                {
                    switch (response.status)
                    {
                        case 401:toastr.error('Credentials Incorrect');break;
                        case 202:toastr.error('Error Happened');break;
                        case 200:{
                            $scope.eventscategores=response.data.data;


                            var categories_list=[];
                            var main_cat=0;
                            var j=0;
                            var i=0;

                            for(i=0;i<$scope.eventscategores.length;i++)
                            {

                                var category =
                                    {

                                        main_id: '',
                                        main_title:'',
                                    }


                                if(main_cat!=$scope.eventscategores[i]['main_id'])
                                {
                                    var category_main= {

                                        main_id:$scope.eventscategores[i]['main_id'],
                                        main_title:$scope.eventscategores[i]['main_title'],

                                    }

                                    categories_list[j]=category_main;
                                    j++;

                                    if($scope.eventscategores[i]['second_id']!=null) {
                                        category.main_id = $scope.eventscategores[i]['second_id'];
                                        category.main_title = '--' + $scope.eventscategores[i]['second_title'];
                                        categories_list[j] = category;
                                        j++;
                                    }
                                    main_cat=$scope.eventscategores[i]['main_id'];

                                }
                                else
                                {

                                    category.main_id=$scope.eventscategores[i]['second_id'];
                                    category.main_title='--'+$scope.eventscategores[i]['second_title'];

                                    categories_list[j]=category;
                                    j++;

                                }
                            }

                            $scope.categories_list_items=categories_list;

                        }break;
                    }
                },
                function(response){

                    switch (response.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Not Found'); break;
                        default : toastr.error('Server Error Check Again please !'); break;
                    }


                }
            );


                    eventsService.getmyevent(id,
                        function(response){

                            switch (response.status) {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    $scope.editeventval=response.data.data;
                                    $scope.modelevent=$scope.trusthtml($scope.editeventval.content);
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");


                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboardsection',{ section:'events',page:1 });

                        }
                    );


                }

        $scope.showaddeventform = function () {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
            $scope.htmlEditor = '.asd asd asd ..';
            // setup editor options
            $scope.editorOptions = {
                language: 'en',
                uiColor: '#ffffff'
            };

            $scope.section='addevent';
            categoriesServices.getcategories('events',40,1,
                function(response){
                    switch (response.status)
                    {
                        case 401:toastr.error('Credentials Incorrect');break;
                        case 202:toastr.error('Error Happened');break;
                        case 200:{
                            $scope.productscategores=response.data.data;

                            var categories_list=[];
                            var main_cat=0;
                            var j=0;
                            var i=0;

                            for(i=0;i<$scope.productscategores.length;i++)
                            {

                                var category =
                                    {

                                        main_id: '',
                                        main_title:'',
                                    }


                                if(main_cat!=$scope.productscategores[i]['main_id'])
                                {
                                    var category_main= {

                                        main_id:$scope.productscategores[i]['main_id'],
                                        main_title:$scope.productscategores[i]['main_title'],

                                    }

                                    categories_list[j]=category_main;
                                    j++;

                                    if($scope.productscategores[i]['second_id']!=null) {
                                        category.main_id = $scope.productscategores[i]['second_id'];
                                        category.main_title = '--' + $scope.productscategores[i]['second_title'];
                                        categories_list[j] = category;
                                        j++;
                                    }

                                    main_cat=$scope.productscategores[i]['main_id'];

                                }
                                else
                                {

                                    category.main_id=$scope.productscategores[i]['second_id'];
                                    category.main_title='--'+$scope.productscategores[i]['second_title'];

                                    categories_list[j]=category;
                                    j++;

                                }
                            }

                            $scope.categories_list_items=categories_list;

                        }break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                },
                function(response){

                    switch (response.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Not Found'); break;
                        default : toastr.error('Server Error Check Again please !'); break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                }
            );





                }

        $scope.saveevent = function (e) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    var event = {

                        id   :angular.element(document).find("#event_id").val(),
                        title: angular.element(document).find("#eventtitle").val(),
                        content_text: $scope.modelevent,
                        category_id: angular.element(document).find("#event_category_id").val(),
                        tags: angular.element(document).find("#eventtags").val(),
                        image_url:'',
                        all_can_see:(angular.element(document).find("#event_edit_all").is(':checked'))? 1:0,
                        attorney_can_see:(angular.element(document).find("#event_edit_attorney").is(':checked'))? 1:0,
                        chiropractor_can_see:(angular.element(document).find("#event_edit_chiropractor").is(':checked'))? 1:0,
                        dentist_can_see:(angular.element(document).find("#event_edit_dentist").is(':checked'))? 1:0,

                        optometrist_can_see:(angular.element(document).find("#event_edit_optometrist").is(':checked'))? 1:0,
                        osteopathist_can_see:(angular.element(document).find("#event_edit_osteopathist").is(':checked'))? 1:0,
                        phd_can_see:(angular.element(document).find("#event_edit_phd").is(':checked'))? 1:0,
                        pharmacist_can_see:(angular.element(document).find("#event_edit_pharmacist").is(':checked'))? 1:0,


                        physician_can_see:(angular.element(document).find("#event_edit_physician").is(':checked'))? 1:0,
                        podiatrist_can_see:(angular.element(document).find("#event_edit_podiatrist").is(':checked'))? 1:0,
                        poi_can_see:(angular.element(document).find("#event_edit_poi").is(':checked'))? 1:0,
                        veterinarian_can_see:(angular.element(document).find("#event_edit_veterinarian").is(':checked'))? 1:0,

                    }

                    if($scope.eventimages) event.image_url=$scope.eventimages;
                    if( event.id =="" || event.title=="" || event.content_text==""||event.category_id=="")
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }

                    if( event.all_can_see =="" && event.attorney_can_see=="" && event.chiropractor_can_see==""&&event.dentist_can_see=="" &&
                        event.optometrist_can_see =="" && event.osteopathist_can_see=="" && event.phd_can_see==""&&event.pharmacist_can_see=="" &&
                        event.physician_can_see =="" && event.podiatrist_can_see=="" && event.poi_can_see==""&&event.veterinarian_can_see=="")
                    {
                        toastr.error('Please Choose speciality ');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }


                    eventsService.saveevent(event,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('event Updated');
                                    $state.go('dashboardsection',{ section:'events',page:1 });
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        }
                    );


                }

        $scope.addevent = function (e) {

              angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    $scope.addeventDate = new Date();


                    var event = {
                        title: angular.element(document).find("#addeventtitle").val(),
                        content_text: $scope.modeladdevent,
                        category_id: angular.element(document).find("#addevent_category_id").val(),
                        tags: angular.element(document).find("#addeventtags").val(),
                        event_date:utilitiesServices.formatDate($scope.addeventDate),
                        image_url:'',


                        all_can_see:(angular.element(document).find("#all").is(':checked'))? 1:0,
                        attorney_can_see:(angular.element(document).find("#attorney").is(':checked'))? 1:0,
                        chiropractor_can_see:(angular.element(document).find("#chiropractor").is(':checked'))? 1:0,
                        dentist_can_see:(angular.element(document).find("#dentist").is(':checked'))? 1:0,

                        optometrist_can_see:(angular.element(document).find("#optometrist").is(':checked'))? 1:0,
                        osteopathist_can_see:(angular.element(document).find("#osteopathist").is(':checked'))? 1:0,
                        phd_can_see:(angular.element(document).find("#phd").is(':checked'))? 1:0,
                        pharmacist_can_see:(angular.element(document).find("#pharmacist").is(':checked'))? 1:0,


                        physician_can_see:(angular.element(document).find("#physician").is(':checked'))? 1:0,
                        podiatrist_can_see:(angular.element(document).find("#podiatrist").is(':checked'))? 1:0,
                        poi_can_see:(angular.element(document).find("#poi").is(':checked'))? 1:0,
                        veterinarian_can_see:(angular.element(document).find("#veterinarian").is(':checked'))? 1:0,

                        event_end_date:utilitiesServices.formatDate($scope.addendeventDate),
                        event_type    :angular.element(document).find("#addeventtype").val(),
                        discipline    :angular.element(document).find("#addeventdiscipline").val(),
                        subdiscipline :angular.element(document).find("#addeventsubdiscipline").val(),
                        organization  :angular.element(document).find("#addeventorganization").val(),
                        venue         :angular.element(document).find("#addeventvenue").val(),
                        fulladdress   :angular.element(document).find("#addeventfulladdress").val(),
                        lot           :angular.element(document).find("#addeventlatitude").val(),
                        lan           :angular.element(document).find("#addeventlongitude").val()

                    }


                    if($scope.addeventimages) event.image_url=$scope.addeventimages;
                    if( event.id =="" || event.title=="" || event.content_text==""||event.category_id=="")
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }

                    if( event.all_can_see =="" && event.attorney_can_see=="" && event.chiropractor_can_see==""&&event.dentist_can_see=="" &&
                        event.optometrist_can_see =="" && event.osteopathist_can_see=="" && event.phd_can_see==""&&event.pharmacist_can_see=="" &&
                        event.physician_can_see =="" && event.podiatrist_can_see=="" && event.poi_can_see==""&&event.veterinarian_can_see=="")
                    {
                        toastr.error('Please Choose speciality ');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }




            eventsService.addevent(event,
                        function(response){

                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('event added');
                                    $state.go('dashboardsection',{ section:'events',page:1 });
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        }
                    );

                }

        $scope.deleteclassified = function (id) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    classifiedsService.deleteclassified(id,
                        function(response){
                            switch (response.status) {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:{
                                    toastr.success('Classified deleted');
                                    angular.element(document).find("#classified_"+id).remove();
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        }
                    );
                }

        $scope.publishclassified = function (id,status) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    classifiedsService.publishclassified(id,status,
                        function(response){
                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('classified updated');
                                }break;

                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboardsection',{ section:'classifieds',page:1 });
                        }
                    );
                }

        $scope.showclassifiedform = function () {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    $scope.dateoptions();

                    $scope.section='addclassified';

                    $scope.seteditoroptions();



            categoriesServices.getcategories('classifieds',40,1,
                function(response)
                {
                    switch (response.status)
                    {
                        case 401:toastr.error('Credentials Incorrect');break;
                        case 202:toastr.error('Error Happened');break;
                        case 200:{
                            $scope.classifiedscategores=response.data.data;

                            var categories_list=[];
                            var main_cat=0;
                            var j=0;
                            var i=0;

                            for(i=0;i<$scope.classifiedscategores.length;i++)
                            {

                                var category =
                                    {

                                        main_id: '',
                                        main_title:'',
                                    }


                                if(main_cat!=$scope.classifiedscategores[i]['main_id'])
                                {
                                    var category_main= {

                                        main_id:$scope.classifiedscategores[i]['main_id'],
                                        main_title:$scope.classifiedscategores[i]['main_title'],

                                    }

                                    categories_list[j]=category_main;
                                    j++;

                                    if($scope.classifiedscategores[i]['second_id']!=null) {
                                        category.main_id = $scope.classifiedscategores[i]['second_id'];
                                        category.main_title = '--' + $scope.classifiedscategores[i]['second_title'];
                                        categories_list[j] = category;
                                        j++;
                                    }

                                    main_cat=$scope.classifiedscategores[i]['main_id'];

                                }
                                else
                                {

                                    category.main_id=$scope.classifiedscategores[i]['second_id'];
                                    category.main_title='--'+$scope.classifiedscategores[i]['second_title'];

                                    categories_list[j]=category;
                                    j++;

                                }
                            }

                            $scope.categories_list_items=categories_list;

                        }break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                },
                function(response){

                    switch (response.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Not Found'); break;
                        default : toastr.error('Server Error Check Again please !'); break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                }
            );




        }

        $scope.editclassified = function (id) {

            $scope.section='editclassified';
            $scope.seteditoroptions();

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
            categoriesServices.getcategories('classifieds',40,1,
                function(response)
                {
                    switch (response.status)
                    {
                        case 401:toastr.error('Credentials Incorrect');break;
                        case 202:toastr.error('Error Happened');break;
                        case 200:{
                            $scope.classifiedcategores=response.data.data;


                            var categories_list=[];
                            var main_cat=0;
                            var j=0;
                            var i=0;

                            for(i=0;i<$scope.classifiedcategores.length;i++)
                            {

                                var category =
                                    {

                                        main_id: '',
                                        main_title:'',
                                    }


                                if(main_cat!=$scope.classifiedcategores[i]['main_id'])
                                {
                                    var category_main= {

                                        main_id:$scope.classifiedcategores[i]['main_id'],
                                        main_title:$scope.classifiedcategores[i]['main_title'],

                                    }

                                    categories_list[j]=category_main;
                                    j++;

                                    if($scope.classifiedcategores[i]['second_id']!=null) {
                                        category.main_id = $scope.classifiedcategores[i]['second_id'];
                                        category.main_title = '--' + $scope.classifiedcategores[i]['second_title'];
                                        categories_list[j] = category;
                                        j++;
                                    }
                                    main_cat=$scope.classifiedcategores[i]['main_id'];

                                }
                                else
                                {

                                    category.main_id=$scope.classifiedcategores[i]['second_id'];
                                    category.main_title='--'+$scope.classifiedcategores[i]['second_title'];

                                    categories_list[j]=category;
                                    j++;

                                }
                            }

                            $scope.categories_list_items=categories_list;

                        }break;
                    }
                },
                function(response){

                    switch (response.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Not Found'); break;
                        default : toastr.error('Server Error Check Again please !'); break;
                    }


                }
            );


            classifiedsService.getmyclassified(id,
                function(response)
                {
                    switch (response.status) {
                        case 401:toastr.error('Credentials Incorrect');break;
                        case 202:toastr.error('Error Happened');break;
                        case 200:{
                            $scope.editclassifiedval=response.data.data;
                            $scope.modelclassified=$scope.trusthtml($scope.editclassifiedval.content);
                            $scope.myDate=$scope.editclassifiedval.end_date;

                        }break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                },
                function(response){
                    switch (response.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Not Found'); break;
                        default : toastr.error('Server Error Check Again please !'); break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                    $state.go('dashboardsection',{ section:'classifieds',page:1 });

                        }
            );


            }

        $scope.addclassified = function (e) {


                    $scope.seteditoroptions();
                    $scope.section='addclassified';
            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    var classified = {
                        tags   :angular.element(document).find("#addclassifiedtags").val(),
                        title: angular.element(document).find("#addclassifiedtitle").val(),
                        content_text:$scope.modeladdclassified,
                        category_id: angular.element(document).find("#addclassified_category_id").val(),
                        end_date:utilitiesServices.formatDate($scope.myDate),

                        hideowner:(angular.element(document).find("#hideowner").is(':checked'))? 1:0,

                        all_can_see:(angular.element(document).find("#classified_all").is(':checked'))? 1:0,
                        attorney_can_see:(angular.element(document).find("#classified_attorney").is(':checked'))? 1:0,
                        chiropractor_can_see:(angular.element(document).find("#classified_chiropractor").is(':checked'))? 1:0,
                        dentist_can_see:(angular.element(document).find("#classified_dentist").is(':checked'))? 1:0,

                        optometrist_can_see:(angular.element(document).find("#classified_optometrist").is(':checked'))? 1:0,
                        osteopathist_can_see:(angular.element(document).find("#classified_osteopathist").is(':checked'))? 1:0,
                        phd_can_see:(angular.element(document).find("#classified_phd").is(':checked'))? 1:0,
                        pharmacist_can_see:(angular.element(document).find("#classified_pharmacist").is(':checked'))? 1:0,


                        physician_can_see:(angular.element(document).find("#classified_physician").is(':checked'))? 1:0,
                        podiatrist_can_see:(angular.element(document).find("#classified_podiatrist").is(':checked'))? 1:0,
                        poi_can_see:(angular.element(document).find("#classified_poi").is(':checked'))? 1:0,
                        veterinarian_can_see:(angular.element(document).find("#classified_veterinarian").is(':checked'))? 1:0,
                        image_url:'',

                    }




                    if($scope.classifiedaddimages) classified.image_url=$scope.classifiedaddimages;
                    if(  classified.title=="" || classified.content_text==""||classified.category_id=="")
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }

                    if( classified.all_can_see =="" && classified.attorney_can_see=="" && classified.chiropractor_can_see==""&&classified.dentist_can_see=="" &&
                        classified.optometrist_can_see =="" && classified.osteopathist_can_see=="" && classified.phd_can_see==""&&classified.pharmacist_can_see=="" &&
                        classified.physician_can_see =="" && classified.podiatrist_can_see=="" && classified.poi_can_see==""&&classified.veterinarian_can_see=="")
                    {
                        toastr.error('Please Choose speciality ');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }



                    classifiedsService.addclassified(classified,
                        function(response){
                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('classified added');
                                    $state.go('dashboardsection',{ section:'classifieds',page:1 });
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        }
                    );


                }

        $scope.saveclassified = function (e) {


                    $scope.seteditoroptions();
                    $scope.section='editclassified';
                    angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    var classified = {
                        id   :angular.element(document).find("#classified_id").val(),
                        title: angular.element(document).find("#classifiedtitle").val(),
                        content_text:$scope.modelclassified,
                        category_id: angular.element(document).find("#classified_category_id").val(),

                        hideowner:(angular.element(document).find("#classified_edit_hideowner").is(':checked'))? '1':'0',

                        end_date:utilitiesServices.formatDate($scope.myDate),

                        all_can_see:(angular.element(document).find("#classified_edit_all").is(':checked'))? 1:0,
                        attorney_can_see:(angular.element(document).find("#classified_edit_attorney").is(':checked'))? 1:0,
                        chiropractor_can_see:(angular.element(document).find("#classified_edit_chiropractor").is(':checked'))? 1:0,
                        dentist_can_see:(angular.element(document).find("#classified_edit_dentist").is(':checked'))? 1:0,

                        optometrist_can_see:(angular.element(document).find("#classified_edit_optometrist").is(':checked'))? 1:0,
                        osteopathist_can_see:(angular.element(document).find("#classified_edit_osteopathist").is(':checked'))? 1:0,
                        phd_can_see:(angular.element(document).find("#classified_edit_phd").is(':checked'))? 1:0,
                        pharmacist_can_see:(angular.element(document).find("#classified_edit_pharmacist").is(':checked'))? 1:0,


                        physician_can_see:(angular.element(document).find("#classified_edit_physician").is(':checked'))? 1:0,
                        podiatrist_can_see:(angular.element(document).find("#classified_edit_podiatrist").is(':checked'))? 1:0,
                        poi_can_see:(angular.element(document).find("#classified_edit_poi").is(':checked'))? 1:0,
                        veterinarian_can_see:(angular.element(document).find("#classified_edit_veterinarian").is(':checked'))? 1:0,

                    }




            if($scope.classifiedimages) classified.image_url=$scope.classifiedimages;

            if(  classified.title=="" || classified.content_text==""||classified.category_id=="")
            {
                toastr.error('Missing Information');
                angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                return false;
            }

            if( classified.all_can_see =="" && classified.attorney_can_see=="" && classified.chiropractor_can_see==""&&classified.dentist_can_see=="" &&
                classified.optometrist_can_see =="" && classified.osteopathist_can_see=="" && classified.phd_can_see==""&&classified.pharmacist_can_see=="" &&
                classified.physician_can_see =="" && classified.podiatrist_can_see=="" && classified.poi_can_see==""&&classified.veterinarian_can_see=="")
            {
                toastr.error('Please Choose speciality ');
                angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                return false;
            }

                    classifiedsService.saveclassified(classified,
                        function(response){

                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('classified Updated');
                                    $state.go('dashboardsection',{ section:'classifieds',page:1 });
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                        },
                        function(response){
                            angular.element(document).find("#dashboard_content").removeClass( "spinner" );

                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        }
                    );


                }

        $scope.deletegroup = function (id) {

                angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    groupsService.deletegroup(id,
                        function(response){

                            switch (response.status) {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:{
                                    toastr.success('Group deleted');
                                    angular.element(document).find("#group_"+id).remove();
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        }
                    );
                }

        $scope.publishgroup = function (id,status) {

                 angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    groupsService.publishgroup(id,status,
                        function(response){
                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('group updated');
                                }break;

                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboardsection',{ section:'groups',page:1 });
                        }
                    );
                }

        $scope.showaddgroup    = function (id)   {

                    angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    $scope.section='addgroup';

            accountServices.getmycommunity('groups',
                function(response) {
                    switch (response.status) {
                        case 401:
                            toastr.error('Credentials Incorrect');
                            break;
                        case 202:
                            toastr.error('Error Happened');
                            break;
                        case 200: {
                            $scope.communities = response.data;



                        } break;
                    }

                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                },
                function(response){

                    switch (response.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Not Found'); break;
                        default : toastr.error('Server Error Check Again please !'); break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                }
            );


            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                    return ;
        }

        $scope.editgroup    = function (id) {

                    $scope.section='editgroup';

                    $scope.seteditoroptions();

                 angular.element(document).find("#dashboard_content").LoadingOverlay("show");


            accountServices.getmycommunity('groups',
                function(response) {
                    switch (response.status) {
                        case 401:
                            toastr.error('Credentials Incorrect');
                            break;
                        case 202:
                            toastr.error('Error Happened');
                            break;
                        case 200: {
                            $scope.communities = response.data;

                        } break;
                    }
                },
                function(response){

                    switch (response.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Not Found'); break;
                        default : toastr.error('Cant Get Communities !'); break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                    $state.go('dashboardsection',{ section:'groups',page:1 });
                }
            );





                    groupsService.getmygroup(id,
                        function(response){

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    //  response.data.data;
                                    $scope.editgroupdval=response.data.data;
                                    $scope.modelgroup=$scope.trusthtml($scope.editgroupdval.description);
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboardsection',{ section:'groups',page:1 });

                        }
                    );


                }

        $scope.savegroup = function (e) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

            var group = {

                id   :angular.element(document).find("#group_id").val(),
                title: angular.element(document).find("#grouptitle").val(),
                description: $scope.modelgroup,
                tags: angular.element(document).find("#grouptags").val(),
                image_url:'',
                all_can_see:(angular.element(document).find("#edit_group_all_can_see").prop('checked')==true)? 1:0,
                attorney_can_see:(angular.element(document).find("#edit_group_attorney_can_see").prop('checked')==true)? 1:0,
                chiropractor_can_see:(angular.element(document).find("#edit_group_chiropractor_can_see").prop('checked')==true)? 1:0,
                dentist_can_see:(angular.element(document).find("#edit_group_dentist_can_see").prop('checked')==true)? 1:0,

                optometrist_can_see:(angular.element(document).find("#edit_group_optometrist_can_see").prop('checked')==true)? 1:0,
                osteopathist_can_see:(angular.element(document).find("#edit_group_osteopathist_can_see").prop('checked')==true)? 1:0,
                phd_can_see:(angular.element(document).find("#edit_group_phd_can_see").prop('checked')==true)? 1:0,
                pharmacist_can_see:(angular.element(document).find("#edit_group_pharmacist_can_see").prop('checked')==true)? 1:0,


                physician_can_see:(angular.element(document).find("#edit_group_physician_can_see").prop('checked')==true)? 1:0,
                podiatrist_can_see:(angular.element(document).find("#edit_group_podiatrist_can_see").prop('checked')==true)? 1:0,
                poi_can_see:(angular.element(document).find("#edit_group_poi_can_see").prop('checked')==true)? 1:0,
                veterinarian_can_see:(angular.element(document).find("#edit_group_veterinarian_can_see").prop('checked')==true)? 1:0,
            }

            if($scope.groupimages) group.image_url=$scope.groupimages;
            if( group.id =="" || group.title=="" || group.content=="")
            {
                toastr.error('Missing Information');
                angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                return false;
            }


            if( group.all_can_see ==0 && group.attorney_can_see==0 && group.chiropractor_can_see==0 && group.dentist_can_see==0 &&
                group.optometrist_can_see ==0 && group.osteopathist_can_see==0 && group.phd_can_see==0 && group.pharmacist_can_see==0 &&
                group.physician_can_see ==0 && group.podiatrist_can_see==0 && group.poi_can_see==0 && group.veterinarian_can_see==0)
            {
                toastr.error('Please Choose speciality ');
                angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                return false;
            }




            groupsService.savegroup(group,
                        function(response){

                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('group Updated');
                                    $state.go('dashboardsection',{ section:'groups',page:1 });
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboardsection',{ section:'groups',page:1 });

                        }
                    );


                }

        $scope.addgroup = function (e) {




            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    var group = {

                        title: angular.element(document).find("#addgrouptitle").val(),
                        description: $scope.modeladdgroup,
                        tags: angular.element(document).find("#addgrouptags").val(),
                        image_url:'',


                        all_can_see:(angular.element(document).find("#group_all_can_see").prop('checked')==true)? 1:0,
                        attorney_can_see:(angular.element(document).find("#group_attorney_can_see").prop('checked')==true)? 1:0,
                        chiropractor_can_see:(angular.element(document).find("#group_chiropractor_can_see").prop('checked')==true)? 1:0,
                        dentist_can_see:(angular.element(document).find("#group_dentist_can_see").prop('checked')==true)? 1:0,

                        optometrist_can_see:(angular.element(document).find("#group_optometrist_can_see").prop('checked')==true)? 1:0,
                        osteopathist_can_see:(angular.element(document).find("#group_osteopathist_can_see").prop('checked')==true)? 1:0,
                        phd_can_see:(angular.element(document).find("#group_phd_can_see").prop('checked')==true)? 1:0,
                        pharmacist_can_see:(angular.element(document).find("#group_pharmacist_can_see").prop('checked')==true)? 1:0,


                        physician_can_see:(angular.element(document).find("#group_physician_can_see").prop('checked')==true)? 1:0,
                        podiatrist_can_see:(angular.element(document).find("#group_podiatrist_can_see").prop('checked')==true)? 1:0,
                        poi_can_see:(angular.element(document).find("#group_poi_can_see").prop('checked')==true)? 1:0,
                        veterinarian_can_see:(angular.element(document).find("#group_veterinarian_can_see").prop('checked')==true)? 1:0,
                    }





                    if($scope.addgroupimages) group.image_url=$scope.addgroupimages;
                    if( group.title=="" || group.description=="" )
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }

                    if( group.all_can_see ==0 && group.attorney_can_see==0 && group.chiropractor_can_see==0 && group.dentist_can_see==0 &&
                        group.optometrist_can_see ==0 && group.osteopathist_can_see==0 && group.phd_can_see==0 && group.pharmacist_can_see==0 &&
                        group.physician_can_see ==0 && group.podiatrist_can_see==0 && group.poi_can_see==0 && group.veterinarian_can_see==0)
                    {
                        toastr.error('Please Choose speciality ');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }



                    groupsService.addgroup(group,
                        function(response){

                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('Group added');
                                    $state.go('dashboardsection',{ section:'groups',page:1 });
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        }
                    );


                }



        $scope.deleteforumtopic = function (id) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    forumsService.deleteforumtopic(id,
                        function(response){

                            switch (response.status) {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:{
                                    toastr.success('Topic deleted');
                                    angular.element(document).find("#forumtopic_"+id).remove();
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        }
                    );
                }

        $scope.publishforumtopic = function (id,status) {

                angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    forumsService.publishforumtopic(id,status,
                        function(response){
                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('Topic updated');
                                }break;

                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboardsection',{ section:'forum',page:1 });
                        }
                    );
                }

        $scope.showaddforumtopic = function ()  {

            $scope.section='addforum';
            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
            $scope.htmlEditor = '.asd asd asd ..';
            // setup editor options
            $scope.editorOptions = {
                language: 'en',
                uiColor: '#ffffff'
            };



            forumsService.getmyforumlists(
                function(response)
                {
                    switch (response.status)
                    {
                        case 401:toastr.error('Credentials Incorrect');break;
                        case 202:toastr.error('Error Happened');break;
                        case 200:{
                            $scope.forumlist=response.data.data;

                            var categories_list=[];
                            var main_cat=0;
                            var j=0;
                            var i=0;

                            for(i=0;i<$scope.forumlist.length;i++)
                            {

                                var category =
                                    {

                                        main_id: '',
                                        main_title:'',
                                    }


                                if(main_cat!=$scope.forumlist[i]['main_id'])
                                {
                                    var category_main= {

                                        main_id:$scope.forumlist[i]['main_id'],
                                        main_title:$scope.forumlist[i]['main_title'],

                                    }

                                    categories_list[j]=category_main;
                                    j++;

                                    if($scope.forumlist[i]['second_id']!=null) {
                                        category.main_id = $scope.forumlist[i]['second_id'];
                                        category.main_title = '--' + $scope.forumlist[i]['second_title'];
                                        categories_list[j] = category;
                                        j++;
                                    }

                                    main_cat=$scope.forumlist[i]['main_id'];

                                }
                                else
                                {

                                    category.main_id=$scope.forumlist[i]['second_id'];
                                    category.main_title='--'+$scope.forumlist[i]['second_title'];

                                    categories_list[j]=category;
                                    j++;

                                }
                            }

                            $scope.forums_list_items=categories_list;

                        }break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                },
                function(response){

                    switch (response.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Not Found'); break;
                        default : toastr.error('Server Error Check Again please !'); break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                }
            );
         }

        $scope.editforumtopic = function (id) {

                    $scope.section='editforum';
                    angular.element(document).find("#dashboard_content").LoadingOverlay("show");
            $scope.seteditoroptions();

            forumsService.getmyforumlists(
                function(response)
                {
                    switch (response.status)
                    {
                        case 401:toastr.error('Credentials Incorrect');break;
                        case 202:toastr.error('Error Happened');break;
                        case 200:{
                            $scope.forumlist=response.data.data;

                            var categories_list=[];
                            var main_cat=0;
                            var j=0;
                            var i=0;

                            for(i=0;i<$scope.forumlist.length;i++)
                            {

                                var category =
                                    {

                                        main_id: '',
                                        main_title:'',
                                    }


                                if(main_cat!=$scope.forumlist[i]['main_id'])
                                {
                                    var category_main= {

                                        main_id:$scope.forumlist[i]['main_id'],
                                        main_title:$scope.forumlist[i]['main_title'],

                                    }

                                    categories_list[j]=category_main;
                                    j++;

                                    if($scope.forumlist[i]['second_id']!=null) {
                                        category.main_id = $scope.forumlist[i]['second_id'];
                                        category.main_title = '--' + $scope.forumlist[i]['second_title'];
                                        categories_list[j] = category;
                                        j++;
                                    }

                                    main_cat=$scope.forumlist[i]['main_id'];

                                }
                                else
                                {

                                    category.main_id=$scope.forumlist[i]['second_id'];
                                    category.main_title='--'+$scope.forumlist[i]['second_title'];

                                    categories_list[j]=category;
                                    j++;

                                }
                            }

                            $scope.forums_list_items=categories_list;

                        }break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                },
                function(response){

                    switch (response.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Not Found'); break;
                        default : toastr.error('Server Error Check Again please !'); break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                }
            );

                    forumsService.getmyforumtopic(id,
                        function(response){

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    $scope.editforumtopicval=response.data.data;
                                    $scope.modeladdforumtopic=$scope.trusthtml($scope.editforumtopicval.content);
                                    console.log($scope.modeladdforumtopic);
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                       },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboardsection',{ section:'forum',page:1 });
                        }
                    );


                }

        $scope.saveforumtopic = function (e)  {

            $scope.seteditoroptions();
            $scope.section='editforum';

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

            var forumtopic = {
                id   :angular.element(document).find("#topic_id").val(),
                title: angular.element(document).find("#topictitle").val(),
                content: $scope.modeladdforumtopic,
                tags: angular.element(document).find("#topictags").val(),
                forum_id: angular.element(document).find("#editforum_id").val(),
            }



            if( forumtopic.id ==""|| forumtopic.forum_id =="" || forumtopic.title=="" || forumtopic.content==null)
            {
                toastr.error('Missing Information');
                angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                return false;
            }

            forumsService.savetopic(forumtopic,
                        function(response){
                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('Topic updated');
                                    $state.go('dashboardsection',{ section:'forum',page:1 });
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response){

                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboardsection',{ section:'forum',page:1 });

                        }
                    );

        }

        $scope.addforumtopic = function (e) {

            $scope.seteditoroptions();


            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    var forumtopic = {

                        title: angular.element(document).find("#addtopictitle").val(),
                        content: $scope.modeladdforumtopic,
                        tags: angular.element(document).find("#addtopictags").val(),
                        forum_id: angular.element(document).find("#addforum_id").val(),
                    }


                    if( forumtopic.forum_id =="" || forumtopic.title=="" || forumtopic.content==null)
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }



                    forumsService.addtopic(forumtopic,
                        function(response){

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('Topic added');
                                    $state.go('dashboardsection',{ section:'forum',page:1 });
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        }
                    );


                }

        $scope.deleteforumpost = function (id) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    forumsService.deleteforumpost(id,
                        function(response){

                            switch (response.status) {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:{
                                    toastr.success('Topic deleted');
                                    angular.element(document).find("#forumspost_"+id).remove();
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        }
                    );
                }

        $scope.publishforumpost = function (id,status) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    forumsService.publishforumpost(id,status,
                        function(response){
                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('Post updated');
                                }break;

                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboardsection',{ section:'forumpost',page:1 });
                        }
                    );
                }

        $scope.showaddforumpost = function () {

                    $scope.section='addforumpost';
                    angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    $scope.htmlEditor = '.asd asd asd ..';
                    $scope.editorOptions = {
                    language: 'en',
                    uiColor: '#ffffff'
                    };


            forumsService.getmyforumtopicslists(
                function(response)
                {
                    switch (response.status)
                    {
                        case 401:toastr.error('Credentials Incorrect');break;
                        case 202:toastr.error('Error Happened');break;
                        case 200:{
                            $scope.topicslist=response.data.data;
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        }break;
                    }
                },
                function(response){

                    switch (response.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Not Found'); break;
                        default : toastr.error('Server Error Check Again please !'); break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                    $state.go('dashboardsection',{ section:'forumpost',page:1 });
                }
            );

        }

        $scope.editforumpost = function (id) {

                    $scope.section='editforumpost';
                    angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    forumsService.getmyforumtopicslists(
                    function(response)
                       {
                    switch (response.status)
                    {
                        case 401:toastr.error('Credentials Incorrect');break;
                        case 202:toastr.error('Error Happened');break;
                        case 200:{
                            $scope.topicslist=response.data.data;

                        }break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                },
                    function(response){

                    switch (response.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Not Found'); break;
                        default : toastr.error('Server Error Check Again please !'); break;
                    }
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                }
                    );


                    forumsService.getmyforumpost(id,
                        function(response){

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    $scope.editforumpostval=response.data.data;
                                    $scope.modelforumpostedit=$scope.trusthtml($scope.editforumpostval.text);

                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboardsection',{ section:'forumpost',page:1 });

                        }
                    );


                }

        $scope.saveforumpost = function (e) {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

            var forumpost = {

                        id   :angular.element(document).find("#forumpost_id").val(),
                        title: angular.element(document).find("#editforumposttitle").val(),
                        content_text: $scope.modelforumpostedit,
                        tags: angular.element(document).find("#editforumposttags").val(),
                        topic_id: angular.element(document).find("#edittopic_id").val(),

                    }


                    if( forumpost.id =="" || forumpost.title=="" || forumpost.content_text=="" || forumpost.topic_id=="")
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }


                    forumsService.savepost(forumpost,
                        function(response){

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('Post updated');
                                    $state.go('dashboardsection',{ section:'forumpost',page:1 });
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response){

                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('dashboardsection',{ section:'forumpost',page:1 });

                        }
                    );


                }

        $scope.addforumpost = function (e) {

                    $scope.seteditoroptions();


                    angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    var forumpost = {

                        title: angular.element(document).find("#forumposttitle").val(),
                        content: $scope.modelforumpost,
                        tags: angular.element(document).find("#forumposttags").val(),
                        topic_id: angular.element(document).find("#addtopic_id").val(),
                    }


                    if( forumpost.topic_id =="" || forumpost.title=="" || forumpost.content==null)
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }



                    forumsService.addforumpost(forumpost,
                        function(response){

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('Post added');
                                    $state.go('dashboardsection',{ section:'forumpost',page:1 });
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        }
                    );


                }





        $scope.initdashboardcontr=function()
        {

             $scope.pageparm= 1;
             if($stateParams.page!=null && $stateParams.page!=undefined &&  $stateParams.page!='')
                    $scope.pageparm= $stateParams.page


                    $scope.prepageparm=parseInt($scope.pageparm)-1;
                    $scope.nextpageparm=parseInt($scope.pageparm)+1;

            if($stateParams.section!=null ||$stateParams.section!=undefined)
            {
                 switch ($stateParams.section)
                 {
                     case 'articles'     :$scope.my_articles($scope.pageparm);break;
                     case 'addarticle'   :$scope.showarticleform();break;
                     case 'editarticle'  :$scope.editarticle($scope.pageparm);break;
                     case 'blogs'        :$scope.my_blogs($scope.pageparm);break;
                     case 'addpost'      :$scope.showeditpostform();break;
                     case 'editpost'     :$scope.editpost($scope.pageparm);break;

                     case 'products'     :$scope.my_products($scope.pageparm);break;
                     case 'addproduct'   :$scope.showeditproductform();break;
                     case 'editproduct'  :$scope.editproduct($scope.pageparm);break;

                     case 'events'       :$scope.my_events($scope.pageparm);break;
                     case 'addevent'     :$scope.showaddeventform();break;
                     case 'editevent'    :$scope.editevent($scope.pageparm);break;
                     case 'classifieds'   :$scope.my_classifieds($scope.pageparm);break;
                     case 'addclassified' :$scope.showclassifiedform();break;
                     case 'editclassified':$scope.editclassified($scope.pageparm);break;
                     case 'addgroup'     :$scope.showaddgroup();break;
                     case 'groups'       :$scope.my_groups($scope.pageparm);break;
                     case 'editgroup'    :$scope.editgroup($scope.pageparm);break;
                     case 'forum'      :$scope.my_forumstopics($scope.pageparm);break;
                     case 'addforum'   :$scope.showaddforumtopic();break;
                     case 'editforum'   :$scope.editforumtopic($scope.pageparm);break;




                     case 'forumpost'  :$scope.my_forumsposts($scope.pageparm);break;
                     case 'addforumpost'  :$scope.showaddforumpost();break;
                     case 'editforumpost'  :$scope.editforumpost($scope.pageparm);break;

                     default :{
                       //  $state.go('dashboardsection',{section:'articles'});
                         //      $scope.section='articles';
                                $scope.my_articles(1);

                               }
                               break;
                 }
            }

            else {
                $scope.my_articles(1);

            }
         }




    }]);



})();
