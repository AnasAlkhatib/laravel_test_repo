/**
 * Created by anas on 4/20/17.
 */
// public/scripts/app.js

(function() {

    'use strict';

    angular
        .module('fadApp', ['ngResource','ngRoute','ngMessages', 'ngAnimate', 'toastr', 'ui.router', 'satellizer',
                           'fadNavServices','fadBlogsServices','fadArticlesServices','fadClassifiedsService',
                           'fadEventsService','fadGroupsService','fadStoreService','fadProfessionalsService',
                           'fadForumsServices','fadInboxServices','fadAccountServices','fadAuthServices',
                           'fadUtilitiesServices','fadMyprofileServices','fadCategoriesServices','fadQuestionsService','fadActivationServices','ngCkeditor','ngMaterial'])
        .config(function($stateProvider, $urlRouterProvider, $authProvider,$httpProvider, $provide,$locationProvider,toastrConfig,$routeProvider) {

            angular.extend(toastrConfig, {
                autoDismiss: false,
                containerId: 'toast-container',
                maxOpened: 0,
                newestOnTop: true,
                positionClass: 'toast-top-right',
                preventDuplicates: false,
                preventOpenDuplicates: false,
                closeButton: true,
                allowHtml: true,
                // target: 'body'
            });



         /*   $routeProvider
                .when('/activateaccount', {
                    templateUrl: '../views/activateView.html',
                    controller: 'ActivateController as activate',
                })
                .otherwise({
                    redirectTo: '/'
                });

*/
          // $locationProvider.html5Mode(true);

            /**
             * Helper auth functions
             */
            var skipIfLoggedIn = function($q, $auth) {
                var deferred = $q.defer();
                if ($auth.isAuthenticated()) {
                    deferred.reject();
                } else {
                    deferred.resolve();
                }
                return deferred.promise;
            };

            var loginRequired = function($q, $location, $auth) {
                var deferred = $q.defer();
                if ($auth.isAuthenticated()) {
                    deferred.resolve();
                } else {

                    var myEl = angular.element( document.querySelector( '#authmodal' ) ).addClass('is-visible');
                    angular.element( document.querySelector( '#cd-login' ) ).addClass('is-visible is-selected');
                    angular.element( document.querySelector( '#login_switcher' ) ).addClass('is-selected selected');
                  //  $location.path('/');

                }
                return deferred.promise;
            };

            // use the HTML5 History API
        /*    $locationProvider.html5Mode({
                enabled: true,
                requireBase: true
            });
*/
            // enable html5Mode for pushstate ('#'-less URLs)
           // $locationProvider.html5Mode(true);
            //$locationProvider.hashPrefix('!');

            function redirectWhenLoggedOut($q, $injector) {



                return {

                    responseError: function(rejection) {

                        // Need to use $injector.get to bring in $state or else we get
                        // a circular dependency error
                        var $state = $injector.get('$state');

                        // Instead of checking for a status code of 400 which might be used
                        // for other reasons in Laravel, we check for the specific rejection
                        // reasons to tell us if we need to redirect to the login state
                        var rejectionReasons = ['token_not_provided', 'token_expired', 'token_absent', 'token_invalid'];

                        // Loop through each rejection reason and redirect to the login
                        // sate if one is encountered
                        angular.forEach(rejectionReasons, function(value, key) {

                            if(rejection.data.error === value) {

                                // If we get a rejection corresponding to one of the reasons
                                // in our array, we know we need to authenticate the user so
                                // we can remove the current user from local storage
                                localStorage.removeItem('user');

                               // $state.go('/', null, {reload: true});
                                // Send the user to the auth state so they can login
                                 $state.go('auth');
                            }
                        });

                        return $q.reject(rejection);
                    }
                }
            }

            // Setup for the $httpInterceptor
            $provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);

            // Push the new factory onto the $http interceptor array
            $httpProvider.interceptors.push('redirectWhenLoggedOut');


            // Satellizer configuration that specifies which API
            // route the JWT should be retrieved from
            $authProvider.loginUrl = 'api/v1/auth/auth';

            // Redirect to the auth state if any other states
            // are requested other than users
            $urlRouterProvider.otherwise('/');

            // Controller
            /*$rootScope.isAuthenticated = function() {
                return $auth.isAuthenticated();
            };

             if (!isAuthenticated())  $state.go('/');*/

         /*   function configureRoutes( $routeProvider ) {
                $routeProvider
                    .when( "/activate", {} )

                ;
            }

*/



            $stateProvider
                .state('/', {
                    url: '/',
                    templateUrl: '../views/indexView.html',
                    controller: 'IndexController as index'
                })
                .state('activate', {
                    url: '/activate/:token/:email',
                    templateUrl: '../views/activateView.html',
                    controller: 'ActivateController as activate',
                    resolve: {
                        skipIfLoggedIn: skipIfLoggedIn
                    }
                })
                .state('resetpassword', {
                    url: '/resetpassword/:token',
                    templateUrl: '../views/resetpasswordView.html',
                    controller: 'ResetPasswordController as resetpassword',
                    resolve: {
                        skipIfLoggedIn: skipIfLoggedIn
                    }
                })
                .state('activateaccount', {
                    url: '/activateaccount',
                    templateUrl: '../views/activateView.html',
                    controller: 'ActivateController as activate',
                    resolve: {

                    }
                })
                .state('register', {
                    url: '/register',
                    templateUrl: '../views/registerView.html',
                    controller: 'RegisterController as register',
                    resolve: {
                        skipIfLoggedIn: skipIfLoggedIn
                    }
                })
                .state('myprofile', {
                    url: '/myprofile',
                    templateUrl: '../views/myprofileView.html',
                    controller: 'MyprofileController as myprofile',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('manageprofile', {
                    url: '/manageprofile/:section/:page',
                    templateUrl: '../views/manageprofileView.html',
                    controller: 'ManageprofileController as manageprofile',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('profile', {
                    url: '/profile/:profileid',
                    templateUrl: '../views/profileView.html',
                    controller: 'profileController as profile',
                    resolve:
                        {
                          loginRequired: loginRequired
                        }
                })
                .state('events', {
                    url: '/events/:page',
                    templateUrl: '../views/eventsView.html',
                    controller: 'EventsController as events',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('eventdetails', {
                    url: '/event/:eventid',
                    templateUrl: '../views/eventView.html',
                    controller: 'EventController as event',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('groups', {
                    url: '/groups/:page',
                    templateUrl: '../views/groupsView.html',
                    controller: 'GroupsController as groups',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('groupdetails', {
                    url: '/group/:groupid',
                    templateUrl: '../views/groupView.html',
                    controller: 'GroupController as group',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('forums', {
                    url: '/forums',
                    templateUrl: '../views/forumsView.html',
                    controller: 'ForumsController as forums',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('forumdetails', {
                    url: '/forum/:forumid/:page',
                    templateUrl: '../views/forumView.html',
                    controller: 'ForumController as forum',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('topicdetails', {
                    url: '/forums/topic/{topicid:[0-9]+}/:page',
                    templateUrl: '../views/forumtopicView.html',
                    controller: 'ForumtopicController as forumtopic',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('postdetails', {
                    url: '/forums/post/:postid',
                    templateUrl: '../views/forumpostView.html',
                    controller: 'ForumPostController as forumpost',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('classifieds', {
                    url: '/classifieds/:page',
                    templateUrl: '../views/classifiedsView.html',
                    controller: 'ClassifiedsController as classifieds',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('classifieddetails', {
                    url: '/classified/:classifiedID',
                    templateUrl: '../views/classifiedpageView.html',
                    controller: 'ClassifiedController as classified',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('questions', {
                    url: '/questions/:page',
                    templateUrl: '../views/questionsView.html',
                    controller: 'QuestionsController as questions',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('questiondetails', {
                    url: '/question/:id',
                    templateUrl: '../views/questionpageView.html',
                    controller: 'QuestionController as question',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('blogs', {
                    url: '/blogs/:page',
                    templateUrl: '../views/blogsView.html',
                    controller: 'BlogsController as blogs',
                    resolve: {

                    }
                })
                .state('blogpostdetails', {
                    url: '/blog/:blogid',
                    templateUrl: '../views/blogView.html',
                    controller: 'BlogController as blog',
                    resolve: {

                    }
                })
                .state('articles', {
                    url: '/articles/:page',
                    templateUrl: '../views/articlesView.html',
                    controller: 'ArticlesController as articles',
                    resolve: {

                    }
                })
                .state('articledetails', {
                    url: '/article/:articleid',
                    templateUrl: '../views/articleView.html',
                    controller: 'ArticleController as article',
                    resolve: {

                    }
                })
                .state('store', {
                    url: '/store/:page',
                    templateUrl: '../views/storeView.html',
                    controller: 'StoreController as store',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('productdetails', {
                    url: '/product/:productid',
                    templateUrl: '../views/productView.html',
                    controller: 'ProductController as product',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('inbox', {
                    url: '/inbox',
                    templateUrl: '../views/inboxView.html',
                    controller: 'InboxController as inbox',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('inboxthread', {
                    url: '/inbox/:thread',
                    templateUrl: '../views/inboxView.html',
                    controller: 'InboxController as inbox',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('newthread', {
                    url: '/inbox/:section/:thread',
                    templateUrl: '../views/inboxView.html',
                    controller: 'InboxController as inbox',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('login', {
                    url: '/login',
                    templateUrl: '../views/authView.html',
                    controller: 'AuthController'
                    /*resolve: {
                        skipIfLoggedIn: skipIfLoggedIn
                    }*/
                })
                .state('professionals', {
                    url: '/professionals/:page',
                    templateUrl: '../views/professionalsView.html',
                    controller: 'ProfessionalController as professional',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('dashboard', {
                    url: '/dashboard',
                    templateUrl: '../views/dashboardView.html',
                    controller: 'DashboardController as dashboard',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('settings', {
                url: '/settings',
                templateUrl: '../views/settingsView.html',
                controller: 'SettingsController as settings',
                resolve: {
                    loginRequired: loginRequired
                }
               })
                .state('settingssection', {
                    url: '/settings/:section',
                    templateUrl: '../views/settingsView.html',
                    controller: 'SettingsController as settings',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('dashboardsection', {
                    url: '/dashboard/:section/:page',
                    templateUrl: '../views/dashboardView.html',
                    controller: 'DashboardController as dashboard',
                    resolve: {
                        loginRequired: loginRequired
                    }
                })
                .state('contactus', {
                    url: '/contactus',
                    templateUrl: '../views/contactusView.html',
                    controller: 'ContactusController as contactus',
                    resolve: {

                }
            }).state('features', {
                url: '/features',
                templateUrl: '../views/featuresView.html',
                controller: 'FeaturesController as features',
                resolve: {

                }
            }).state('disclaimer', {
                url: '/disclaimer',
                templateUrl: '../views/disclaimerView.html',
                controller: 'DisclaimerController as disclaimer',
                resolve: {

                }
            }).state('privacyandpolicy', {
                url: '/privacyandpolicy',
                templateUrl: '../views/privacyandpolicyView.html',
                controller: 'PrivacyandpolicyController as privacyandpolicy',
                resolve: {

                }
            }).state('termsandconditions', {
                url: '/termsandconditions',
                templateUrl: '../views/termsandconditionsView.html',
                controller: 'TermsandconditionsController as termsandconditions',
                resolve: {

                }
            }).state('missionandvission', {
                url: '/missionandvission',
                templateUrl: '../views/missionandvissionView.html',
                controller: 'MissionandvissionController as missionandvission',
                resolve: {

                }
            });
        })
        .run(function($rootScope, $state,$auth) {

            var user=JSON.parse(localStorage.getItem('faduser'));





            if(user){
                $rootScope.authenticated = true;
                $rootScope.username = user.name;
                $rootScope.userrole = user.role;
                $rootScope.currentUser = user;
                $rootScope.flipUser = user.flipprofile;

            }
            else
            {
                $rootScope.authenticated = false;
                $rootScope.userrole ='';
                $rootScope.username = '';
                $rootScope.currentUser = '';
                $rootScope.flipUser = '';


            }



            // if($state.current.name === 'profile') alert('asdas');

            // $stateChangeStart is fired whenever the state changes. We can use some parameters
            // such as toState to hook into details about the state as it is changing
            $rootScope.$on('$stateChangeStart', function(event, toState) {




                // Grab the user from local storage and parse it to an object
               // var user =localStorage.getItem('user');




                var user=JSON.parse(localStorage.getItem('faduser'));

                if(user){
                    $rootScope.authenticated = true;
                    $rootScope.username = user.name;
                    $rootScope.userrole = user.role;
                    $rootScope.currentUser = user;

                }
                else
                {
                    $rootScope.authenticated = false;
                }


                // If there is any user data in local storage then the user is quite
                // likely authenticated. If their token is expired, or if they are
                // otherwise not actually authenticated, they will be redirected to
                // the auth state because of the rejected request anyway
                /*if(user) {

                    user = JSON.parse(localStorage.getItem('user'));
                    // The user's authenticated state gets flipped to
                    // true so we can now show parts of the UI that rely
                    // on the user being logged in
                    $rootScope.authenticated = true;

                    // Putting the user's data on $rootScope allows
                    // us to access it anywhere across the app. Here
                    // we are grabbing what is in local storage
                    $rootScope.currentUser = user;

                    // If the user is logged in and we hit the auth route we don't need
                    // to stay there and can send the user to the main state
                    if(toState.name === "auth") {

                        // Preventing the default behavior allows us to use $state.go
                        // to change states
                        event.preventDefault();

                        // go to the "main" state which in our case is users
                        $state.go('myprofile');
                    }
                }
                */
            });
        });
})();