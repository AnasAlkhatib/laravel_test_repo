/**
 * Created by anas on 4/20/17.
 */

// public/scripts/StoreController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('ProductController',['storeService','$scope','$sce','forumsService','$stateParams','$state','toastr',
            function (storeService,$scope, $sce,forumsService,$stateParams,$state,toastr) {


            $scope.trusthtml = function(text) {
                return $sce.trustAsHtml(text);
            };


            $scope.sidebar_latestforums=function()
            {

                    angular.element(document).find("#latestforums").LoadingOverlay("show");
                    $scope.sidebarlatestposts=forumsService.getlatestposts(4,1,
                            function(response)
                            {
                                switch (response.status)
                                {
                                    case 401:console.log('Credentials Incorrect');break;
                                    case 200: $scope.sidebarlatestposts=response.data.data;break;
                                }
                                angular.element(document).find("#latestforums").LoadingOverlay("hide");
                            },
                            function(error)
                            {
                                switch (error.status)
                                {
                                    case 401: console.log('Credentials Incorrect'); break;
                                    case 422: console.log('Validation Error '+response.message); break;
                                    default: console.log(error.status+'Server Error Check Again please !'); break;
                                }
                                angular.element(document).find("#latestforums").LoadingOverlay("hide");
                            }
                        );

                }


            $scope.sidebar_latestproducts=function()
            {

                angular.element(document).find("#latestproducts").LoadingOverlay("show",{image:"",fontawesome : "fa fa-spinner fa-spin "});

                    $scope.sidebarlatestproducts=storeService.getlatestproducts('small',9,0, $scope.productid,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401:console.log('Credentials Incorrect');break;
                                case 200: $scope.sidebarlatestproducts=response.data.data;break;
                            }
                            angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                        },
                        function(error)
                        {
                            switch (error.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422:console.log('Validation Error '+response.message); break;
                                default: console.log(error.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                        }
                    );
            }

            $scope.getproduct = function (productid)
            {
                angular.element(document).find("#containerpart").LoadingOverlay("show");
                    $scope.mainproduct=storeService.getproduct(productid,
                        function(response)
                        {
                            switch (response.status)
                            {

                                case 401:toastr.error('Credentials Incorrect');break
                                case 200:{
                                    $scope.mainproduct=response.data.data;


                                    if($scope.mainproduct==null) {
                                        toastr.error('Not Found');
                                        $state.go('store');
                                    }

                                }break;

                            }
                            angular.element(document).find("#containerpart").LoadingOverlay("hide");
                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#containerpart").LoadingOverlay("hide");
                            $state.go('store');
                        }
                    );
                }

            $scope.initproductcontr=function()
            {
                if($stateParams.productid!=null && $stateParams.productid!=undefined &&  $stateParams.productid!='')
                {
                    $scope.productid=$stateParams.productid;
                    $scope.getproduct($stateParams.productid);
                   // console.log($scope.mainproduct);

                }
                else
                    $state.go('store');
            };


        }]);

})();
