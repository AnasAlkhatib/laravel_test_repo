/**
 * Created by anas on 4/20/17.
 */

// public/scripts/authController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('QuestionsController',['questionsService','$scope','$sce','$routeParams','forumsService',
                    'articlesService','blogsService','storeService','$stateParams','$rootScope','toastr',
            function (questionsService,$scope, $sce,$routeParams,forumsService,articlesService,blogsService,storeService,
                      $stateParams,$rootScope,toastr) {




            $scope.trusthtml = function(text) {
                return $sce.trustAsHtml(text);
            };


            $scope.sidebar_latestarticles=function()     {
                    angular.element(document).find("#latestarticles").LoadingOverlay("show");

                    $scope.sidebarlatestarticles=articlesService.getlatestarticles('small',4,0,0,
                        function(response){
                            switch (response.status)
                            {
                                case 401:console.log('Credentials Incorrect');break;
                                case 200: $scope.sidebarlatestarticles=response.data.data;break;
                            }
                            angular.element(document).find("#latestarticles").LoadingOverlay("hide");
                        },
                        function(error)
                        {
                            switch (error.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422: console.log('Validation Error '+response.message); break;
                                default: console.log(error.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestarticles").LoadingOverlay("hide");
                        }
                    );
                }

            $scope.sidebar_latestblogs=function()        {
                    angular.element(document).find("#latestblogs").LoadingOverlay("show");

                    $scope.sidebarlatestblogs=blogsService.getlatestblogposts(4,0,0,0,
                        function(response){
                            switch (response.status)

                            {
                                case 401:console.log('Credentials Incorrect');break;
                                case 200:$scope.sidebarlatestblogs=response.data.data;break;
                            }
                            angular.element(document).find("#latestblogs").LoadingOverlay("hide");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422: console.log('Validation Error '+response.message); break;
                                default: console.log(response.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestblogs").LoadingOverlay("hide");
                        }
                    );
                }

            $scope.sidebar_latestproducts=function()     {
                    angular.element(document).find("#latestproducts").LoadingOverlay("show");

                    if($rootScope.isAuthenticated()==1)
                        $scope.sidebarlatestproducts=storeService.getproducts('small',9,0,
                            function(response)
                            {
                                switch (response.status)
                                {
                                    case 401:console.log('Credentials Incorrect');break;
                                    case 200:$scope.sidebarlatestproducts=response.data.data;break;
                                }
                                angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                            },
                            function(error)
                            {
                                switch (error.status)
                                {
                                    case 401: console.log('Credentials Incorrect'); break;
                                    case 422: console.log('Validation Error '+response.message); break;
                                    default : console.log(error.status+'Server Error Check Again please !'); break;
                                }
                                angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                            }
                        );
                    else
                    {
                        $scope.sidebarlatestproducts =0;
                        angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                    }


                }

            $scope.getquestions = function (page)    {

                 angular.element(document).find("#containerpart").LoadingOverlay("show");
                $scope.questions=questionsService.getquestions('big',6,page,
                    function(response){
                        switch (response.status) {

                            case 401:toastr.error('Credentials Incorrect');break;
                            case 200:{

                                $scope.questions=response.data.data;
                                $scope.pagination=response.data.meta.pagination;

                            }break;
                        }
                        angular.element(document).find("#containerpart").LoadingOverlay("hide");
                    },
                    function(error)
                    {
                        switch (error.status)
                        {
                            case 401: toastr.error('Credentials Incorrect'); break;
                            case 422: toastr.error('Validation Error '+error.message); break;
                            default: toastr.error(error.status+'Server Error Check Again please !'); break;
                        }
                        angular.element(document).find("#containerpart").LoadingOverlay("hide");
                    }
                );


            }


            $scope.initquestionscontr=function(){

                    $scope.pageparm= 1;

                    if($stateParams.page!=null && $stateParams.page!=undefined &&  $stateParams.page!='')
                        $scope.pageparm= $stateParams.page

                    $scope.prepageparm=parseInt($scope.pageparm)-1;
                    $scope.nextpageparm=parseInt($scope.pageparm)+1;
                    $scope.getquestions($scope.pageparm);

            };


        }]);
})();
