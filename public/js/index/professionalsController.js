/**
 * Created by anas on 4/20/17.
 */

// public/scripts/authController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('ProfessionalController',['professionalsService','$scope','$sce','$stateParams','toastr',
                    function (professionalsService,$scope, $sce,$stateParams,toastr)
                    {





            $scope.trusthtml = function(text)
            {
                return $sce.trustAsHtml(text);
            };

            $scope.getmembers = function (page)            {
                $scope.professionalsError='';
                angular.element(document).find("#containerpart").LoadingOverlay("show");

                $scope.homememebrs=professionalsService.getprofessionals('big',8,page,
                    function(response)
                    {
                        switch (response.status)
                        {
                            case 401:toastr.error('Credentials Incorrect'); break;
                            case 200:
                                {
                                 $scope.homememebrs=response.data.data;
                                 $scope.pagination=response.data.meta.pagination;
                                 }break;
                        }
                        angular.element(document).find("#containerpart").LoadingOverlay("hide");
                    },
                    function(error)
                    {
                        switch (error.status)
                        {
                            case 401:toastr.error('Credentials Incorrect'); break;
                            case 422: toastr.error('Validation Error '+error.message); break;
                            default:
                                {
                                    toastr.error( 'Error happened reload page please');

                                } break;
                        }
                        angular.element(document).find("#containerpart").LoadingOverlay("hide");
                    }
                );


            }

            $scope.searchmembers = function (page)  {


                            angular.element(document).find("#containerpart").LoadingOverlay("show");

                            $scope.task='search';

                            var searchterm = {
                                speciality   :angular.element(document).find("#speciality").val(),
                                searchterm   :angular.element(document).find("#searchterm").val(),
                                sex: (angular.element(document).find("#professional_sex_male").prop('checked')==true)? 'male':
                                     (angular.element(document).find("#professional_sex_female").prop('checked')==true) ? 'female':'',

                            }



                            $scope.homememebrs=professionalsService.searchprofessionals(8,page,searchterm,
                                function(response)
                                {
                                    switch (response.status)
                                    {
                                        case 401:toastr.error('Credentials Incorrect'); break;
                                        case 200:
                                        {
                                            $scope.homememebrs=response.data.data;
                                            $scope.pagination=response.data.meta.pagination;
                                            console.log($scope.memebrs);
                                        }break;
                                    }
                                    angular.element(document).find("#containerpart").LoadingOverlay("hide");
                                },
                                function(error)
                                {
                                    switch (error.status)
                                    {
                                        case 401:toastr.error('Credentials Incorrect'); break;
                                        case 422: toastr.error('Validation Error '+error.message); break;
                                        default:
                                        {
                                            toastr.error( 'Error happened reload page please');
                                            console.log(error.status+'Server Error Check Again please !');
                                        } break;
                                    }
                                    angular.element(document).find("#containerpart").LoadingOverlay("hide");
                                }
                            );


                        }


            $scope.initprofessionalscontr=function()
            {

                $scope.professionalsError='';
                $scope.task='';

                $scope.pageparm= 1;
                if($stateParams.page!=null && $stateParams.page!=undefined &&  $stateParams.page!='')
                    $scope.pageparm= $stateParams.page

                $scope.prepageparm=parseInt($scope.pageparm)-1;
                $scope.nextpageparm=parseInt($scope.pageparm)+1;
                $scope.getmembers($scope.pageparm);


            };


        }]);
})();
