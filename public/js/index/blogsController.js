/**
 * Created by anas on 4/20/17.
 */

// public/scripts/BlogsController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('BlogsController',['blogsService','$scope','$sce','$stateParams','toastr',
            function (blogsService,$scope, $sce,$stateParams,toastr) {



        $scope.trusthtml = function(text) {
            return $sce.trustAsHtml(text);
        };

        $scope.getblogposts = function (page)   {

            angular.element(document).find("#containerpart").LoadingOverlay("show", {image:"",fontawesome : "fa fa-spinner fa-spin"});
            $scope.blogposts=blogsService.getblogs2('big',8,page,
                function(response)
                {
                    switch (response.status)
                    {
                        case 401:toastr.error('Credentials Incorrect'); break;
                        case 200:
                            {
                             $scope.blogposts=response.data.data;
                             $scope.pagination=response.data.meta.pagination;
                            }
                            break;
                    }
                  angular.element(document).find("#containerpart").LoadingOverlay('hide');
                }
                ,
                function(error)
                {
                    switch (error.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Validation Error '+response.message); break;
                        default: toastr.error(error.status+'Server Error Check Again please !'); break;
                    }
                    angular.element(document).find("#containerpart").LoadingOverlay("hide");
                });

        }

        $scope.initblogcontr=function(){

            $scope.pageparm= 1;

                if($stateParams.page!=null && $stateParams.page!=undefined && $stateParams.page!='')
                    $scope.pageparm= $stateParams.page

                $scope.prepageparm=parseInt($scope.pageparm)-1;
                $scope.nextpageparm=parseInt($scope.pageparm)+1;
                $scope.getblogposts($scope.pageparm);


      };


    }]);

})();
