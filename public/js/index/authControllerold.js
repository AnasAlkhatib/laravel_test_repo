
/**
 * Created by anas on 4/20/17.
 */

// public/scripts/authController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('AuthControllerold', ['$scope','$auth', 'accountServices',AuthControllerold]);

    function AuthControllerold($auth, $state,accountServices) {




        var vm = this;

        vm.loginError = false;
        vm.loginErrorText='';

        vm.login = function() {


            var credentials = {
                email: vm.email,
                password: vm.password
            }



            if(credentials.email==null || credentials.password==null ) {
                vm.loginError = true;
                vm.loginErrorText='email and password required';
                return;
            }


            $auth.login(credentials).then(

                function(response) {

                // Return an $http request for the now authenticated
                // user so that we can flatten the promise chain
                // return $http.get('api/authenticate/user');
               // $state.go('home', {});
                var user =response.data.data;
                //var token = JSON.stringify(response.data.data.token);
                //console.log('cxvcxv');



                // Set the stringified user data into local storage
                localStorage.setItem('user', user);
                localStorage.setItem('token', user.token);

                // The user's authenticated state gets flipped to
                // true so we can now show parts of the UI that rely
                // on the user being logged in
                $rootScope.authenticated = true;


                // Putting the user's data on $rootScope allows
                // us to access it anywhere across the app
                $rootScope.currentUser = response.data.data;

                    accountServices.getme();
                // Everything worked out so we can now redirect to
                // the users state to view the data
                  $state.go('home');

                // Handle errors
            }) .catch(function(error) {
                vm.loginError = true;
                vm.loginErrorText = error.data.error;
                // Handle errors here, such as displaying a notification
                // for invalid email and/or password.
            });

            /*, function(error) {
                vm.loginError = true;
                vm.loginErrorText = error.data.error;

                // Because we returned the $http.get request in the $auth.login
                // promise, we can chain the next promise to the end here
            }).then(function(response) {

                // Stringify the returned data to prepare it
                // to go into local storage


                 var user = JSON.stringify(response.data);

                //console.log('cxvcxv');

                 alert(user);

                // Set the stringified user data into local storage
                localStorage.setItem('user', user);

                // The user's authenticated state gets flipped to
                // true so we can now show parts of the UI that rely
                // on the user being logged in
                $rootScope.authenticated = true;


                // Putting the user's data on $rootScope allows
                // us to access it anywhere across the app
                $rootScope.currentUser = response.data.data;

                // Everything worked out so we can now redirect to
                // the users state to view the data
              //  $state.go('home');
            });*/
        }


        vm.logout = function() {

            $auth.logout().then(function() {

                // Remove the authenticated user from local storage
                localStorage.removeItem('user');
                localStorage.removeItem('faduser');

                // Flip authenticated to false so that we no longer
                // show UI elements dependant on the user being logged in
                $rootScope.authenticated = false;

                // Remove the current user info from rootscope
                $rootScope.currentUser = null;
            });

            $state.go('/');
        }
    }

})();

