
/**
 * Created by anas on 4/20/17.
 */

// public/scripts/authController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('AuthController',  function($scope, $state, $auth, toastr,accountServices,activationServices ) {

            $scope.resetpassword = function() {

                angular.element(document).find("#cd-reset-password").LoadingOverlay("show");


                if( angular.element(document).find("#reset-email").val()=="" )
                {   toastr.error('Type Your Email!');
                    angular.element(document).find("#cd-reset-password").LoadingOverlay("hide");
                    return false;
                }


                $scope.activate= activationServices.resetpassword(angular.element(document).find("#reset-email").val(),
                    function(response)
                    {
                        switch (response.status)
                        {
                            case 401:toastr.error('Email Not Found!');break;
                            case 202: {

                                toastr.success('An Email has been sent ,Check your mail inbox');

                            }break;
                        }
                        angular.element(document).find("#cd-reset-password").LoadingOverlay("hide");
                    },
                    function(error)
                    {
                        switch (error.status)
                        {
                            case 401:  toastr.error('Email Not Found!'); break;
                            default:   toastr.error('Server Error Check Again please !'); break;
                        }

                        angular.element(document).find("#cd-reset-password").LoadingOverlay("hide");

                    }
                );


            };

            $scope.resendactivationlink = function() {

                angular.element(document).find("#cd-resend-activation").LoadingOverlay("show");


                if( angular.element(document).find("#resend-activation-email").val()=="" )
                {   toastr.error('Type Your Email!');
                    angular.element(document).find("#cd-resend-activation").LoadingOverlay("hide");
                    return false;
                }


                $scope.activate= activationServices.resendactivationlink(angular.element(document).find("#resend-activation-email").val(),
                    function(response)
                    {
                        switch (response.status)
                        {
                            case 401:toastr.error('Email Not Found!');break;
                            case 202: {

                                toastr.success('An Email has been sent ,Check your mail inbox');

                            }break;
                        }
                        angular.element(document).find("#cd-resend-activation").LoadingOverlay("hide");
                    },
                    function(error)
                    {
                        switch (error.status)
                        {
                            case 401:  toastr.error('Email Not Found!'); break;
                            default:   toastr.error('Server Error Check Again please !'); break;
                        }

                        angular.element(document).find("#cd-resend-activation").LoadingOverlay("hide");

                    }
                );


            };

            $scope.login = function() {

                var credentials = {
                    login: $scope.email,
                    password: $scope.password
                }

                angular.element(document).find("#cd-login").LoadingOverlay("show");
                $auth.login(credentials)
                    .then(function(response) {

                        angular.element(document).find("#cd-login").LoadingOverlay("hide");

                         var myEl = angular.element( document.querySelector( '#logintab' ) );
                        myEl.removeClass('is-visible');

                        var myEl = angular.element( document.querySelector( '#logintab' ) );
                        myEl.addClass('cd-user-modal');

                        var myEl = angular.element( document.querySelector( '#authmodal' ) );
                        myEl.removeClass('is-visible');

                       localStorage.setItem('faduser',JSON.stringify(response.data.user));

                      $state.go($state.current, null, {reload: true});
                        //$state.go('/');
                    })
                    .catch(function(error) {

                        angular.element(document).find("#cd-login").LoadingOverlay("hide");
                        switch (error.status)
                        {

                            case 401: toastr.error('Credentials Incorrect'); break;
                            case 422: toastr.error('Validation Error'); break;
                            case 423: toastr.error('Account not active ,activate your account please by clicking on the link that was sent to your email '); break;
                            default: toastr.error(error.status+'Server Error Check Again please !'); break;
                        }
                    });
            };

            $scope.register = function() {


                angular.element(document).find("#cd-signup").LoadingOverlay("show");

                var user = {
                    name       :angular.element(document).find("#register-username").val(),
                    email      :angular.element(document).find("#register-email").val(),
                    password   :angular.element(document).find("#register-password").val(),
                    speciality :angular.element(document).find("#register-speciality").val(),
                    aboutme :angular.element(document).find("#register-aboutme").val(),
                    description :angular.element(document).find("#register-description").val(),
                    city       :angular.element(document).find("#register-city").val(),
                    country    :angular.element(document).find("#register-country").val(),
                    terms      :(angular.element(document).find("#register-accept-terms").is(':checked'))? 1:0,
                }

                

                if( user.terms==0)
                {   toastr.error('You Have to Accept our terms ');
                    angular.element(document).find("#cd-signup").LoadingOverlay("hide");
                    return false;
                }

                if( user.name=="" || user.email==""||user.password=="" || user.city==""||user.country=="" || user.speciality==""||user.aboutme=="" || user.description=="")
                {   toastr.error('Missing Information');
                    angular.element(document).find("#cd-signup").LoadingOverlay("hide");
                    return false;
                }

                accountServices.registeruser(user,
                    function(response){


                        switch (response.status) {

                            case 401:toastr.error('Credentials Incorrect');break;
                            case 202:toastr.error('Error Happened');break;
                            case 200:{
                                toastr.success('You have succesfully registered and an Actvation link was send to you email');
                                $state.go('/',{},{reload:true});
                            }break;
                        }
                        angular.element(document).find("#cd-signup").LoadingOverlay("hide");

                    },
                    function(response)
                    {
                        switch (response.status)
                        {
                            case 401: toastr.error('Credentials Incorrect'); break;
                            case 422: {

                                var  result=response.data.errors;

                                var error_desc = " Please Fix The Following Errors : "+"\n";

                                for (var property1 in result) {
                                    error_desc = error_desc + result[property1]+" \n ";
                                }

                                toastr.error(error_desc);

                            }break;

                            default : toastr.error('Server Error Check Again please !'+response); break;
                        }
                        // toastr.error(response);
                        angular.element(document).find("#cd-signup").LoadingOverlay("hide");

                    }
                );




            };

            $scope.authenticate = function(provider) {
                $auth.authenticate(provider)
                    .then(function() {
                        toastr.success('You have successfully signed in with ' + provider + '!');
                        $state.go('/');
                    })
                    .catch(function(error) {
                        if (error.message) {
                            // Satellizer promise reject error.
                            toastr.error(error.message);
                        } else if (error.data) {
                            // HTTP response error from server
                            toastr.error(error.data.message, error.status);
                        } else {
                            toastr.error(error);
                        }
                    });
            };
        });


})();

