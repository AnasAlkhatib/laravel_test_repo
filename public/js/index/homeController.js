/**
 * Created by anas on 4/20/17.
 */

// public/scripts/authController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('HomeController',HomeController);


    function HomeController($auth, $state,$rootScope,$scope) {

       // if ($rootScope.authenticated!=true) $state.go('/', {});

        var vm = this;

        vm.users;
        vm.error;


        vm.sidebar_latestforums=function(){

            $scope.sidebarlatestposts=forumsService.getlatestposts(4,1,
                function(response){
                    switch (response.status) {

                        case 401:alert('Credentials Incorrect');break;
                        case 200:{
                            $scope.sidebarlatestposts=response.data.data;
                            angular.element(document).find("#latestforums").removeClass( "spinner" );
                            // alert('Great! Data Loaded');
                            console.log(response.data.data);
                        }break;

                    }
                },
                function(response){
                    switch (response.status) {

                        case 401:alert('Credentials Incorrect');break;
                        case 422:{

                            alert('Sorry Error Happened');
                            $state.go('forums');

                        }break;

                    }
                }
            );
        }


        // We would normally put the logout method in the same
        // spot as the login method, ideally extracted out into
        // a service. For this simpler example we'll leave it here
        vm.logoutuse = function() {


            $auth.logout().then(function() { alert('asd');

                // Remove the authenticated user from local storage
                localStorage.removeItem('user');

                // Flip authenticated to false so that we no longer
                // show UI elements dependant on the user being logged in
                $rootScope.authenticated = false;

                // Remove the current user info from rootscope
                $rootScope.currentUser = null;
            });

            $state.go('/');
        }



    }

})();
