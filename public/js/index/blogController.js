/**
 * Created by anas on 4/20/17.
 */

// public/scripts/BlogsController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('BlogController',['blogsService','$scope','$sce','$stateParams','forumsService','articlesService',
                    'classifiedsService','storeService','$state','toastr','$rootScope',
            function (blogsService,$scope, $sce,$stateParams,forumsService,articlesService,classifiedsService,
                      storeService,$state,toastr,$rootScope) {



        $scope.trusthtml = function(text) {
            return $sce.trustAsHtml(text);
        };

        $scope.sidebar_latestforums=function()         {

            angular.element(document).find("#latestforums").LoadingOverlay("show");

            if($rootScope.isAuthenticated()==1)
                    $scope.sidebarlatestposts=forumsService.getlatestposts(4,1,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401:console.log('Credentials Incorrect');break;
                                case 200: $scope.sidebarlatestposts=response.data.data;break;
                            }
                            angular.element(document).find("#latestforums").LoadingOverlay("hide");
                        },
                        function(error)
                        {
                            switch (error.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422: console.log('Validation Error '+response.message); break;
                                default: console.log(error.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestforums").LoadingOverlay("hide");
                        }
                    );
            else
            {
                $scope.sidebarlatestposts = 0;
                angular.element(document).find("#latestforums").LoadingOverlay("hide");
            }
        }

        $scope.sidebar_latestarticles=function()       {
            angular.element(document).find("#latestarticles").LoadingOverlay("show");

                    $scope.sidebarlatestarticles=articlesService.getlatestarticles('small',4,0,0,
                        function(response){
                            switch (response.status)
                            {
                                case 401:console.log('Credentials Incorrect');break;
                                case 200: $scope.sidebarlatestarticles=response.data.data;break;
                            }
                            angular.element(document).find("#latestarticles").LoadingOverlay("hide");
                        },
                        function(error)
                        {
                            switch (error.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422: console.log('Validation Error '+response.message); break;
                                default: console.log(error.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestarticles").LoadingOverlay("hide");
                        }
                    );
                }

        $scope.sidebar_latestblogs=function()          {
            angular.element(document).find("#latestblogs").LoadingOverlay("show");

                    $scope.sidebarlatestblogs=blogsService.getlatestblogposts(4,0,$scope.blog_id,'latest',
                        function(response){
                            switch (response.status)
                            {
                                case 401:console.log('Credentials Incorrect');break;
                                case 200:$scope.sidebarlatestblogs=response.data.data;break;
                            }
                            angular.element(document).find("#latestblogs").LoadingOverlay("hide");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422: console.log('Validation Error '+response.message); break;
                                default: console.log(response.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestblogs").LoadingOverlay("hide");
                        }
                    );
                }

        $scope.sidebar_latestclassifieds=function()    {
            angular.element(document).find("#latestclassifieds").LoadingOverlay("show");

             if($rootScope.isAuthenticated()==1)
                    $scope.sidebarlatestclassifieds=classifiedsService.getclassifieds('small',4,0,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: console.log('Credentials Incorrect');break;
                                case 200: $scope.sidebarlatestclassifieds=response.data.data;break;
                            }
                            angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                        },
                        function(error)
                        {
                            switch (error.status)
                            {
                                case 401:console.log('Credentials Incorrect'); break;
                                case 422:console.log('Validation Error '+response.message); break;
                                default: console.log(error.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                        }
                    );
             else
               {
                 $scope.sidebarlatestclassifieds = 0;
                 angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
               }

        }

        $scope.sidebar_latestproducts=function()       {
            angular.element(document).find("#latestproducts").LoadingOverlay("show");

            if($rootScope.isAuthenticated()==1)
                    $scope.sidebarlatestproducts=storeService.getproducts('small',9,0,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401:console.log('Credentials Incorrect');break;
                                case 200:$scope.sidebarlatestproducts=response.data.data;break;
                            }
                            angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                        },
                        function(error)
                        {
                            switch (error.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422: console.log('Validation Error '+response.message); break;
                                default : console.log(error.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                        }
                    );
            else
            {
                $scope.sidebarlatestproducts =0;
                angular.element(document).find("#latestproducts").LoadingOverlay("hide");
            }


        }

        $scope.getblogpost = function (postid)         {

            angular.element(document).find("#containerpart").LoadingOverlay("show");

            $scope.blogpost=blogsService.getblogspost(postid,
                function(response)
                {
                    switch (response.status)
                    {
                        case 401:toastr.error('Credentials Incorrect');break;
                        case 200:$scope.blogpost=response.data.data;break;
                    }
                    angular.element(document).find("#containerpart").LoadingOverlay("hide");
                },
                function(error)
                {
                    switch (error.status)
                    {
                        case 401: toastr.error('Credentials Incorrect'); break;
                        case 422: toastr.error('Validation Error '+error.message); break;
                        default: toastr.error(error.status+'Server Error Check Again please !'); break;
                    }
                    angular.element(document).find("#containerpart").LoadingOverlay("hide");
                }
            );


        }

        $scope.initblogcontr=function(){

            if($stateParams.blogid!=null && $stateParams.blogid!=undefined && $stateParams.blogid!='')
            {
                $scope.blog_id=$stateParams.blogid;
                $scope.getblogpost($stateParams.blogid);
            }

            else
                $state.go('blogs');



      };


    }]);

})();
