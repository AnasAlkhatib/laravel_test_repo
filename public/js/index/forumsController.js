/**
 * Created by anas on 4/20/17.
 */

// public/scripts/ForumsController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('ForumsController',['forumsService','$scope','$sce','toastr',function (forumsService,$scope, $sce,toastr) {


            $scope.statisticsresulterror='';

            $scope.trusthtml = function(text) {
                return $sce.trustAsHtml(text);
            };


            $scope.getforumsstatistics = function ()   {
               ;

                angular.element(document).find("#right_bar").LoadingOverlay("show");

                $scope.sidebarstatistics=forumsService.getforumstatistics(
                    function(response){
                        switch (response.status) {

                            case 401: toastr.error('Credentials Incorrect'); break;
                            case 200:{
                                $scope.sidebarstatistics=response.data.data;

                            }break;
                        }
                        angular.element(document).find("#right_bar").LoadingOverlay("hide");
                    },
                    function(response){
                        switch (response.status)
                        {
                            case 401: toastr.error('Credentials Incorrect'); break;
                            case 422: toastr.error('Not Found'); break;
                            default : toastr.error('Server Error Check Again please !'); break;
                        }
                        angular.element(document).find("#right_bar").LoadingOverlay("hide");
                    }
                );


            }

            $scope.getforums = function () {

                angular.element(document).find("#containerpart").LoadingOverlay("show");

                $scope.homeforums=forumsService.getforums(6,0,
                    function(response){
                        switch (response.status) {

                            case 401: toastr.error('Credentials Incorrect'); break;
                            case 200:{
                                $scope.homeforums=response.data.data;
                                $scope.pagination=response.data.meta;
                            }break;
                        }
                        angular.element(document).find("#containerpart").LoadingOverlay("hide");
                    },
                    function(response){
                        switch (response.status)
                        {
                            case 401: toastr.error('Credentials Incorrect'); break;
                            case 422: toastr.error('Not Found'); break;
                            default : toastr.error('Server Error Check Again please !'); break;
                        }
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                    }
                );


            }

            $scope.initforumscontr=function(){
                $scope.getforums();
                $scope.getforumsstatistics();

            };


        }]);
})();
