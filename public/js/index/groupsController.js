/**
 * Created by anas on 4/20/17.
 */

// public/scripts/GroupsController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('GroupsController',['groupsService','$scope','$sce','$stateParams','toastr',
            function (groupsService,$scope, $sce,$stateParams,toastr) {



            $scope.trusthtml = function(text) {
                return $sce.trustAsHtml(text);
            };


            $scope.getgroups = function (page) {

                angular.element(document).find("#containerpart").LoadingOverlay("show");
                $scope.homegroups=groupsService.getgroups('big',9,page,
                    function(response){
                        switch (response.status) {

                            case 401:toastr.error('Credentials Incorrect');break;
                            case 200:{
                                $scope.homegroups=response.data.data;
                                $scope.pagination=response.data.meta.pagination;
                            }break;
                        }
                        angular.element(document).find("#containerpart").LoadingOverlay("hide");
                    },
                    function(error)
                    {
                        switch (error.status)
                        {
                            case 401: toastr.error('Credentials Incorrect'); break;
                            case 422: toastr.error('Validation Error '+error.message); break;
                            default: toastr.error(error.status+'Server Error Check Again please !'); break;
                        }
                        angular.element(document).find("#containerpart").LoadingOverlay("hide");
                        $state.go('groups');
                    }

                );


            }

            $scope.initgroupscontr=function(){
                $scope.pageparm= 1;

                if($stateParams.page!=null && $stateParams.page!=undefined &&  $stateParams.page!='')
                    $scope.pageparm= $stateParams.page

                $scope.prepageparm=parseInt($scope.pageparm)-1;
                $scope.nextpageparm=parseInt($scope.pageparm)+1;
                $scope.getgroups($scope.pageparm);
            };


        }]);
})();
