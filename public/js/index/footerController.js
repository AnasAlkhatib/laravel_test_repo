/**
 * Created by anas on 4/20/17.
 */

// public/scripts/authController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('FooterController', ['blogsService','$scope','$sce', function(blogsService,$scope, $sce,toastr) {



            $scope.trusthtml = function(text) {
                return $sce.trustAsHtml(text);
            };

            $scope.getposts2 = function () {

                // $scope.post='sss';
                $scope.posts=blogsService.getblogs2('small',3,0,
                    function(response){
                        switch (response.status) {

                            case 401:alert('Credentials Incorrect');break;
                            case 200:{
                                      $scope.posts=response.data.data;
                                     // alert('Great! Data Loaded');
                                      //console.log($scope.posts);
                                      }break;

                        }


                }
               );


            }
            $scope.getposts=function(){

                $scope.footerposts=blogsService.getlatestblogposts(3,0,0,'nothing',
                    function(response){
                        switch (response.status) {

                            case 401:alert('Credentials Incorrect');break;
                            case 200:{
                                $scope.footerposts=response.data.data;
                              //  angular.element(document).find("#latestblogs").removeClass( "spinner_sidebar" );
                                // alert('Great! Data Loaded');

                            }break;

                        }
                    },
                    function(response){
                        switch (response.status) {

                            case 401:alert('Credentials Incorrect');break;
                            case 422:{

                                alert('Sorry Error Happened');
                                $state.go('forums');

                            }break;

                        }
                    }
                );
            }

           // $scope.getposts();
            // alert('asda');
          // console.log($scope.posts);

        }]);


})();
