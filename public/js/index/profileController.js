/**
 * Created by anas on 4/20/17.
 */

// public/scripts/authController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('profileController',['professionalsService','$scope','$sce','$stateParams','utilitiesServices','toastr','$state','$rootScope',
            function (professionalsService,$scope, $sce,$stateParams,utilitiesServices,toastr,$state,$rootScope)
            {

                $scope.getmemberprofile = function (profileid)
                {
                    angular.element(document).find("#containerpart").LoadingOverlay("show");

                    $scope.memberprofile=professionalsService.getprofessional(profileid,
                        function(response){
                            switch (response.status)
                            {
                                case 401:{toastr.error('Credentials Incorrect');  $state.go('professionals');} break;
                                case 200:
                                    {
                                    $scope.memberprofile=response.data.data;
                                    angular.element(document).find("#containerpart").LoadingOverlay("hide");
                                    }break;


                            }
                        },
                        function(error)
                        {
                            angular.element(document).find("#containerpart").LoadingOverlay("hide");
                            switch (error.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default: toastr.error('Server Error Check Again please !'); break;
                            }
                            $state.go('professionals');
                        }
                    );


                }


                $scope.initprofilecontr=function()
                {
                    $scope.myprofile=0;
                    $scope.profileError='';

                    if($stateParams.profileid!=null && $stateParams.profileid!=undefined &&  $stateParams.profileid!='')
                    {
                        if($stateParams.profileid==$rootScope.currentUser.id)  $scope.myprofile=1;
                        $scope.getmemberprofile($stateParams.profileid);
                    }
                    else
                        $state.go('professionals');

                };


            }]);

})();
