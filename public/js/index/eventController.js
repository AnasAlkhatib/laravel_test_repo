/**
 * Created by anas on 4/20/17.
 */

// public/scripts/EventsController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('EventController',['eventsService','$scope','$sce','$stateParams','forumsService','articlesService',
                                       'blogsService','storeService','$state','toastr','$rootScope','classifiedsService',
            function (eventsService,$scope, $sce,$stateParams,forumsService,articlesService,blogsService,storeService,
                      $state,toastr,$rootScope,classifiedsService) {





            $scope.trusthtml = function(text) {
                return $sce.trustAsHtml(text);
            };

            $scope.sidebar_latestforums=function(){

                    angular.element(document).find("#latestforums").LoadingOverlay("show");

                    $scope.sidebarlatestposts=forumsService.getlatestposts(4,1,
                        function(response){
                            switch (response.status) {

                                case 401:console.log('Credentials Incorrect'); break;
                                case 200:{
                                    $scope.sidebarlatestposts=response.data.data;

                                }break;

                            }
                            angular.element(document).find("#latestforums").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (error.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422: console.log('Validation Error '+response.message); break;
                                default : console.log(error.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestforums").LoadingOverlay("hide");
                        }
                    );
                }

            $scope.sidebar_latestarticles=function()
                {
                    angular.element(document).find("#latestarticles").LoadingOverlay("show");

                    $scope.sidebarlatestarticles=articlesService.getlatestarticles('small',4,0,0,
                        function(response){
                            switch (response.status)
                            {
                                case 401:console.log('Credentials Incorrect');break;
                                case 200: $scope.sidebarlatestarticles=response.data.data;break;
                            }
                            angular.element(document).find("#latestarticles").LoadingOverlay("hide");
                        },
                        function(error)
                        {
                            switch (error.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422: console.log('Validation Error '+response.message); break;
                                default: console.log(error.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestarticles").LoadingOverlay("hide");
                        }
                    );
                }

            $scope.sidebar_latestblogs=function()
                {
                    angular.element(document).find("#latestblogs").LoadingOverlay("show");

                    $scope.sidebarlatestblogs=blogsService.getlatestblogposts(4,0,$scope.blog_id,'latest',
                        function(response){
                            switch (response.status)
                            {
                                case 401:console.log('Credentials Incorrect');break;
                                case 200:$scope.sidebarlatestblogs=response.data.data;break;
                            }
                            angular.element(document).find("#latestblogs").LoadingOverlay("hide");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422: console.log('Validation Error '+response.message); break;
                                default: console.log(response.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestblogs").LoadingOverlay("hide");
                        }
                    );
                }

            $scope.sidebar_latestproducts=function()
                {
                    angular.element(document).find("#latestproducts").LoadingOverlay("show");

                    if($rootScope.isAuthenticated()==1)
                        $scope.sidebarlatestproducts=storeService.getproducts('small',9,0,
                            function(response)
                            {
                                switch (response.status)
                                {
                                    case 401:console.log('Credentials Incorrect');break;
                                    case 200:$scope.sidebarlatestproducts=response.data.data;break;
                                }
                                angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                            },
                            function(error)
                            {
                                switch (error.status)
                                {
                                    case 401: console.log('Credentials Incorrect'); break;
                                    case 422: console.log('Validation Error '+response.message); break;
                                    default : console.log(error.status+'Server Error Check Again please !'); break;
                                }
                                angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                            }
                        );
                    else
                    {
                        $scope.sidebarlatestproducts =0;
                        angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                    }


                }

            $scope.sidebar_latestclassifieds=function()
                {
                    angular.element(document).find("#latestclassifieds").LoadingOverlay("show");

                    if($rootScope.isAuthenticated()==1)
                        $scope.sidebarlatestclassifieds=classifiedsService.getclassifieds('small',4,0,
                            function(response)
                            {
                                switch (response.status)
                                {
                                    case 401: console.log('Credentials Incorrect');break;
                                    case 200: $scope.sidebarlatestclassifieds=response.data.data;break;
                                }
                                angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                            },
                            function(error)
                            {
                                switch (error.status)
                                {
                                    case 401:console.log('Credentials Incorrect'); break;
                                    case 422:console.log('Validation Error '+response.message); break;
                                    default: console.log(error.status+'Server Error Check Again please !'); break;
                                }
                                angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                            }
                        );
                    else
                    {
                        $scope.sidebarlatestclassifieds = 0;
                        angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                    }

                }


            $scope.getevent = function (eventid) {

                angular.element(document).find("#containerpart").LoadingOverlay("show");

                $scope.mainevent=eventsService.getevent(eventid,
                    function(response){
                        switch (response.status)
                        {
                            case 401:toastr.error('Credentials Incorrect'); break;
                            case 200:{

                                $scope.mainevent=response.data.data;

                                if($scope.mainevent==null) {
                                    toastr.error('Not Found');
                                    $state.go('events');
                                }

                            }break;
                        }
                        angular.element(document).find("#containerpart").LoadingOverlay("hide");
                    },
                    function(response){
                        switch (response.status)
                        {
                            case 401: toastr.error('Credentials Incorrect'); break;
                            case 422: toastr.error('Not Found'); break;
                            default : toastr.error('Server Error Check Again please !'); break;
                        }
                        angular.element(document).find("#containerpart").LoadingOverlay("hide");
                        $state.go('events');
                    }
                );


            }

            $scope.init=function(){

                if($stateParams.eventid!=null && $stateParams.eventid!=undefined &&   $stateParams.eventid!='')
                    $scope.getevent($stateParams.eventid);

                else
                    $state.go('events');
            };

                $scope.init();



        }]);
})();
