/**
 * Created by anas on 4/20/17.
 */

// public/scripts/EventsController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('EventsController',['eventsService','$scope','$sce','$stateParams','$timeout','$rootScope','$state','toastr',
             function (eventsService,$scope, $sce,$stateParams,$timeout,$rootScope,$state,toastr) {



            $scope.trusthtml = function(text) {
                return $sce.trustAsHtml(text);
            };

            $scope.getevents = function (page) {

                angular.element(document).find("#containerpart").LoadingOverlay("show");
                angular.element(document).find("#containerpart2").LoadingOverlay("show");

                $scope.homeevents=eventsService.getevents('big',10,page,
                    function(response){
                        switch (response.status) {

                            case 401:console.log('Credentials Incorrect'); break;
                            case 200:{
                                $scope.pagination=response.data.meta.pagination;
                                $scope.events = response.data.data;
                                if($scope.pagination.current_page==1) {
                                    $scope.slider_events = $scope.events.slice(0, 4);
                                    $scope.row_events = $scope.events.slice(4, 10);
                                }
                                else
                                    $scope.row_events = $scope.events;


                            }break;
                        }

                        angular.element(document).find("#containerpart").LoadingOverlay("hide");
                        angular.element(document).find("#containerpart2").LoadingOverlay("hide");
                    },
                    function(response){
                        switch (error.status)
                        {
                            case 401: console.log('Credentials Incorrect'); break;
                            case 422: console.log('Validation Error '+response.message); break;
                            default : console.log(error.status+'Server Error Check Again please !'); break;
                        }
                        angular.element(document).find("#containerpart").LoadingOverlay("hide");
                        angular.element(document).find("#containerpart2").LoadingOverlay("hide");
                    }
                );
            }



                 $scope.$on('$viewContentLoaded', function(event) {


                     if($scope.pageparm==1)
                     $timeout(function(){
                         MasterSliderShowcase3.initMasterSliderShowcase3();

                     }, 4000);
                 });


            $scope.init=function(){
                $scope.pageparm= 1;


                if($stateParams.page!=null && $stateParams.page!=undefined &&   $stateParams.page!='')
                    $scope.pageparm= $stateParams.page


                $scope.prepageparm=parseInt($scope.pageparm)-1;
                $scope.nextpageparm=parseInt($scope.pageparm)+1;
                $scope.getevents($scope.pageparm);
            };

            //init function
                 $scope.init();

        }]);
})();
