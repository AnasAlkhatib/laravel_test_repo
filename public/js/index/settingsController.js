/**
 * Created by anas on 4/20/17.
 */

// public/scripts/ArticlesController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .directive('fileModel', ['$parse', function ($parse) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    var model = $parse(attrs.fileModel);
                    var modelSetter = model.assign;

                    element.bind('change', function(){
                        scope.$apply(function(){
                            modelSetter(scope, element[0].files[0]);
                        });
                    });
                }
            };
        }])
        .controller('SettingsController',['accountServices','$scope','$sce','$stateParams','$state','toastr',
            function (accountServices,$scope, $sce,$stateParams,$state,toastr) {




        $scope.trusthtml = function(text) {
            return $sce.trustAsHtml(text);
        };


        $scope.checkbasicinfo=function()   {
             //check input values
            /*
            var me = {

              name:angular.element(document).find("#name").val(),
            }
            */
            var name           =angular.element(document).find("#name").val();
            var currentposition=angular.element(document).find("#currentposition").val();
            var aboutme        =angular.element(document).find("#aboutme").val();
            var description    =angular.element(document).find("#description").val();
            var country        =angular.element(document).find("#country").val();
            var state          =angular.element(document).find("#state").val();
            var address        =angular.element(document).find("#address").val();

              if(name=='' || currentposition=='' || aboutme=='' || description=='' || country=='' ||state=='' || address=='' )
              {

                  alert('missing info');
                  return false;

              }
              return true;



        }

        $scope.savebasicinfo=function(e) {

           // e.preventDefault();

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");


            var check=$scope.checkbasicinfo();
            if(check==false)
            {
                toastr.error('Missing Information');
                angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                return fales;

            }

            var me = {

                name: angular.element(document).find("#name").val(),
                currentposition: angular.element(document).find("#currentposition").val(),
                aboutme: angular.element(document).find("#aboutme").val(),
                description: angular.element(document).find("#description").val(),
                country: angular.element(document).find("#country").val(),
                state: angular.element(document).find("#state").val(),
                address: angular.element(document).find("#address").val()
            }



                   $scope.section='editbasicinfo';


                    $scope.myarticles=accountServices.savebasicinfo(me,
                        function(response){
                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 200:{


                                    accountServices.getme(function(response)
                                                                 {
                                                                     $state.go('settingssection',{section:'basicinfo'},{reload:true});
                                                                     angular.element(document).find("#dashboard_content").LoadingOverlay("hide");


                                                                 },
                                        function(error){
                                            switch (error.status)
                                            {
                                                case 401: toastr.error('Credentials Incorrect'); break;
                                                case 422: toastr.error('Validation Error '+error.message); break;
                                                default : toastr.error(error.status+'Server Error Check Again please !'); break;
                                            }
                                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                                            $state.go('settingssection',{section:'basicinfo'},{reload:true});
                                        }
                                        );

                                }break;


                            }

                        },
                        function(error){
                            switch (error.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Validation Error '+error.message); break;
                                default: toastr.error(error.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        }
                    );



                }

        $scope.getbasicinfo=function(section){

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    $scope.section=section;

            $scope.mybasicinfo=JSON.parse(localStorage.getItem('faduser'));
            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

            return;
        }


        $scope.deletedocument = function (id) {

                    angular.element(document).find("#dashboard_content").LoadingOverlay("show");
            accountServices.deletedocument(id,
                        function(response){

                            switch (response.status) {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:{
                                    toastr.success('Document deleted');
                                    angular.element(document).find("#document_"+id).remove();
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        }
                    );
        }

        $scope.getmyproofdocument=function()  {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

            $scope.section='proofdocument';

                $scope.documents=accountServices.getmyproofdocument(
                    function(response)
                    {
                        switch (response.status)
                        {
                            case 401:console.log('Credentials Incorrect');break;
                            case 200: $scope.documents=response.data.data;break;
                        }

                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                    },
                    function(error)
                    {
                        switch (error.status)
                        {
                            case 401: console.log('Credentials Incorrect'); break;
                            case 422: console.log('Validation Error '+response.message); break;
                            default: console.log(error.status+'Server Error Check Again please !'); break;
                        }
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                    }
                );



        }

        $scope.uploaddocument = function () {

                    angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    var documentfile = {
                        title: $scope.documenttitle,
                        image_url:'',
                    }

                    if($scope.adddocumentfile) documentfile.image_url=$scope.adddocumentfile;

                    if( documentfile.title =="" || documentfile.image_url=="" )
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        return false;
                    }


            accountServices.uploadproofdocument(documentfile,
                        function(response){

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('Document added');
                                    $state.go('settingssection',{ section:'proofdocument'});
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        }
                    );


                }

        $scope.showuploaddocument = function () {


                    angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    $scope.section='uploadproofdocument';
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");




                }

        $scope.privacy=function(){

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
            $scope.section='privacy';
            return;

                    $scope.myblogs=blogsService.getmyblogs('small',5,page,
                        function(response){
                            switch (response.status) {

                                case 401:alert('Credentials Incorrect');break;
                                case 200:{
                                    $scope.myblogs=response.data.data;
                                    $scope.blogspagination=response.data.meta.pagination;
                                    angular.element(document).find("#dashboard_content").removeClass( "spinner" );
                                    // alert('Great! Data Loaded');
                                    console.log(response.data.data);
                                }break;

                            }
                        },
                        function(response){
                            angular.element(document).find("#dashboard_content").removeClass( "spinner" );
                            switch (response.status) {

                                case 401:alert('Credentials Incorrect');break;
                                case 422:{

                                    alert('Sorry Error Happened');
                                    $state.go('forums');

                                }break;

                            }
                        }
                    );
                }

        $scope.settings=function(){

            angular.element(document).find("#dashboard_content").addClass( "spinner" );
            angular.element(document).find("#dashboard_content").removeClass( "spinner" );
            $scope.section='settings';
            return;

                    $scope.myclassifieds=classifiedsService.getmyclassifieds('small',4,page,
                        function(response){
                            switch (response.status) {

                                case 401:alert('Credentials Incorrect');break;
                                case 200:{
                                    $scope.myclassifieds=response.data.data;
                                    $scope.classifiedspagination=response.data.meta.pagination;
                                    angular.element(document).find("#dashboard_content").removeClass( "spinner" );
                                    // alert('Great! Data Loaded');
                                    console.log(response.data.data);
                                }break;

                            }
                        },
                        function(response){
                            angular.element(document).find("#dashboard_content").removeClass( "spinner" );
                            switch (response.status) {

                                case 401:alert('Credentials Incorrect');break;
                                case 422:{

                                    alert('Sorry Error Happened');
                                    $state.go('forums');

                                }break;

                            }
                        }
                    );
                }



        $scope.init=function()
        {
            if($stateParams.section!=null ||$stateParams.section!=undefined)
            {
                 switch ($stateParams.section)
                 {
                     case 'basicinfo'      :$scope.getbasicinfo($stateParams.section);break;
                     case 'privacy'        :$scope.privacy();break;
                     case 'settings'       :$scope.settings();break;
                     case 'editbasicinfo'  :$scope.getbasicinfo($stateParams.section);break;
                     case 'editprivacy'    :$scope.privacy();break;
                     case 'editsettings'   :$scope.settings();break;

                     case 'proofdocument'         :$scope.getmyproofdocument();break;
                     case 'uploadproofdocument'   :$scope.showuploaddocument();break;

                     default :{$scope.getbasicinfo('basicinfo'); }    break;
                 }
            }

            else
                {
                $scope.getbasicinfo('basicinfo');
                }
        }

        $scope.init();


    }]);



})();
