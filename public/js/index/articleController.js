/**
 * Created by anas on 4/20/17.
 */

// public/scripts/ArticlesController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('ArticleController',['articlesService','$scope','$sce','$stateParams','forumsService','blogsService',
                    'classifiedsService','storeService','$state','$rootScope',
             function (articlesService,$scope, $sce,$stateParams,forumsService,blogsService,classifiedsService,
                       storeService,$state,$rootScope) {




        $scope.trusthtml = function(text) {
            return $sce.trustAsHtml(text);
        };

        $scope.sidebar_latestforums=function()
        {

                     angular.element(document).find("#latestforums").LoadingOverlay("show");

                     if($rootScope.isAuthenticated()==1)
                         $scope.sidebarlatestposts=forumsService.getlatestposts(4,1,
                             function(response)
                             {
                                 switch (response.status)
                                 {
                                     case 401:console.log('Credentials Incorrect');break;
                                     case 200: $scope.sidebarlatestposts=response.data.data;break;
                                 }
                                 angular.element(document).find("#latestforums").LoadingOverlay("hide");
                             },
                             function(error)
                             {
                                 switch (error.status)
                                 {
                                     case 401: console.log('Credentials Incorrect'); break;
                                     case 422: console.log('Validation Error '+response.message); break;
                                     default: console.log(error.status+'Server Error Check Again please !'); break;
                                 }
                                 angular.element(document).find("#latestforums").LoadingOverlay("hide");
                             }
                         );
                     else
                     {
                         $scope.sidebarlatestposts = 0;
                         angular.element(document).find("#latestforums").LoadingOverlay("hide");
                     }
                 }

        $scope.sidebar_latestclassifieds=function()
        {
                     angular.element(document).find("#latestclassifieds").LoadingOverlay("show");

                     if($rootScope.isAuthenticated()==1)
                         $scope.sidebarlatestclassifieds=classifiedsService.getclassifieds('small',4,0,
                             function(response)
                             {
                                 switch (response.status)
                                 {
                                     case 401: console.log('Credentials Incorrect');break;
                                     case 200: $scope.sidebarlatestclassifieds=response.data.data;break;
                                 }
                                 angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                             },
                             function(error)
                             {
                                 switch (error.status)
                                 {
                                     case 401:console.log('Credentials Incorrect'); break;
                                     case 422:console.log('Validation Error '+response.message); break;
                                     default: console.log(error.status+'Server Error Check Again please !'); break;
                                 }
                                 angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                             }
                         );
                     else
                     {
                         $scope.sidebarlatestclassifieds = 0;
                         angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                     }

                 }

        $scope.sidebar_latestproducts=function()
        {
                     angular.element(document).find("#latestproducts").LoadingOverlay("show");

                     if($rootScope.isAuthenticated()==1)
                         $scope.sidebarlatestproducts=storeService.getproducts('small',9,0,
                             function(response)
                             {
                                 switch (response.status)
                                 {
                                     case 401:console.log('Credentials Incorrect');break;
                                     case 200:$scope.sidebarlatestproducts=response.data.data;break;
                                 }
                                 angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                             },
                             function(error)
                             {
                                 switch (error.status)
                                 {
                                     case 401: console.log('Credentials Incorrect'); break;
                                     case 422: console.log('Validation Error '+response.message); break;
                                     default : console.log(error.status+'Server Error Check Again please !'); break;
                                 }
                                 angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                             }
                         );
                     else
                     {
                         $scope.sidebarlatestproducts =0;
                         angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                     }


                 }

        $scope.sidebar_latestblogs=function()
        {
                     angular.element(document).find("#latestblogs").LoadingOverlay("show");

                     $scope.sidebarlatestblogs=blogsService.getlatestblogposts(4,0,0,'none',
                         function(response){
                             switch (response.status)
                             {
                                 case 401:console.log('Credentials Incorrect');break;
                                 case 200:$scope.sidebarlatestblogs=response.data.data;break;
                             }
                             angular.element(document).find("#latestblogs").LoadingOverlay("hide");

                         },
                         function(error)
                         {
                             switch (error.status)
                             {
                                 case 401: console.log('Credentials Incorrect'); break;
                                 case 422: console.log('Validation Error '+error.message); break;
                                 default: console.log(error.status+'Server Error Check Again please !'); break;
                             }
                             angular.element(document).find("#latestblogs").LoadingOverlay("hide");
                         }
                     );
                 }

        $scope.sidebar_latestarticles=function()
        {

            angular.element(document).find("#latestarticles").LoadingOverlay("show");

                     $scope.sidebarlatestarticles=articlesService.getlatestarticles('small',4,0,$scope.article_id,
                         function(response)
                         {
                             switch (response.status)
                             {
                                 case 401:console.log('Credentials Incorrect');break;
                                 case 200:$scope.sidebarlatestarticles=response.data.data;break;
                             }
                             angular.element(document).find("#latestarticles").LoadingOverlay("hide");
                         },
                         function(error)
                         {
                             switch (error.status)
                             {
                                 case 401: console.log('Credentials Incorrect'); break;
                                 case 422: console.log('Validation Error '+response.message); break;
                                 default: console.log(error.status+'Server Error Check Again please !'); break;
                             }
                             angular.element(document).find("#latestarticles").LoadingOverlay("hide");
                         }
                     );
                 }


        $scope.getarticle = function (articleid)
        {

            angular.element(document).find("#containerpart").LoadingOverlay("show",{image:"",fontawesome : "fa fa-spinner fa-spin"});

                     $scope.article=articlesService.getarticle(articleid,
                         function(response)
                         {
                             switch (response.status)
                             {
                                 case 401:console.log('Credentials Incorrect');break;
                                 case 200: $scope.article=response.data.data;break;
                             }
                             angular.element(document).find("#containerpart").LoadingOverlay("hide");
                         },
                         function(error)
                         {
                             switch (error.status)
                             {
                                 case 401: console.log('Credentials Incorrect'); break;
                                 case 422:console.log('Validation Error '+response.message); break;
                                 default: console.log(error.status+'Server Error Check Again please !'); break;
                             }
                             angular.element(document).find("#containerpart").LoadingOverlay("hide");
                         }
                     );

                 }


        $scope.initarticlecontr=function()
        {

           if($stateParams.articleid!=null && $stateParams.articleid!=undefined &&  $stateParams.articleid!='')
           {
               $scope.article_id = $stateParams.articleid;
               $scope.getarticle($stateParams.articleid);
           }
           else
              $state.go('articles');
        };






    }]);

})();
