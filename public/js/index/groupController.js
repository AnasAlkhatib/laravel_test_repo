/**
 * Created by anas on 4/20/17.
 */

// public/scripts/GroupsController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .directive('fileModel', ['$parse', function ($parse) {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    var model = $parse(attrs.fileModel);
                    var modelSetter = model.assign;

                    element.bind('change', function(){
                        scope.$apply(function(){
                            modelSetter(scope, element[0].files[0]);
                        });
                    });
                }
            };
        }])
        .controller('GroupController',['groupsService','$scope','$sce','$stateParams','forumsService','articlesService',
                                       'blogsService','classifiedsService','$state','storeService','toastr','$rootScope',
            function (groupsService,$scope, $sce,$stateParams,forumsService,articlesService,blogsService,
                      classifiedsService,$state,storeService,toastr,$rootScope) {




            $scope.seteditoroptions=function(){
                    $scope.htmlEditor = '';
                    // setup editor options
                    $scope.editorOptions = {
                        language: 'en',
                        uiColor: '#ffffff'
                    };
            };

            $scope.trusthtml = function(text) {
                return $sce.trustAsHtml(text);
            };

            $scope.sidebar_latestforums=function(){

                    angular.element(document).find("#containerpart2").LoadingOverlay("show");

                    $scope.sidebarlatestposts=forumsService.getlatestposts(4,1,
                        function(response){
                            switch (response.status) {

                                case 401:console.log('Credentials Incorrect'); break;
                                case 200:{
                                    $scope.sidebarlatestposts=response.data.data;



                                }break;

                            }
                            angular.element(document).find("#containerpart2").LoadingOverlay("show");
                        },
                        function(response){
                            switch (error.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422: console.log('Validation Error '+response.message); break;
                                default : console.log(error.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                        }
                    );
                }

            $scope.sidebar_latestarticles=function() {
                    angular.element(document).find("#latestarticles").LoadingOverlay("show");

                    $scope.sidebarlatestarticles=articlesService.getlatestarticles('small',4,0,0,
                        function(response){
                            switch (response.status)
                            {
                                case 401:console.log('Credentials Incorrect');break;
                                case 200: $scope.sidebarlatestarticles=response.data.data;break;
                            }
                            angular.element(document).find("#latestarticles").LoadingOverlay("hide");
                        },
                        function(error)
                        {
                            switch (error.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422: console.log('Validation Error '+response.message); break;
                                default: console.log(error.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestarticles").LoadingOverlay("hide");
                        }
                    );
                }

            $scope.sidebar_latestblogs=function() {
                    angular.element(document).find("#latestblogs").LoadingOverlay("show");

                    $scope.sidebarlatestblogs=blogsService.getlatestblogposts(4,0,$scope.blog_id,'latest',
                        function(response){
                            switch (response.status)
                            {
                                case 401:console.log('Credentials Incorrect');break;
                                case 200:$scope.sidebarlatestblogs=response.data.data;break;
                            }
                            angular.element(document).find("#latestblogs").LoadingOverlay("hide");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422: console.log('Validation Error '+response.message); break;
                                default: console.log(response.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestblogs").LoadingOverlay("hide");
                        }
                    );
                }

            $scope.sidebar_latestproducts=function()   {
                    angular.element(document).find("#latestproducts").LoadingOverlay("show");


                        $scope.sidebarlatestproducts=storeService.getproducts('small',9,0,
                            function(response)
                            {
                                switch (response.status)
                                {
                                    case 401:console.log('Credentials Incorrect');break;
                                    case 200:$scope.sidebarlatestproducts=response.data.data;break;
                                }
                                angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                            },
                            function(error)
                            {
                                switch (error.status)
                                {
                                    case 401: console.log('Credentials Incorrect'); break;
                                    case 422: console.log('Validation Error '+response.message); break;
                                    default : console.log(error.status+'Server Error Check Again please !'); break;
                                }
                                angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                            }
                        );



                }

            $scope.sidebar_latestclassifieds=function()   {
                    angular.element(document).find("#latestclassifieds").LoadingOverlay("show");

                        $scope.sidebarlatestclassifieds=classifiedsService.getclassifieds('small',4,0,
                            function(response)
                            {
                                switch (response.status)
                                {
                                    case 401: console.log('Credentials Incorrect');break;
                                    case 200: $scope.sidebarlatestclassifieds=response.data.data;break;
                                }
                                angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                            },
                            function(error)
                            {
                                switch (error.status)
                                {
                                    case 401:console.log('Credentials Incorrect'); break;
                                    case 422:console.log('Validation Error '+response.message); break;
                                    default: console.log(error.status+'Server Error Check Again please !'); break;
                                }
                                angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                            }
                        );


                }

            $scope.sidebar_latestmembers=function(groupid){

                angular.element(document).find("#latestmembers").LoadingOverlay("show");

                    $scope.sidebar_latestmembers=groupsService.getgroupmembers(groupid,4,0,
                        function(response){
                            switch (response.status) {

                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 200:{
                                    $scope.sidebar_latestmembers=response.data.data;



                                }break;
                            }
                            angular.element(document).find("#latestmembers").LoadingOverlay("hide");
                        },
                        function(error)
                        {
                            switch (error.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Validation Error '+error.message); break;
                                default: toastr.error(error.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestmembers").LoadingOverlay("hide");
                            $state.go('groups');
                        }
                    );
                }

            $scope.sendjoinRequest = function (groupid)   {

                    angular.element(document).find("#group_info").LoadingOverlay("show");



                    groupsService.postjoinrequest(groupid,
                        function(response)
                        {
                            switch (response.status)
                            {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 200:{

                                    toastr.success('Request Sent');
                                    $state.go($state.current, {}, {reload: true});

                                }break;
                            }
                            angular.element(document).find("#group_info").LoadingOverlay("hide");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#group_info").LoadingOverlay("hide");

                        }
                    );


                }

            $scope.removejoinRequest = function (groupid)   {

                    angular.element(document).find("#group_info").LoadingOverlay("show");



                    groupsService.postremoverequest(groupid,
                        function(response)
                        {
                            switch (response.status)
                            {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:{
                                    toastr.success('Request Sent');
                                    $state.go($state.current, {}, {reload: true});

                                }break;
                            }
                            angular.element(document).find("#group_info").LoadingOverlay("hide");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#group_info").LoadingOverlay("hide");

                        }
                    );


                }

            $scope.removeuser = function (groupid,memberid)   {

                    angular.element(document).find("#content_center").LoadingOverlay("show");

                    groupsService.removemember(groupid,memberid,
                        function(response)
                        {
                            switch (response.status)
                            {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:{
                                    toastr.success('User Removed From Group');

                                    $scope.managegroup(groupid,1);
                                }break;
                            }
                            angular.element(document).find("#content_center").LoadingOverlay("hide");
                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#content_center").LoadingOverlay("hide");

                        }
                    );
            }

            $scope.approveuser = function (groupid,memberid)   {

                    angular.element(document).find("#content_center").LoadingOverlay("show");



                    groupsService.approvemember(groupid,memberid,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:{
                                    toastr.success('User Approved');


                                }break;
                            }
                            angular.element(document).find("#content_center").LoadingOverlay("hide");
                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#content_center").LoadingOverlay("hide");
                        }
                    );
                }

            $scope.backtomaingroup = function () {

                angular.element(document).find("#content_center").LoadingOverlay("show");
                $scope.task='';
                angular.element(document).find("#content_center").LoadingOverlay("hide");

            }

            $scope.gottoprofile = function (profileid) {


                $state.go('profile',{profileid:profileid});


                }

            $scope.getgroup = function (groupid) {

                angular.element(document).find("#group_info").LoadingOverlay("show");

                $scope.maingroup=groupsService.getgroup(groupid,
                    function(response){
                        switch (response.status) {

                            case 401:toastr.error('Credentials Incorrect');break;
                            case 200:{
                                $scope.maingroup=response.data.data;

                            }break;
                        }
                        angular.element(document).find("#group_info").LoadingOverlay("hide");
                    },
                    function(error)
                    {
                        switch (error.status)
                        {
                            case 401: toastr.error('Credentials Incorrect'); break;
                            case 422: toastr.error('Validation Error '+error.message); break;
                            default: toastr.error(error.status+'Server Error Check Again please !'); break;
                        }
                        angular.element(document).find("#group_info").LoadingOverlay("hide");
                        $state.go('groups');
                    }
                );


            }

            $scope.managegroup = function (groupid,page) {

                    angular.element(document).find("#content_center").LoadingOverlay("show");

                    $scope.groupusers_list=groupsService.getgroupmembers(groupid,30,page,
                        function(response)
                        {
                            switch (response.status)
                            {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 200:{
                                    $scope.groupusers_list=response.data.data;
                                    $scope.groupusers_pagination=response.data.meta.pagination;
                                    $scope.task='manageusers';
                                    $scope.manage_users_prepageparm=page-1;
                                    $scope.manage_users_nextpageparm=page+1;

                                }break;
                            }
                            angular.element(document).find("#content_center").LoadingOverlay("hide");
                        },
                        function(error)
                        {
                            switch (error.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Validation Error '+error.message); break;
                                default: toastr.error(error.status+'Server Error Check Again please !'); break;
                            }
                            $scope.task='';
                            angular.element(document).find("#content_center").LoadingOverlay("hide");
                        }
                    );
                }


            $scope.getgroupcontent = function (groupid,page) {

                    angular.element(document).find("#content_center").LoadingOverlay("show");

                    $scope.groupcontent=groupsService.getgroupcontent(groupid,3,page,
                        function(response)
                        {
                            switch (response.status)
                            {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 200:{
                                    $scope.groupcontent=response.data.data;
                                    $scope.groupcontent_pagination=response.data.meta.pagination;
                                    $scope.task='';
                                    $scope.contentpage=page;


                                   // $scope.manage_users_prepageparm=page-1;
                                  //  $scope.manage_users_nextpageparm=page+1;
                                   // console.log($scope.groupcontent);

                                }break;
                            }
                            angular.element(document).find("#content_center").LoadingOverlay("hide");
                        },
                        function(error)
                        {
                            switch (error.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Validation Error '+error.message); break;
                                default: toastr.error(error.status+'Server Error Check Again please !'); break;
                            }
                            $scope.task='';
                            angular.element(document).find("#content_center").LoadingOverlay("hide");
                        }
                    );
                }

            $scope.cancel=function(e)   {
                    $scope.task='';
                }

            $scope.addgrouppost = function (e) {

                    $scope.seteditoroptions();


                    angular.element(document).find("#content_center").LoadingOverlay("show");

                    var group = {

                        title: angular.element(document).find("#contenttitle").val(),
                        content: $scope.postcontent,
                        id: $scope.maingroup.id,
                        image_url:'',
                    }


                    if($scope.addgroupcontentimages) group.image_url=$scope.addgroupcontentimages;

                    if( group.id =="" || group.title=="" || group.content==null || group.content=='')
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#content_center").LoadingOverlay("hide");
                        return false;
                    }



                    groupsService.addgroupcontent(group,
                        function(response){

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('Post added');
                                    $scope.task='';
                                    $scope.postcontent='';
                                    angular.element(document).find("#contenttitle").val('');

                                    $scope.getgroupcontent($scope.maingroup.id,$scope.contentpage);
                                }break;
                            }
                            angular.element(document).find("#content_center").LoadingOverlay("hide");

                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            $scope.task='';

                            angular.element(document).find("#content_center").LoadingOverlay("hide");

                        }
                    );


                }

            $scope.editgrouppost = function (id) {
                    window.scrollTo(0,0);
                    $scope.task='editpost';
                    angular.element(document).find("#containerpart").LoadingOverlay("show");
                    $scope.seteditoroptions();

                    forumsService.getmyforumpost(id,
                        function(response){

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    $scope.editforumpostval=response.data.data;
                                    $scope.modelupdateforumpost=$scope.trusthtml($scope.editforumpostval.text);

                                }break;
                            }
                            angular.element(document).find("#containerpart").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            $scope.task='';
                            angular.element(document).find("#containerpart").LoadingOverlay("hide");

                        }
                    );
                }

            $scope.savegrouppost = function (e)  {
                    window.scrollTo(0,0);
                    $scope.seteditoroptions();
                    $scope.task='editpost';

                    angular.element(document).find("#containerpart").LoadingOverlay("show");

                    var forumpost = {
                        id   :angular.element(document).find("#editpost_id").val(),
                        title: angular.element(document).find("#title").val(),
                        content: $scope.modelupdateforumpost,
                        topic_id: angular.element(document).find("#post_edit_topic_id").val(),
                    }



                    if( forumpost.id ==""|| forumpost.topic_id =="" || forumpost.title=="" || forumpost.content==null)
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#containerpart").LoadingOverlay("hide");
                        return false;
                    }

                    forumsService.savepost(forumpost,
                        function(response){
                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{

                                    toastr.success('Post updated');
                                    $scope.task='';
                                }break;
                            }
                            angular.element(document).find("#containerpart").LoadingOverlay("hide");

                        },
                        function(response){

                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            $scope.task='';
                            angular.element(document).find("#containerpart").LoadingOverlay("hide");
                        }
                    );
                }

            $scope.deletecontentpost = function (id) {

                    angular.element(document).find("#content_center").LoadingOverlay("show");
                    groupsService.deletegroupcontent(id,
                        function(response){

                            switch (response.status) {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:{
                                    toastr.success('Post deleted');
                                    $scope.getgroupcontent($scope.maingroup.id,$scope.contentpage);
                                }break;
                            }
                            angular.element(document).find("#content_center").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#content_center").LoadingOverlay("hide");
                        }
                    );
                }

            $scope.addgroupphoto = function (e) {

                    $scope.seteditoroptions();


                    angular.element(document).find("#content_center").LoadingOverlay("show");

                    var group = {

                        title: angular.element(document).find("#phototitle").val(),
                        id: $scope.maingroup.id,
                        image_url:'',
                    }


                    if($scope.addgroupphotoimages) group.image_url=$scope.addgroupphotoimages;

                    if( group.id =="" || group.title=="" )
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#content_center").LoadingOverlay("hide");
                        return false;
                    }

                    groupsService.addgroupphoto(group,
                        function(response){

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('Photo added');
                                    $scope.task='';

                                    angular.element(document).find("#phototitle").val('');

                                    $scope.getgroupcontent($scope.maingroup.id,$scope.contentpage);
                                }break;
                            }
                            angular.element(document).find("#content_center").LoadingOverlay("hide");

                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            $scope.task='';

                            angular.element(document).find("#content_center").LoadingOverlay("hide");

                        }
                    );


                }

            $scope.addgroupvideo2 = function (e) {

                    $scope.seteditoroptions();


                    angular.element(document).find("#content_center").LoadingOverlay("show");

                    var group = {

                        title: angular.element(document).find("#phototitle").val(),
                        id: $scope.maingroup.id,
                        image_url:'',
                    }


                    if($scope.addgroupphotoimages) group.image_url=$scope.addgroupphotoimages;

                    if( group.id =="" || group.title=="" )
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#content_center").LoadingOverlay("hide");
                        return false;
                    }

                    groupsService.addgroupcontent(group,
                        function(response){

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('Photo added');
                                    $scope.task='';

                                    angular.element(document).find("#phototitle").val('');

                                    $scope.getgroupcontent($scope.maingroup.id,$scope.contentpage);
                                }break;
                            }
                            angular.element(document).find("#content_center").LoadingOverlay("hide");

                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            $scope.task='';

                            angular.element(document).find("#content_center").LoadingOverlay("hide");

                        }
                    );


                }

            $scope.addgroupvideo = function (e) {

                    $scope.seteditoroptions();


                    angular.element(document).find("#content_center").LoadingOverlay("show");

                    var group = {

                        title: angular.element(document).find("#videotitle").val(),
                        id: $scope.maingroup.id,
                        video_url:'',
                    }


                    if($scope.addgroupcontentvideo) group.video_url=$scope.addgroupcontentvideo;

                    if( group.id =="" || group.title=="" )
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#content_center").LoadingOverlay("hide");
                        return false;
                    }

                    groupsService.addgroupvideo(group,
                        function(response){

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('Video added');
                                    $scope.task='';

                                    angular.element(document).find("#phototitle").val('');

                                    $scope.getgroupcontent($scope.maingroup.id,$scope.contentpage);
                                }break;
                            }
                            angular.element(document).find("#content_center").LoadingOverlay("hide");

                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: {toastr.error(response.data.errors.video_url); }break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            $scope.task='';

                            angular.element(document).find("#content_center").LoadingOverlay("hide");

                        }
                    );


                }



            $scope.init=function()
            {
                $scope.task='';
                $scope.manage_users_prepageparm=0;
                $scope.manage_users_nextpageparm=2;
                $scope.contentpage=1;


                    if($stateParams.groupid!=null && $stateParams.groupid!=undefined &&   $stateParams.groupid!='')
                    {
                        $scope.getgroup($stateParams.groupid);
                        $scope.sidebar_latestmembers($stateParams.groupid);
                        $scope.getgroupcontent($stateParams.groupid,1);
                    }

                    else
                        $state.go('groups');
                };

                $scope.init();






        }]);
})();
