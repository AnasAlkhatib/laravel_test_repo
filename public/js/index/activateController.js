/**
 * Created by anas on 4/20/17.
 */

// public/scripts/BlogsController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('ActivateController',['activationServices','$scope','$sce','$stateParams','$state','toastr',
            function (activationServices,$scope, $sce,$stateParams,$state,toastr) {


        $scope.trusthtml = function(text) {
            return $sce.trustAsHtml(text);
        };


        $scope.initactivatecontr=function(){

            $scope.error_found='';
            $scope.message='';

            angular.element(document).find("#containerpart").LoadingOverlay("show");


            if($stateParams.token!=null && $stateParams.token!=undefined && $stateParams.token!='' && $stateParams.email!=null && $stateParams.email!=undefined && $stateParams.email!='')
            {

                $scope.activate= activationServices.activateaccount($stateParams.email,$stateParams.token,
                    function(response)
                    {
                        switch (response.status)
                        {
                            case 401:toastr.error('Account Already Active');break;
                            case 202: {

                                toastr.success('your account successfully activated');
                                $scope.message='you can now login';
                            }break;
                        }
                        angular.element(document).find("#containerpart").LoadingOverlay("hide");
                    },
                    function(error)
                    {
                        switch (error.status)
                        {
                            case 401:  $scope.error_found='Account Already Active';break;//toastr.error('Account Already Active'); break;
                            case 422:  $scope.error_found='Account Already Active';break; //toastr.error('Activation Code Not Correct'); break;
                            default: $scope.error_found='Server Error Check Again please !';break;//toastr.error('Server Error Check Again please !'); break;
                        }

                        angular.element(document).find("#containerpart").LoadingOverlay("hide");

                    }
                );

            }

            else
                $state.go('/');



      };


    }]);

})();
