/**
 * Created by anas on 4/20/17.
 */

// public/scripts/ForumsController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('ForumController',['forumsService','$scope','$sce','$stateParams','$state','articlesService',
                    'blogsService','storeService','classifiedsService','toastr','$rootScope',
                    function (forumsService,$scope, $sce,$stateParams,$state,articlesService,blogsService,storeService,classifiedsService,toastr,$rootScope) {





            $scope.trusthtml = function(text) {
                return $sce.trustAsHtml(text);
            };

            $scope.mylocale = {
                   formatDate: function(date) {
                                var m = moment(date);
                                return m.isValid() ? m.format('YYYY-MM-DD') : '';
                            }
                        };

            $scope.dateoptions=function()  {

                            $scope.myDate = new Date();

                            $scope.minDate = new Date(
                                $scope.myDate.getFullYear(),
                                $scope.myDate.getMonth() - 2,
                                $scope.myDate.getDate()
                            );


                            $scope.isOpen = false;

             }

            $scope.seteditoroptions=function(){
                            $scope.htmlEditor = '';
                            // setup editor options
                            $scope.editorOptions = {
                                language: 'en',
                                uiColor: '#ffffff'
                            };
            }

            $scope.getforumtopics = function (forumid,page) {
                angular.element(document).find("#containerpart").LoadingOverlay("show");

                $scope.hometopics=forumsService.getforumtopics(forumid,6,page,
                    function(response){
                        switch (response.status) {

                            case 401: toastr.error('Credentials Incorrect'); break;
                            case 200:{
                                $scope.hometopics=response.data.data;
                                $scope.pagination=response.data.meta.pagination;


                                if($scope.hometopics=='' && page>1)
                                 $state.go('forumdetails', {'forumid': forumid,'page':1}, {});



                            }break;

                        }
                        angular.element(document).find("#containerpart").LoadingOverlay("hide");

                    },
                    function(response){
                        switch (response.status)
                        {
                            case 401: toastr.error('Credentials Incorrect'); break;
                            case 422: toastr.error('Not Found'); break;
                            default : toastr.error('Server Error Check Again please !'); break;
                        }
                        angular.element(document).find("#containerpart").LoadingOverlay("hide");
                    }
                );
            }

            $scope.sidebar_latestarticles=function()  {
                            angular.element(document).find("#latestarticles").LoadingOverlay("show");

                            $scope.sidebarlatestarticles=articlesService.getlatestarticles('small',4,0,0,
                                function(response){
                                    switch (response.status)
                                    {
                                        case 401:console.log('Credentials Incorrect');break;
                                        case 200: $scope.sidebarlatestarticles=response.data.data;break;
                                    }
                                    angular.element(document).find("#latestarticles").LoadingOverlay("hide");
                                },
                                function(error)
                                {
                                    switch (error.status)
                                    {
                                        case 401: console.log('Credentials Incorrect'); break;
                                        case 422: console.log('Validation Error '+response.message); break;
                                        default: console.log(error.status+'Server Error Check Again please !'); break;
                                    }
                                    angular.element(document).find("#latestarticles").LoadingOverlay("hide");
                                }
                            );
                        }

            $scope.sidebar_latestblogs=function()     {
                            angular.element(document).find("#latestblogs").LoadingOverlay("show");

                            $scope.sidebarlatestblogs=blogsService.getlatestblogposts(4,0,$scope.blog_id,'latest',
                                function(response){
                                    switch (response.status)
                                    {
                                        case 401:console.log('Credentials Incorrect');break;
                                        case 200:$scope.sidebarlatestblogs=response.data.data;break;
                                    }
                                    angular.element(document).find("#latestblogs").LoadingOverlay("hide");

                                },
                                function(response)
                                {
                                    switch (response.status)
                                    {
                                        case 401: console.log('Credentials Incorrect'); break;
                                        case 422: console.log('Validation Error '+response.message); break;
                                        default: console.log(response.status+'Server Error Check Again please !'); break;
                                    }
                                    angular.element(document).find("#latestblogs").LoadingOverlay("hide");
                                }
                            );
                        }

            $scope.sidebar_latestclassifieds=function()   {
                            angular.element(document).find("#latestclassifieds").LoadingOverlay("show");

                            if($rootScope.isAuthenticated()==1)
                                $scope.sidebarlatestclassifieds=classifiedsService.getclassifieds('small',4,0,
                                    function(response)
                                    {
                                        switch (response.status)
                                        {
                                            case 401: console.log('Credentials Incorrect');break;
                                            case 200: $scope.sidebarlatestclassifieds=response.data.data;break;
                                        }
                                        angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                                    },
                                    function(error)
                                    {
                                        switch (error.status)
                                        {
                                            case 401:console.log('Credentials Incorrect'); break;
                                            case 422:console.log('Validation Error '+response.message); break;
                                            default: console.log(error.status+'Server Error Check Again please !'); break;
                                        }
                                        angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                                    }
                                );
                            else
                            {
                                $scope.sidebarlatestclassifieds = 0;
                                angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                            }

                        }

            $scope.sidebar_latestproducts=function()      {
                            angular.element(document).find("#latestproducts").LoadingOverlay("show");

                            if($rootScope.isAuthenticated()==1)
                                $scope.sidebarlatestproducts=storeService.getproducts('small',9,0,
                                    function(response)
                                    {
                                        switch (response.status)
                                        {
                                            case 401:console.log('Credentials Incorrect');break;
                                            case 200:$scope.sidebarlatestproducts=response.data.data;break;
                                        }
                                        angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                                    },
                                    function(error)
                                    {
                                        switch (error.status)
                                        {
                                            case 401: console.log('Credentials Incorrect'); break;
                                            case 422: console.log('Validation Error '+response.message); break;
                                            default : console.log(error.status+'Server Error Check Again please !'); break;
                                        }
                                        angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                                    }
                                );
                            else
                            {
                                $scope.sidebarlatestproducts =0;
                                angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                            }


                        }

            $scope.new_topic=function()  {
                $scope.seteditoroptions();
                $scope.task='addtopic';
                window.scrollTo(0,0);
                return;



            }

            $scope.addforumtopic = function (e) {

                            $scope.seteditoroptions();


                            angular.element(document).find("#containerpart").LoadingOverlay("show");

                            var forumtopic = {

                                title: angular.element(document).find("#addtopictitle").val(),
                                content: $scope.topiccontent,
                                tags: angular.element(document).find("#addtopictags").val(),
                                forum_id: angular.element(document).find("#addforum_id").val(),
                            }



                            if( forumtopic.forum_id =="" || forumtopic.title=="" || forumtopic.content==null || forumtopic.content=='')
                            {
                                toastr.error('Missing Information');
                                angular.element(document).find("#containerpart").LoadingOverlay("hide");
                                return false;
                            }



                            forumsService.addtopic(forumtopic,
                                function(response){

                                    switch (response.status) {

                                        case 401:toastr.error('Credentials Incorrect');break;
                                        case 202:toastr.error('Error Happened');break;
                                        case 200:{
                                            toastr.success('Topic added');
                                             $scope.task='';
                                            //$state.go($state.current, {}, {reload: true});
                                            $scope.getforumtopics($stateParams.forumid,$scope.pageparm);
                                        }break;
                                    }
                                    angular.element(document).find("#containerpart").LoadingOverlay("hide");

                                },
                                function(response){
                                    switch (response.status)
                                    {
                                        case 401: toastr.error('Credentials Incorrect'); break;
                                        case 422: toastr.error('Not Found'); break;
                                        default : toastr.error('Server Error Check Again please !'); break;
                                    }
                                    $scope.task='';
                                    angular.element(document).find("#containerpart").LoadingOverlay("hide");

                                }
                            );


                        }

            $scope.editforumtopic = function (id) {

                            $scope.task='edittopic';
                            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                        $scope.seteditoroptions();

                            forumsService.getmyforumtopic(id,
                                function(response){

                                    switch (response.status) {

                                        case 401:toastr.error('Credentials Incorrect');break;
                                        case 202:toastr.error('Error Happened');break;
                                        case 200:{
                                            $scope.editforumtopicval=response.data.data;
                                            $scope.modelupdateforumtopic=$scope.trusthtml($scope.editforumtopicval.content);



                                        }break;
                                    }
                                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                                },
                                function(response){
                                    switch (response.status)
                                    {
                                        case 401: toastr.error('Credentials Incorrect'); break;
                                        case 422: toastr.error('Not Found'); break;
                                        default : toastr.error('Server Error Check Again please !'); break;
                                    }
                                    $scope.task='';
                                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                                }
                            );


                        }

            $scope.saveforumtopic = function (e)  {

                            $scope.seteditoroptions();
                            $scope.task='editpost';

                            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                            var forumtopic = {
                                id   :angular.element(document).find("#topic_id").val(),
                                title: angular.element(document).find("#topictitle").val(),
                                content: $scope.modelupdateforumtopic,
                                tags: angular.element(document).find("#topictags").val(),
                                forum_id: angular.element(document).find("#forum_id").val(),
                            }



                            if( forumtopic.id ==""|| forumtopic.forum_id =="" || forumtopic.title=="" || forumtopic.content==null)
                            {
                                toastr.error('Missing Information');
                                angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                                return false;
                            }

                            forumsService.savetopic(forumtopic,
                                function(response){
                                    switch (response.status) {

                                        case 401:toastr.error('Credentials Incorrect');break;
                                        case 202:toastr.error('Error Happened');break;
                                        case 200:{

                                            toastr.success('Topic updated');
                                            $scope.task='';
                                        }break;
                                    }
                                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                                },
                                function(response){

                                    switch (response.status)
                                    {
                                        case 401: toastr.error('Credentials Incorrect'); break;
                                        case 422: toastr.error('Not Found'); break;
                                        default : toastr.error('Server Error Check Again please !'); break;
                                    }
                                    $scope.task='';
                                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");


                                }
                            );

                        }

            $scope.cancel_add=function(e)   {
                     $scope.task='';
                 }


            $scope.deleteforumtopic = function (id) {

                            angular.element(document).find("#containerpart").LoadingOverlay("show");
                            forumsService.deleteforumtopic(id,
                                function(response){

                                    switch (response.status) {
                                        case 401:toastr.error('Credentials Incorrect');break;
                                        case 202:{
                                            toastr.success('Topic deleted');
                                            angular.element(document).find("#topic_"+id).remove();
                                            $scope.pagination.total=$scope.pagination.total-1;
                                        }break;
                                    }
                                    angular.element(document).find("#containerpart").LoadingOverlay("hide");
                                },
                                function(response){
                                    switch (response.status)
                                    {
                                        case 401: toastr.error('Credentials Incorrect'); break;
                                        case 422: toastr.error('Not Found'); break;
                                        default : toastr.error('Server Error Check Again please !'); break;
                                    }
                                    angular.element(document).find("#containerpart").LoadingOverlay("hide");
                                }
                            );
                        }



            $scope.getforuminfo = function (forumid) {

                            angular.element(document).find("#containerpart").LoadingOverlay("show");

                            $scope.hometopics=forumsService.getforuminfo(forumid,
                                function(response){
                                    switch (response.status) {

                                        case 401: toastr.error('Credentials Incorrect'); break;
                                        case 200:{
                                            $scope.mainforum=response.data.data;
                                        }break;

                                    }
                                    angular.element(document).find("#containerpart").LoadingOverlay("hide");

                                },
                                function(response){
                                    switch (response.status)
                                    {
                                        case 401: toastr.error('Credentials Incorrect'); break;
                                        case 422: toastr.error('Not Found'); break;
                                        default : toastr.error('Server Error Check Again please !'); break;
                                    }
                                    angular.element(document).find("#containerpart").LoadingOverlay("hide");
                                }
                            );
                        }


            $scope.initforumcontr=function()
            {

                $scope.task='';

                 if( $stateParams.forumid!=null && $stateParams.forumid!=undefined &&  $stateParams.forumid!='')
                 {
                                $scope.pageparm= 1;
                                $scope.forumidparm=$stateParams.forumid;

                                if($stateParams.page!=null && $stateParams.page!=undefined && $stateParams.page!='')
                                    $scope.pageparm= $stateParams.page


                                $scope.prepageparm=parseInt($scope.pageparm)-1;
                                $scope.nextpageparm=parseInt($scope.pageparm)+1;
                                $scope.getforumtopics($stateParams.forumid,$scope.pageparm);
                                $scope.getforuminfo($stateParams.forumid);
                 }
                 else
                     $state.go('forums');
            };

        }]);
})();
