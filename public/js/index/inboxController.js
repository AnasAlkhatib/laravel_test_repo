/**
 * Created by anas on 4/20/17.
 */

// public/scripts/GroupsController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('InboxController',['inboxService','$scope','$sce','$auth','$stateParams','userauthService','toastr','$state','$rootScope',
                    'utilitiesServices',
            function (inboxService,$scope, $sce,$auth,$stateParams,userauthService,toastr,$state,$rootScope,utilitiesServices) {


            $scope.cleanhtml = function(text) {
                return utilitiesServices.cleanhtml(text);
            };


            $scope.checkthreadbetween = function (recipient_id)     {
                    //console.log(recipient_id);
                    inboxService.check_thread_between(
                        recipient_id,
                        function(response){


                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 200:{
                                    // console.log(response.data.data.thread_id);
                                    $state.go('inboxthread', {'thread':response.data.data.thread_id}, {reload: true});
                                }break;
                                case 202:{

                                    $scope.postnewthread=1;

                                    // $state.go('inboxthread/'+thread.thread_id, null, {reload: 'professionals'});
                                }break;


                            }
                        },
                        function(response){

                            switch (error.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422:{ toastr.error('Validation Error '+response.message);} break;
                                default: toastr.error(error.status+'Server Error Check Again please !'); break;
                            }

                            $state.go('inbox', null, {reload: 'inbox'});
                        });


                }

            $scope.postthread = function (message,recipient_id,$event)  {

                    $event.preventDefault();
                    angular.element(document).find("#thread_messages").LoadingOverlay("show");

                    if( angular.element('#thread_message_text_id').val()==""||recipient_id=="") {
                        angular.element(document).find("#thread_messages").LoadingOverlay("hide");
                        toastr.warning('error sending try again');
                        return false;
                    }

                    inboxService.post_thread(
                        angular.element('#thread_message_text_id').val(),
                        recipient_id,
                        function(response){

                            // angular.element(document).find("#thread_messages").LoadingOverlay("hide");
                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 200:{

                                    $scope.getactivethread(response.data.data.thread_id);
                                    $scope.getactivethreadinfo(response.data.data.thread_id);

                                    angular.element(document).find("#messages_view").append(
                                        '<section id="messages_view">'+
                                        '<div  class="shadow-wrapper pull-right width-100">'+
                                        '<blockquote class="chat-box hero box-shadow shadow-effect-4 white-background pull-right">'+
                                        '<p>'+angular.element('#message_text').val()+'</p>'+
                                        '<small class="font-size-10">'+response.data.data.updated_at+'</small>'+
                                        '</blockquote> </div> </section>'
                                    );
                                    angular.element(document).find("#message_post_area").LoadingOverlay("hide");
                                    angular.element('#message_text').val('');

                                }break;

                            }
                        },
                        function(error){


                            angular.element(document).find("#thread_messages").LoadingOverlay("hide");
                            switch (error.status)
                            {

                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422:{ toastr.error('Validation Error '+error.message);} break;
                                default: toastr.error(error.status+'Server Error Check Again please !'); break;
                            }

                            $state.go('inbox', null, {reload: 'inbox'});
                        });


                }

            $scope.postmessage = function (thread_id, message,recipient_id,$event)    {

                    $event.preventDefault();
                    angular.element(document).find("#message_post_area").LoadingOverlay("show");

                    if( angular.element('#message_text').val()=="") {
                        angular.element(document).find("#message_post_area").LoadingOverlay("hide");
                        toastr.warning('please type your message to post');
                        return false;
                    }



                    inboxService.post_message(
                        thread_id,
                        angular.element('#message_text').val(),
                        $scope.activethreadinfo.chat_partner.id,
                        function(response){

                            angular.element(document).find("#message_post_area").LoadingOverlay("hide");
                            switch (response.status) {

                                case 401:alert('Credentials Incorrect');break;
                                case 200:{


                                    angular.element(document).find("#messages_view").append(
                                        '<section id="messages_view">'+
                                        '<div  class="shadow-wrapper pull-right width-100">'+
                                        '<blockquote class="chat-box hero box-shadow shadow-effect-4 white-background pull-right">'+
                                        '<p>'+angular.element('#message_text').val()+'</p>'+
                                        '<small class="font-size-10">'+response.data.data.updated_at+'</small>'+
                                        '</blockquote> </div> </section>'
                                    );
                                    angular.element(document).find("#message_post_area").LoadingOverlay("hide");
                                    angular.element('#message_text').val('');

                                }break;

                            }
                        },
                        function(error)
                        {
                            angular.element(document).find("#message_post_area").LoadingOverlay("hide");
                            switch (error.status)
                            {

                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422:{ toastr.error('Validation Error '+response.message);} break;
                                default: toastr.error(error.status+'Server Error Check Again please !'); break;
                            }
                        });


                }

            $scope.getmythreads = function () {
                    angular.element(document).find("#inboxlist").LoadingOverlay("show");

                    $scope.homethreads=inboxService.getmythreads(9,0,
                        function(response){
                            angular.element(document).find("#inboxlist").LoadingOverlay("hide");

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 200:{
                                    $scope.homethreads=response.data.data;
                                    $scope.pagination=response.data.meta;


                                }break;

                            }
                        },
                        function(response){
                            angular.element(document).find("#inboxlist").LoadingOverlay("hide");

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 422:{

                                    toastr.error('Sorry Error Happened');


                                }break;



                            }
                        }
                    );


                }

            $scope.getactivethread = function (thread) {



                    $scope.activethreadmessages=inboxService.get_thread_message(thread,100,0,
                        function(response){

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect'); break;
                                case 200:{
                                    $scope.activethreadmessages=response.data.data;
                                    $scope.activethreadpagination=response.data.meta;

                                }break;

                            }


                        },
                        function(error){


                            switch (error.status) {

                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422:{ toastr.error('Validation Error '+error.message);} break;
                                default: toastr.error(error.status+'Server Error Check Again please !'); break;



                            }
                        }
                    );


                }

            $scope.getactivethreadinfo = function (thread_id)   {

                    $scope.activethreadinfo=inboxService.get_thread_info(thread_id,
                        function(response)
                        {

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect'); break;
                                case 200:{
                                    $scope.activethreadinfo=response.data.data;

                                }break;
                            }

                        },
                        function(error){

                            switch (response.status) {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422:{ toastr.error('Validation Error '+error.message);} break;
                                default: toastr.error(error.status+'Server Error Check Again please !'); break;
                            }
                        }
                    );
                }

            $scope.initinboxcontr=function()     {
                $scope.postnewthread=0;

                if($stateParams.thread!=null && $stateParams.thread!=undefined && $stateParams.thread!='' &&
                    $stateParams.section!=null && $stateParams.section!=undefined && $stateParams.section!='' && $stateParams.section=='newthread')
                {
                   // $scope.currentuser=$rootScope.currentUser;
                    $scope.recipientid=$stateParams.thread;
                    $scope.checkthreadbetween($scope.recipientid);

                }
                else if($stateParams.thread!=null && $stateParams.thread!=undefined && $stateParams.thread!='' &&  $stateParams.section!='newthread')
                {

                    $scope.activethreadid=$stateParams.thread;
                    $scope.getactivethread($stateParams.thread);
                    $scope.getactivethreadinfo($stateParams.thread);
                }


               // $scope.currentuser=$rootScope.currentUser;
                $scope.getmythreads();
            };


        }]);
})();
