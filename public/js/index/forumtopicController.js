/**
 * Created by anas on 4/20/17.
 */

// public/scripts/ForumsController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('ForumtopicController',['forumsService','$scope','$sce','$stateParams','$state','blogsService',
                    'storeService','classifiedsService','articlesService','$rootScope','toastr',
            function (forumsService,$scope, $sce,$stateParams,$state,blogsService,storeService,classifiedsService,articlesService,$rootScope,toastr) {




                $scope.trusthtml = function(text) {
                    return $sce.trustAsHtml(text);
                };

                $scope.seteditoroptions=function(){
                    $scope.htmlEditor = '';
                    // setup editor options
                    $scope.editorOptions = {
                        language: 'en',
                        uiColor: '#ffffff'
                    };
                };

                $scope.gettopicposts = function (topicid,page) {

                    angular.element(document).find("#containerpart").LoadingOverlay("show");


                    $scope.homeposts=forumsService.gettopicposts(topicid,6,page,
                        function(response){
                            switch (response.status) {

                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 200:{
                                    $scope.homeposts=response.data.data;
                                    $scope.pagination=response.data.meta.pagination;

                                    if($scope.homeposts=='' && page>1)
                                        $state.go('topicdetails', {'topicid': topicid,'page':1}, {});
                                }break;

                            }
                            angular.element(document).find("#containerpart").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#containerpart").LoadingOverlay("hide");
                        }
                    );


                }

                $scope.gettopicinfo = function (topicid) {

                    angular.element(document).find("#topic_main").LoadingOverlay("show");

                    $scope.maintopic=forumsService.getforumtopic(topicid,
                        function(response){
                            switch (response.status) {

                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 200:{
                                    $scope.maintopic=response.data.data;
                                }break;
                            }
                            angular.element(document).find("#topic_main").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#topic_main").LoadingOverlay("hide");
                        }
                    );
                }


                $scope.sidebar_latestarticles=function()  {
                    angular.element(document).find("#latestarticles").LoadingOverlay("show");

                    $scope.sidebarlatestarticles=articlesService.getlatestarticles('small',4,0,0,
                        function(response){
                            switch (response.status)
                            {
                                case 401:console.log('Credentials Incorrect');break;
                                case 200: $scope.sidebarlatestarticles=response.data.data;break;
                            }
                            angular.element(document).find("#latestarticles").LoadingOverlay("hide");
                        },
                        function(error)
                        {
                            switch (error.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422: console.log('Validation Error '+response.message); break;
                                default: console.log(error.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestarticles").LoadingOverlay("hide");
                        }
                    );
                }

                $scope.sidebar_latestblogs=function()     {
                    angular.element(document).find("#latestblogs").LoadingOverlay("show");

                    $scope.sidebarlatestblogs=blogsService.getlatestblogposts(4,0,$scope.blog_id,'latest',
                        function(response){
                            switch (response.status)
                            {
                                case 401:console.log('Credentials Incorrect');break;
                                case 200:$scope.sidebarlatestblogs=response.data.data;break;
                            }
                            angular.element(document).find("#latestblogs").LoadingOverlay("hide");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422: console.log('Validation Error '+response.message); break;
                                default: console.log(response.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestblogs").LoadingOverlay("hide");
                        }
                    );
                }

                $scope.sidebar_latestclassifieds=function()   {
                    angular.element(document).find("#latestclassifieds").LoadingOverlay("show");

                    if($rootScope.isAuthenticated()==1)
                        $scope.sidebarlatestclassifieds=classifiedsService.getclassifieds('small',4,0,
                            function(response)
                            {
                                switch (response.status)
                                {
                                    case 401: console.log('Credentials Incorrect');break;
                                    case 200: $scope.sidebarlatestclassifieds=response.data.data;break;
                                }
                                angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                            },
                            function(error)
                            {
                                switch (error.status)
                                {
                                    case 401:console.log('Credentials Incorrect'); break;
                                    case 422:console.log('Validation Error '+response.message); break;
                                    default: console.log(error.status+'Server Error Check Again please !'); break;
                                }
                                angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                            }
                        );
                    else
                    {
                        $scope.sidebarlatestclassifieds = 0;
                        angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                    }

                }

                $scope.sidebar_latestproducts=function()      {
                    angular.element(document).find("#latestproducts").LoadingOverlay("show");

                    if($rootScope.isAuthenticated()==1)
                        $scope.sidebarlatestproducts=storeService.getproducts('small',9,0,
                            function(response)
                            {
                                switch (response.status)
                                {
                                    case 401:console.log('Credentials Incorrect');break;
                                    case 200:$scope.sidebarlatestproducts=response.data.data;break;
                                }
                                angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                            },
                            function(error)
                            {
                                switch (error.status)
                                {
                                    case 401: console.log('Credentials Incorrect'); break;
                                    case 422: console.log('Validation Error '+response.message); break;
                                    default : console.log(error.status+'Server Error Check Again please !'); break;
                                }
                                angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                            }
                        );
                    else
                    {
                        $scope.sidebarlatestproducts =0;
                        angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                    }


                }


                $scope.addforumpost = function (e) {

                    $scope.seteditoroptions();

                    window.scrollTo(0,0);
                    angular.element(document).find("#content_area").LoadingOverlay("show");

                    var forumpost = {

                        title: angular.element(document).find("#addposttitle").val(),
                        content: $scope.postcontent,
                        topic_id: angular.element(document).find("#topic_id").val(),
                    }



                    if( forumpost.topic_id =="" || forumpost.title=="" || forumpost.content==null || forumpost.content=='')
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#content_area").LoadingOverlay("hide");
                        return false;
                    }



                    forumsService.addforumpost(forumpost,
                        function(response){

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('Post added');
                                    $scope.task='';
                                    $scope.postcontent='';
                                    angular.element(document).find("#addposttitle").val('');

                                    $scope.gettopicposts($scope.topicidparm,$scope.pageparm);
                                }break;
                            }
                            angular.element(document).find("#content_area").LoadingOverlay("hide");

                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            $scope.task='';

                            angular.element(document).find("#content_area").LoadingOverlay("hide");

                        }
                    );


                }

                $scope.editforumpost = function (id) {
                    window.scrollTo(0,0);
                    $scope.task='editpost';
                    angular.element(document).find("#containerpart").LoadingOverlay("show");
                    $scope.seteditoroptions();

                    forumsService.getmyforumpost(id,
                        function(response){

                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    $scope.editforumpostval=response.data.data;
                                    $scope.modelupdateforumpost=$scope.trusthtml($scope.editforumpostval.text);

                                }break;
                            }
                            angular.element(document).find("#containerpart").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            $scope.task='';
                            angular.element(document).find("#containerpart").LoadingOverlay("hide");

                        }
                    );
                }

                $scope.saveforumpost = function (e)  {
                    window.scrollTo(0,0);
                    $scope.seteditoroptions();
                    $scope.task='editpost';

                    angular.element(document).find("#containerpart").LoadingOverlay("show");

                    var forumpost = {
                        id   :angular.element(document).find("#editpost_id").val(),
                        title: angular.element(document).find("#title").val(),
                        content: $scope.modelupdateforumpost,
                        topic_id: angular.element(document).find("#post_edit_topic_id").val(),
                    }



                    if( forumpost.id ==""|| forumpost.topic_id =="" || forumpost.title=="" || forumpost.content==null)
                    {
                        toastr.error('Missing Information');
                        angular.element(document).find("#containerpart").LoadingOverlay("hide");
                        return false;
                    }

                    forumsService.savepost(forumpost,
                        function(response){
                            switch (response.status) {

                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{

                                    toastr.success('Post updated');
                                    $scope.task='';
                                }break;
                            }
                            angular.element(document).find("#containerpart").LoadingOverlay("hide");

                        },
                        function(response){

                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            $scope.task='';
                            angular.element(document).find("#containerpart").LoadingOverlay("hide");
                        }
                    );
                }

                $scope.cancel_add=function(e)   {
                    $scope.task='';
                }


                $scope.new_post=function()  {
                    $scope.seteditoroptions();
                    $scope.task='addpost';
                    window.scrollTo(0,0);
                    return;
                }


                $scope.deleteforumpost = function (id) {

                    angular.element(document).find("#containerpart").LoadingOverlay("show");
                    forumsService.deleteforumpost(id,
                        function(response){

                            switch (response.status) {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:{
                                    toastr.success('Post deleted');
                                    angular.element(document).find("#post_"+id).remove();
                                    $scope.maintopic.posts_count=$scope.maintopic.posts_count-1;
                                }break;
                            }
                            angular.element(document).find("#containerpart").LoadingOverlay("hide");
                        },
                        function(response){
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#containerpart").LoadingOverlay("hide");
                        }
                    );
                }

                $scope.inittopicontr=function()
                {
                    $scope.task='';
                    $scope.postcontent='';


                    if( $stateParams.topicid!=null && $stateParams.topicid!=undefined &&  $stateParams.topicid!='')
                    {
                        $scope.pageparm= 1;
                        $scope.topicidparm=$stateParams.topicid;

                        if($stateParams.page!=null && $stateParams.page!=undefined &&  $stateParams.page!='')
                            $scope.pageparm= $stateParams.page


                        $scope.prepageparm=parseInt($scope.pageparm)-1;
                        $scope.nextpageparm=parseInt($scope.pageparm)+1;
                        $scope.gettopicposts($stateParams.topicid,$scope.pageparm);
                        $scope.gettopicinfo($stateParams.topicid);
                    }
                    else {
                        alert( $stateParams.topicid);
                        $state.go('forums');
                    }

                };








            }]);
})();
