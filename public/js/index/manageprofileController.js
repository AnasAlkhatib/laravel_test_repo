/**
 * Created by anas on 4/20/17.
 */

// public/scripts/ArticlesController.js

(function() {

    'use strict';

    angular
        .module('fadApp'  )
        .directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function(){
                    scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
        }])
        .controller('ManageprofileController',['myprofileServices','$scope','$sce','forumsService','blogsService',
                    'classifiedsService','storeService','groupsService','$stateParams','eventsService','$state',
            'utilitiesServices','toastr',
            function (myprofileServices,$scope, sce,forumsService,blogsService,classifiedsService,storeService,
                      groupsService,$stateParams,eventsService,$state,utilitiesServices,toastr) {



        $scope.mylocale = {
                    formatDate: function(date) {
                        var m = moment(date);
                        return m.isValid() ? m.format('YYYY-MM-DD') : '';
                    }
                };

        $scope.dateoptions=function(){

             $scope.myDate = new Date();

             $scope.minDate = new Date(
                 $scope.myDate.getFullYear(),
                 $scope.myDate.getMonth() - 2,
                 $scope.myDate.getDate()
             );


             $scope.isOpen = false;

         }

        $scope.trusthtml = function(text) {
           // alert(sce.trustAsHtml(text));
            return sce.trustAsHtml(text);
        };

        $scope.seteditoroptions=function(){
            $scope.htmlEditor = '';
            // setup editor options
            $scope.editorOptions = {
                language: 'en',
                uiColor: '#ffffff'
            };
        };


        $scope.my_education=function()
        {
                   angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                   $scope.section='education';

                    $scope.myeducations=myprofileServices.getmyeducations(
                        function(response){
                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 200:
                                    {
                                      $scope.myeducations=response.data.data;
                                      $scope.pagination=response.data.meta.pagination;
                                    }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){

                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 422:toastr.error('Sorry Error Happened reload page'); break;
                                default: toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        }
                    );

         }

        $scope.deleteeducation = function (id)
        {

            angular.element(document).find("#dashboard_content").LoadingOverlay("show");
            myprofileServices.deleteeducation(id,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:{
                                          angular.element(document).find("#education_"+id).remove();

                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default: toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('manageprofile');

                        }
                    );


                }

        $scope.addeducation= function (e)
        {
            angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    var education = {
                        speciality: angular.element(document).find("#speciality").val(),
                        education_level: angular.element(document).find("#education_level").val(),
                        institute_name: angular.element(document).find("#institute_name").val(),
                        graduation_date: angular.element(document).find("#graduation_date").val(),
                    }


                    if( education.speciality=="" || education.education_level==""||education.institute_name=="" ||education.graduation_date=="")
                    {toastr.error('Missing Information');  angular.element(document).find("#dashboard_content").LoadingOverlay("hide");return false;}

                 myprofileServices.saveeducation(education,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    console.log(response.data.data);
                                    toastr.success('Education added');
                                    $state.go('manageprofile',{ section:'education',page:1 });

                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response)
                        {

                            switch (response.status) {

                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default: toastr.error('Server Error Check Again please !'); break;


                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('manageprofile');
                        }
                    );


                }

        $scope.showeducationform = function () {

                    $scope.htmlEditor = '...';
                    // setup editor options
                    $scope.editorOptions = {
                        uiColor: '#ffffff'
                    };

                    $scope.section='addeducation';
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                    return;
                }


        $scope.my_experience=function()
         {
                    angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    $scope.section='experience';

                    $scope.myexperiences=myprofileServices.getmyexperiences(
                        function(response){
                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 200:
                                {
                                    $scope.myexperiences=response.data.data;
                                    $scope.pagination=response.data.meta.pagination;
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){

                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 422:toastr.error('Sorry Error Happened reload page'); break;
                                default: toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        }
                    );

                }

        $scope.deleteexperience= function (id)
         {

                    angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    myprofileServices.deleteexperience(id,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:{
                                    angular.element(document).find("#experience_"+id).remove();

                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default: toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('manageprofile',{'section':'experience'});

                        }
                    );


                }

        $scope.showexperienceform = function () {

                    $scope.htmlEditor = '...';
                    // setup editor options
                    $scope.editorOptions = {
                        uiColor: '#ffffff'
                    };

                    $scope.section='addexperience';
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                    return;
                }

        $scope.addexperience= function (e)   {
                    angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    var experience = {
                        title: angular.element(document).find("#title").val(),
                        sumary: angular.element(document).find("#sumary").val(),
                        from_date: angular.element(document).find("#from_date").val(),
                        to_date: angular.element(document).find("#to_date").val(),
                    }


                    if( experience.title=="" || experience.sumary=="")
                    {toastr.error('Missing Information');  angular.element(document).find("#dashboard_content").LoadingOverlay("hide");return false;}

                    myprofileServices.saveexperience(experience,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{

                                    toastr.success('experience added');
                                    $state.go('manageprofile',{ section:'experience',page:1 });

                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response)
                        {

                            switch (response.status) {

                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default: toastr.error('Server Error Check Again please !'); break;


                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('manageprofile');
                        }
                    );


                }

        $scope.my_courses=function()   {
                    angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    $scope.section='courses';

                    $scope.mycourses=myprofileServices.getmycourses(
                        function(response){
                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 200:
                                {
                                    $scope.mycourses=response.data.data;
                                    $scope.pagination=response.data.meta.pagination;
                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){

                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 422:toastr.error('Sorry Error Happened reload page'); break;
                                default: toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        }
                    );

                }

        $scope.deletecourse= function (id)  {

                    angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    myprofileServices.deletecourse(id,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:{
                                    angular.element(document).find("#course_"+id).remove();

                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default: toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('manageprofile',{'section':'courses'});

                        }
                    );


                }

        $scope.showcourseform = function () {

                    $scope.htmlEditor = '...';
                    // setup editor options
                    $scope.editorOptions = {
                        uiColor: '#ffffff'
                    };

                    $scope.section='addcourse';
                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                    return;
                }

        $scope.addcourse= function (e)
        {
                    angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    var course = {
                        title: angular.element(document).find("#coursetitle").val(),
                        sumary: angular.element(document).find("#coursesumary").val()
                    }


                    if( course.title=="" || course.course=="")
                    {toastr.error('Missing Information');  angular.element(document).find("#dashboard_content").LoadingOverlay("hide");return false;}

                    myprofileServices.savecourse(course,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('Course added');
                                    $state.go('manageprofile',{ section:'courses',page:1 });

                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response)
                        {

                            switch (response.status) {

                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default: toastr.error('Server Error Check Again please !'); break;


                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('manageprofile');
                        }
                    );


                }

        $scope.getmyavatar=function()
        {
                    angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    $scope.section='avatar';

                    $scope.mycourses=myprofileServices.getmyprofileimage(
                        function(response){
                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 200:
                                {
                                    $scope.myavatar=response.data.data;

                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        },
                        function(response){

                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 422:toastr.error('Sorry Error Happened reload page'); break;
                                default: toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                        }
                    );

                }

        $scope.upateavatar= function (e)
            {
                    angular.element(document).find("#dashboard_content").LoadingOverlay("show");

                    var avatar = {
                        picture_url:''
                    }

                   if($scope.avatarimage) avatar.picture_url=$scope.avatarimage;

                    if( avatar.picture_url=="" )
                    {toastr.error('Missing Information');  angular.element(document).find("#dashboard_content").LoadingOverlay("hide");return false;}

                    myprofileServices.updateavatar(avatar,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401:toastr.error('Credentials Incorrect');break;
                                case 202:toastr.error('Error Happened');break;
                                case 200:{
                                    toastr.success('Profile Image updated');
                                    $state.go('manageprofile',{ section:'avatar',page:1 });

                                }break;
                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                        },
                        function(response)
                        {

                            switch (response.status) {

                                case 401: toastr.error('Credentials Incorrect'); break;
                                case 422: toastr.error('Not Found'); break;
                                default: toastr.error('Server Error Check Again please !'); break;


                            }
                            angular.element(document).find("#dashboard_content").LoadingOverlay("hide");
                            $state.go('manageprofile');
                        }
                    );


                }

         $scope.addavatar=function()
                {
                    angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                    $scope.section='addavatar';

                    angular.element(document).find("#dashboard_content").LoadingOverlay("hide");

                }


        $scope.initdashboardcontr=function()
        {
            $scope.pageparm= 1;
            if($stateParams.page!=null && $stateParams.page!=undefined &&  $stateParams.page!='')
                        $scope.pageparm= $stateParams.page


            $scope.prepageparm=parseInt($scope.pageparm)-1;
            $scope.nextpageparm=parseInt($scope.pageparm)+1;

            if($stateParams.section!=null ||$stateParams.section!=undefined)
            {
                 switch ($stateParams.section)
                 {
                     case 'education'     :$scope.my_education();break;
                     case 'addeducation'  :
                                          {   angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                                              $scope.showeducationform();}break;


                     case 'experience'     :$scope.my_experience();break;
                     case 'addexperience'  :
                                              {   angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                                                  $scope.showexperienceform();
                                              }break;


                     case 'courses'     :$scope.my_courses();break;
                     case 'addcourse'  :
                                         {   angular.element(document).find("#dashboard_content").LoadingOverlay("show");
                                             $scope.showcourseform();
                                          }break;


                     case 'avatar'          :  $scope.getmyavatar();break;

                     case 'updateavatar'    :  $scope.addavatar();break;

                     /*case 'experience'    :$scope.my_articles($scope.pageparm);break;
                     case 'addexperience' :$scope.showarticleform();break;
                     case 'editexperience':$scope.editarticle($scope.pageparm);break;
                     case 'courses'       :$scope.my_articles($scope.pageparm);break;
                     case 'addcourse'     :$scope.showarticleform();break;
                     case 'editcourse'    :$scope.editarticle($scope.pageparm);break;
                     case 'avatar'        :$scope.my_articles($scope.pageparm);break;
                     case 'editavatar'    :$scope.editarticle($scope.pageparm);break;
                     */

                     default :{
                        // $scope.my_education();
                       //  $state.go('dashboardsection',{section:'articles'});
                         toastr.error('Section Not found');
                         $state.go('manageprofile',{section:'education'});

                               }
                               break;
                 }
            }

            else
            {
                $scope.my_education();
            }
        }




    }]);



})();
