/**
 * Created by anas on 4/20/17.
 */

// public/scripts/authController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('QuestionController',['questionsService','classifiedsService','$scope','$sce','$routeParams','forumsService',
                     'articlesService','blogsService','storeService','$stateParams','$state','toastr','$rootScope',
            function (questionsService,classifiedsService,$scope, $sce,$routeParams,forumsService,articlesService,blogsService
                      ,storeService,$stateParams,$state,toastr,$rootScope) {




            $scope.trusthtml = function(text) {
                return $sce.trustAsHtml(text);
            };

            $scope.sidebar_latestforums=function(){

                angular.element(document).find("#containerpart2").LoadingOverlay("show");

                    $scope.sidebarlatestposts=forumsService.getlatestposts(4,1,
                        function(response){
                            switch (response.status) {

                                case 401:console.log('Credentials Incorrect'); break;
                                case 200:{
                                    $scope.sidebarlatestposts=response.data.data;



                                }break;

                            }
                            angular.element(document).find("#containerpart2").LoadingOverlay("show");
                        },
                        function(response){
                            switch (error.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422: console.log('Validation Error '+response.message); break;
                                default : console.log(error.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                        }
                    );
                }

            $scope.sidebar_latestarticles=function()  {
                    angular.element(document).find("#latestarticles").LoadingOverlay("show");

                    $scope.sidebarlatestarticles=articlesService.getlatestarticles('small',4,0,0,
                        function(response){
                            switch (response.status)
                            {
                                case 401:console.log('Credentials Incorrect');break;
                                case 200: $scope.sidebarlatestarticles=response.data.data;break;
                            }
                            angular.element(document).find("#latestarticles").LoadingOverlay("hide");
                        },
                        function(error)
                        {
                            switch (error.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422: console.log('Validation Error '+response.message); break;
                                default: console.log(error.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestarticles").LoadingOverlay("hide");
                        }
                    );
                }

            $scope.sidebar_latestblogs=function()    {
                    angular.element(document).find("#latestblogs").LoadingOverlay("show");

                    $scope.sidebarlatestblogs=blogsService.getlatestblogposts(4,0,$scope.blog_id,'latest',
                        function(response){
                            switch (response.status)
                            {
                                case 401:console.log('Credentials Incorrect');break;
                                case 200:$scope.sidebarlatestblogs=response.data.data;break;
                            }
                            angular.element(document).find("#latestblogs").LoadingOverlay("hide");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422: console.log('Validation Error '+response.message); break;
                                default: console.log(response.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestblogs").LoadingOverlay("hide");
                        }
                    );
                }

            $scope.sidebar_latestproducts=function()   {
                    angular.element(document).find("#latestproducts").LoadingOverlay("show");


                        $scope.sidebarlatestproducts=storeService.getproducts('small',9,0,
                            function(response)
                            {
                                switch (response.status)
                                {
                                    case 401:console.log('Credentials Incorrect');break;
                                    case 200:$scope.sidebarlatestproducts=response.data.data;break;
                                }
                                angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                            },
                            function(error)
                            {
                                switch (error.status)
                                {
                                    case 401: console.log('Credentials Incorrect'); break;
                                    case 422: console.log('Validation Error '+response.message); break;
                                    default : console.log(error.status+'Server Error Check Again please !'); break;
                                }
                                angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                            }
                        );

                }

            $scope.sidebar_latestclassifieds=function()    {
                    angular.element(document).find("#latestclassifieds").LoadingOverlay("show");


                        $scope.sidebarlatestclassifieds=classifiedsService.getclassifieds('small',4,0,
                            function(response)
                            {
                                switch (response.status)
                                {
                                    case 401: console.log('Credentials Incorrect');break;
                                    case 200: $scope.sidebarlatestclassifieds=response.data.data;break;
                                }
                                angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                            },
                            function(error)
                            {
                                switch (error.status)
                                {
                                    case 401:console.log('Credentials Incorrect'); break;
                                    case 422:console.log('Validation Error '+response.message); break;
                                    default: console.log(error.status+'Server Error Check Again please !'); break;
                                }
                                angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                            }
                        );


                }

            $scope.getquestion = function (questionid) {


                 angular.element(document).find("#containerpart").LoadingOverlay("show");

                $scope.question=questionsService.getquestion(questionid,
                    function(response){

                        switch (response.status)
                        {
                            case 401:toastr.error('Credentials Incorrect');break;
                            case 200:{
                                $scope.question=response.data.data;

                                if($scope.question==null) {
                                    toastr.error('Not Found');
                                    $state.go('questions');
                                }
                            }break;
                            case 202:{toastr.error('Not Found!'); $state.go('questions');}break;
                        }
                        angular.element(document).find("#containerpart").LoadingOverlay("hide");
                    },
                    function(error)
                    {
                        switch (error.status)
                        {
                            case 401: toastr.error('Credentials Incorrect'); break;
                            case 422: toastr.error('Validation Error '+error.message); break;
                            default: toastr.error(error.status+'Server Error Check Again please !'); break;
                        }
                        angular.element(document).find("#containerpart").LoadingOverlay("hide");
                        $state.go('questions');
                    }

                );
            }




            $scope.initquestionscontr=function(){

                if(
                    $stateParams.id!=null && $stateParams.id!=undefined && $stateParams.id!='')

                    $scope.getquestion($stateParams.id);

                else
                    $state.go('questions');
            };


        }]);
})();
