/**
 * Created by anas on 4/20/17.
 */

// public/scripts/GroupsController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('ResetPasswordController',['activationServices','$scope','$sce','$stateParams','$state','toastr',
            function (activationServices,$scope, $sce,$stateParams,$state,toastr) {



            $scope.resetuserpassword = function ()   {

                    angular.element(document).find("#containerpart").LoadingOverlay("show");

                var reset_password = {
                    email: angular.element(document).find("#reset_email").val(),
                    password: angular.element(document).find("#reset_password").val(),
                    confirm_password: angular.element(document).find("#reset_confirm_password").val(),
                    token: $scope.token,

                }

                if( reset_password.email=="" || reset_password.password==""||reset_password.confirm_password=="" ||reset_password.token=="")
                {   toastr.error('Missing Information');
                    angular.element(document).find("#containerpart").LoadingOverlay("hide");
                    return false;
                }

                if( reset_password.password!=reset_password.confirm_password)
                {   toastr.error('Password and Confirm Password must be the same !');
                    angular.element(document).find("#containerpart").LoadingOverlay("hide");
                    return false;
                }

                activationServices.resetpasswordaccount(reset_password,
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401:toastr.error('The password reset token or Email is invalid');break;
                                case 202:{
                                    toastr.success('Password Reset Successfully ');
                                    $state.go('/');
                                }break;
                            }
                            angular.element(document).find("#containerpart").LoadingOverlay("hide");

                        },
                        function(response)
                        {
                            switch (response.status)
                            {
                                case 401: toastr.error('The password reset token or Email is invalid'); break;
                                case 422: toastr.error('Not Found'); break;
                                default : toastr.error('Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#containerpart").LoadingOverlay("hide");
                        }
                    );

                }

            $scope.initresetCont=function()
            {
                $scope.token='';

                if($stateParams.token!=null && $stateParams.token!=undefined &&   $stateParams.token!='')
                  {

                      $scope.token=$stateParams.token;

                  }

                else
                        $state.go('/');
            };








        }]);
})();
