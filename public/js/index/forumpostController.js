/**
 * Created by anas on 4/20/17.
 */

// public/scripts/ForumsController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('ForumPostController',['forumsService','$scope','$sce','$stateParams','$state','blogsService',
                    'storeService',
            function (forumsService,$scope, $sce,$stateParams,$state,blogsService,storeService) {




                $scope.trusthtml = function(text) {
                    return $sce.trustAsHtml(text);
                };


                $scope.getpost = function (postid) {

                    $scope.homepost=forumsService.getforumpost(postid,
                        function(response){
                            switch (response.status) {

                                case 401:alert('Credentials Incorrect');break;
                                case 200:{
                                    $scope.homepost=response.data.data;
                                    angular.element(document).find("#containerpart").removeClass( "spinner" );
                                    // alert('Great! Data Loaded');
                                    console.log(response.data.data);
                                }break;

                            }
                        },
                    function(response){
                        switch (response.status) {

                            case 401:alert('Credentials Incorrect');break;
                            case 422:{

                                 alert('Sorry Error Happened');
                                 $state.go('forums');

                            }break;

                        }
                    }
                    );


                }



                $scope.sidebar_latestforums=function(){

                    $scope.sidebarlatestposts=forumsService.getlatestposts(4,1,
                        function(response){
                            switch (response.status) {

                                case 401:alert('Credentials Incorrect');break;
                                case 200:{
                                    $scope.sidebarlatestposts=response.data.data;
                                    angular.element(document).find("#latestforums").removeClass( "spinner" );
                                    // alert('Great! Data Loaded');
                                    console.log(response.data.data);
                                }break;

                            }
                        },
                        function(response){
                            switch (response.status) {

                                case 401:alert('Credentials Incorrect');break;
                                case 422:{

                                    alert('Sorry Error Happened');
                                    $state.go('forums');

                                }break;

                            }
                        }
                    );
                }


                $scope.sidebar_latestblogs=function(){

                    $scope.sidebarlatestblogs=blogsService.getlatestblogposts(4,1,
                        function(response){
                            switch (response.status) {

                                case 401:alert('Credentials Incorrect');break;
                                case 200:{
                                    $scope.sidebarlatestblogs=response.data.data;
                                    angular.element(document).find("#latestblogs").removeClass( "spinner" );
                                    // alert('Great! Data Loaded');
                                    console.log(response.data.data);
                                }break;

                            }
                        },
                        function(response){
                            switch (response.status) {

                                case 401:alert('Credentials Incorrect');break;
                                case 422:{

                                    alert('Sorry Error Happened');
                                    $state.go('forums');

                                }break;

                            }
                        }
                    );
                }

                $scope.sidebar_latestproducts=function(){

                    $scope.sidebarlatestproducts=storeService.getproducts('small',9,0,
                        function(response){
                            switch (response.status) {

                                case 401:alert('Credentials Incorrect');break;
                                case 200:{
                                    $scope.sidebarlatestproducts=response.data.data;
                                    angular.element(document).find("#latestproducts").removeClass( "spinner_sidebar" );
                                    // alert('Great! Data Loaded');
                                    // console.log(response.data.data);
                                }break;

                            }


                        }
                    );



                    $scope.sidebarlatestblogs=blogsService.getlatestblogposts(4,0,
                        function(response){
                            switch (response.status) {

                                case 401:alert('Credentials Incorrect');break;
                                case 200:{
                                    $scope.sidebarlatestblogs=response.data.data;
                                    angular.element(document).find("#latestblogs").removeClass( "spinner_sidebar" );
                                    // alert('Great! Data Loaded');
                                    console.log(response.data.data);
                                }break;

                            }
                        },
                        function(response){
                            switch (response.status) {

                                case 401:alert('Credentials Incorrect');break;
                                case 422:{

                                    alert('Sorry Error Happened');
                                    $state.go('forums');

                                }break;

                            }
                        }
                    );
                }


                $scope.sidebar=function() {

                    $scope.sidebar_latestforums();
                    $scope.sidebar_latestblogs();
                }

                $scope.inittopicontr=function(){


                    if(
                        $stateParams.postid!=null && $stateParams.postid!=undefined &&
                        $stateParams.postid!='')
                    {

                        $scope.postidparm=$stateParams.postid;


                        $scope.getpost($scope.postidparm);
                    }
                    else {

                        $state.go('forums');
                    }



                };








            }]);
})();
