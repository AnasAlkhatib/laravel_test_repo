/**
 * Created by anas on 4/20/17.
 */

// public/scripts/ArticlesController.js

(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('ArticlesController',['articlesService','$scope','$sce','forumsService','blogsService',
                    'classifiedsService','storeService','$stateParams','$rootScope',
            function (articlesService,$scope, $sce,forumsService,blogsService,classifiedsService,storeService,$stateParams,$rootScope) {


        $scope.trusthtml = function(text) {
            return $sce.trustAsHtml(text);
        };

        $scope.sidebar_latestforums=function()
        {

                    angular.element(document).find("#latestforums").LoadingOverlay("show");

                    if($rootScope.isAuthenticated()==1)
                        $scope.sidebarlatestposts=forumsService.getlatestposts(4,1,
                            function(response)
                            {
                                switch (response.status)
                                {
                                    case 401:console.log('Credentials Incorrect');break;
                                    case 200: $scope.sidebarlatestposts=response.data.data;break;
                                }
                                angular.element(document).find("#latestforums").LoadingOverlay("hide");
                            },
                            function(error)
                            {
                                switch (error.status)
                                {
                                    case 401: console.log('Credentials Incorrect'); break;
                                    case 422: console.log('Validation Error '+response.message); break;
                                    default: console.log(error.status+'Server Error Check Again please !'); break;
                                }
                                angular.element(document).find("#latestforums").LoadingOverlay("hide");
                            }
                        );
                    else
                    {
                        $scope.sidebarlatestposts = 0;
                        angular.element(document).find("#latestforums").LoadingOverlay("hide");
                    }
                }

        $scope.sidebar_latestblogs=function()
        {
                    angular.element(document).find("#latestblogs").LoadingOverlay("show");

                    $scope.sidebarlatestblogs=blogsService.getlatestblogposts(4,0,0,'none',
                        function(response){
                            switch (response.status)
                            {
                                case 401:console.log('Credentials Incorrect');break;
                                case 200:$scope.sidebarlatestblogs=response.data.data;break;
                            }
                            angular.element(document).find("#latestblogs").LoadingOverlay("hide");

                        },
                        function(error)
                        {
                            switch (error.status)
                            {
                                case 401: console.log('Credentials Incorrect'); break;
                                case 422: console.log('Validation Error '+response.message); break;
                                default: console.log(error.status+'Server Error Check Again please !'); break;
                            }
                            angular.element(document).find("#latestblogs").LoadingOverlay("hide");
                        }
                    );
                }

        $scope.sidebar_latestclassifieds=function()
        {
                    angular.element(document).find("#latestclassifieds").LoadingOverlay("show");

                    if($rootScope.isAuthenticated()==1)
                        $scope.sidebarlatestclassifieds=classifiedsService.getclassifieds('small',4,0,
                            function(response)
                            {
                                switch (response.status)
                                {
                                    case 401: console.log('Credentials Incorrect');break;
                                    case 200: $scope.sidebarlatestclassifieds=response.data.data;break;
                                }
                                angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                            },
                            function(error)
                            {
                                switch (error.status)
                                {
                                    case 401:console.log('Credentials Incorrect'); break;
                                    case 422:console.log('Validation Error '+response.message); break;
                                    default: console.log(error.status+'Server Error Check Again please !'); break;
                                }
                                angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                            }
                        );
                    else
                    {
                        $scope.sidebarlatestclassifieds = 0;
                        angular.element(document).find("#latestclassifieds").LoadingOverlay("hide");
                    }

                }

        $scope.sidebar_latestproducts=function()
                {
                    angular.element(document).find("#latestproducts").LoadingOverlay("show");

                    if($rootScope.isAuthenticated()==1)
                        $scope.sidebarlatestproducts=storeService.getproducts('small',9,0,
                            function(response)
                            {
                                switch (response.status)
                                {
                                    case 401:console.log('Credentials Incorrect');break;
                                    case 200:$scope.sidebarlatestproducts=response.data.data;break;
                                }
                                angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                            },
                            function(error)
                            {
                                switch (error.status)
                                {
                                    case 401: console.log('Credentials Incorrect'); break;
                                    case 422: console.log('Validation Error '+response.message); break;
                                    default : console.log(error.status+'Server Error Check Again please !'); break;
                                }
                                angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                            }
                        );
                    else
                    {
                        $scope.sidebarlatestproducts =0;
                        angular.element(document).find("#latestproducts").LoadingOverlay("hide");
                    }


                }

        $scope.getarticles = function (page)
        {

            angular.element(document).find("#containerpart").LoadingOverlay("show",{image:"",fontawesome : "fa fa-spinner fa-spin"});

          $scope.articles=articlesService.getarticles('big',6,page,
                function(response)
                {
                    switch (response.status)
                    {
                        case 401:console.log('Credentials Incorrect');break;
                        case 200:
                            {
                              $scope.articles=response.data.data;
                              $scope.pagination=response.data.meta.pagination;
                            }break;
                    }
                    angular.element(document).find("#containerpart").LoadingOverlay("hide");

                },
              function(error)
              {
                  switch (error.status)
                  {
                      case 401: console.log('Credentials Incorrect'); break;
                      case 422:console.log('Validation Error '+response.message); break;
                      default: console.log(error.status+'Server Error Check Again please !'); break;
                  }
                  angular.element(document).find("#containerpart").LoadingOverlay("hide");
              }
            );


        }

        $scope.initarticlecontr=function(){

            $scope.pageparm= 1;

            if($stateParams.page!=null && $stateParams.page!=undefined && $stateParams.page!='')
               $scope.pageparm= $stateParams.page



            $scope.prepageparm=parseInt($scope.pageparm)-1;
            $scope.nextpageparm=parseInt($scope.pageparm)+1;
            $scope.getarticles($scope.pageparm);



        };


    }]);

})();
