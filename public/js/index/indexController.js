/**
 * Created by anas on 4/20/17.
 */

   /**
     * Created by anas on 4/20/17.
     */



    (function() {

        'use strict';

        angular
            .module('fadApp')
            .controller('IndexController',['questionsService','professionalsService','$scope','$sce','$routeParams','forumsService',
                'articlesService','blogsService','storeService','$stateParams','$state','toastr','$rootScope',
                function (questionsService,professionalsService,$scope, $sce,$routeParams,forumsService,articlesService,blogsService
                    ,storeService,$stateParams,$state,toastr,$rootScope) {




                    $scope.trusthtml = function(text) {
                        return $sce.trustAsHtml(text);
                    };

                    $scope.sidebar_latestforums=function(){

                        angular.element(document).find("#latestforums").LoadingOverlay("show");

                        $scope.sidebarlatestposts=forumsService.getlatestposts(4,1,
                            function(response){
                                switch (response.status) {

                                    case 401:console.log('Credentials Incorrect'); break;
                                    case 200:{
                                        $scope.sidebarlatestposts=response.data.data;



                                    }break;

                                }
                                angular.element(document).find("#latestforums").LoadingOverlay("hide");
                            },
                            function(response){
                                switch (error.status)
                                {
                                    case 401: console.log('Credentials Incorrect'); break;
                                    case 422: console.log('Validation Error '+response.message); break;
                                    default : console.log(error.status+'Server Error Check Again please !'); break;
                                }
                                angular.element(document).find("#latestforums").LoadingOverlay("hide");
                            }
                        );
                    }

                    $scope.sidebar_latestarticles=function()  {
                        angular.element(document).find("#latestarticles").LoadingOverlay("show");

                        $scope.sidebarlatestarticles=articlesService.getlatestarticles('small',6,0,0,
                            function(response){
                                switch (response.status)
                                {
                                    case 401:console.log('Credentials Incorrect');break;
                                    case 200:
                                        {
                                            $scope.allarticles=response.data.data;
                                            $scope.pagination=response.data.meta.pagination;


                                            if($scope.pagination.count>1) {
                                                $scope.main_article = $scope.allarticles.slice(0, 1);
                                                $scope.articles = $scope.allarticles.slice(1, 6);
                                            }

                                    }break;
                                }
                                angular.element(document).find("#latestarticles").LoadingOverlay("hide");
                            },
                            function(error)
                            {
                                switch (error.status)
                                {
                                    case 401: console.log('Credentials Incorrect'); break;
                                    case 422: console.log('Validation Error '+response.message); break;
                                    default: console.log(error.status+'Server Error Check Again please !'); break;
                                }
                                angular.element(document).find("#latestarticles").LoadingOverlay("hide");
                            }
                        );
                    }

                    $scope.sidebar_latestblogs=function()    {
                        angular.element(document).find("#latestblogs").LoadingOverlay("show");

                        $scope.sidebarlatestblogs=blogsService.getlatestblogposts(4,0,$scope.blog_id,'latest',
                            function(response){
                                switch (response.status)
                                {
                                    case 401:console.log('Credentials Incorrect');break;
                                    case 200:$scope.sidebarlatestblogs=response.data.data;break;
                                }
                                angular.element(document).find("#latestblogs").LoadingOverlay("hide");

                            },
                            function(response)
                            {
                                switch (response.status)
                                {
                                    case 401: console.log('Credentials Incorrect'); break;
                                    case 422: console.log('Validation Error '+response.message); break;
                                    default: console.log(response.status+'Server Error Check Again please !'); break;
                                }
                                angular.element(document).find("#latestblogs").LoadingOverlay("hide");
                            }
                        );
                    }

                    $scope.sidebar_getmembers = function ()    {
                        $scope.professionalsError='';
                        angular.element(document).find("#latestmembers").LoadingOverlay("show");

                        if($rootScope.isAuthenticated()==1)
                        $scope.homememebrs=professionalsService.getprofessionals('big',8,1,
                            function(response)
                            {
                                switch (response.status)
                                {
                                    case 401:toastr.error('Credentials Incorrect'); break;
                                    case 200:
                                    {
                                        $scope.homememebrs=response.data.data;

                                    }break;
                                }
                                angular.element(document).find("#latestmembers").LoadingOverlay("hide");
                            },
                            function(error)
                            {
                                switch (error.status)
                                {
                                    case 401:toastr.error('Credentials Incorrect'); break;
                                    case 422: toastr.error('Validation Error '+error.message); break;
                                    default:
                                    {
                                        toastr.error( 'Error happened reload page please');

                                    } break;
                                }
                                angular.element(document).find("#latestmembers").LoadingOverlay("hide");
                            }
                        );
                        else
                        {
                            $scope.homememebrs = 0;
                            angular.element(document).find("#latestmembers").LoadingOverlay("hide");
                        }


                    }


                    $scope.initindexcontr=function() {

                    }


                }]);
    })();
