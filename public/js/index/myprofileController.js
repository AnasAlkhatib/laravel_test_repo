/**
 * Created by anas on 4/20/17.
 */


(function() {

    'use strict';

    angular
        .module('fadApp')
        .controller('MyprofileController',['articlesService','$scope','$sce','forumsService','blogsService',
                    'classifiedsService','storeService','$stateParams','accountServices',
        function (articlesService,$scope, $sce,forumsService,blogsService,classifiedsService,storeService,
                  $stateParams,accountServices) {

            $scope.sidebar_latestforums=function(){

                $scope.sidebarlatestposts=forumsService.getlatestposts(4,1,
                    function(response){
                        switch (response.status) {

                            case 401:alert('Credentials Incorrect');break;
                            case 200:{
                                $scope.sidebarlatestposts=response.data.data;
                                angular.element(document).find("#latestforums").removeClass( "spinner" );
                                // alert('Great! Data Loaded');
                                console.log(response.data.data);
                            }break;

                        }
                    },
                    function(response){
                        switch (response.status) {

                            case 401:alert('Credentials Incorrect');break;
                            case 422:{

                                alert('Sorry Error Happened');
                                $state.go('forums');

                            }break;

                        }
                    }
                );
            }

            $scope.sidebar_latestarticles=function(){

                $scope.sidebarlatestarticles=articlesService.getlatestarticles('small',4,0,
                    function(response){
                        switch (response.status) {

                            case 401:alert('Credentials Incorrect');break;
                            case 200:{
                                $scope.sidebarlatestarticles=response.data.data;
                                angular.element(document).find("#latestarticles").removeClass( "spinner" );
                                // alert('Great! Data Loaded');
                                console.log(response.data.data);
                            }break;

                        }
                    },
                    function(response){
                        switch (response.status) {

                            case 401:alert('Credentials Incorrect');break;
                            case 422:{

                                alert('Sorry Error Happened');
                                $state.go('forums');

                            }break;

                        }
                    }
                );
            }

            $scope.sidebar_latestblogs=function(){

                $scope.sidebarlatestblogs=blogsService.getlatestblogposts(4,0,
                    function(response){
                        switch (response.status) {

                            case 401:alert('Credentials Incorrect');break;
                            case 200:{
                                $scope.sidebarlatestblogs=response.data.data;
                                angular.element(document).find("#latestblogs").removeClass( "spinner_sidebar" );
                                // alert('Great! Data Loaded');
                                console.log(response.data.data);
                            }break;

                        }
                    },
                    function(response){
                        switch (response.status) {

                            case 401:alert('Credentials Incorrect');break;
                            case 422:{

                                alert('Sorry Error Happened');
                                $state.go('forums');

                            }break;

                        }
                    }
                );
            }

            $scope.sidebar_latestclassifieds=function(){

                $scope.sidebarlatestclassifieds=classifiedsService.getclassifieds('small',4,0,
                    function(response){
                        switch (response.status) {

                            case 401:alert('Credentials Incorrect');break;
                            case 200:{
                                $scope.sidebarlatestclassifieds=response.data.data;
                                angular.element(document).find("#latestclassifieds").removeClass( "spinner_sidebar" );
                                // alert('Great! Data Loaded');
                                console.log(response.data.data);
                            }break;

                        }
                    },
                    function(response){
                        switch (response.status) {

                            case 401:alert('Credentials Incorrect');break;
                            case 422:{

                                alert('Sorry Error Happened');
                                $state.go('forums');

                            }break;

                        }
                    }
                );
            }

            $scope.sidebar_latestproducts=function(){

                $scope.sidebarlatestproducts=storeService.getproducts('small',9,0,
                    function(response){
                        switch (response.status) {

                            case 401:alert('Credentials Incorrect');break;
                            case 200:{
                                $scope.sidebarlatestproducts=response.data.data;
                                angular.element(document).find("#latestproducts").removeClass( "spinner_sidebar" );
                                // alert('Great! Data Loaded');
                                // console.log(response.data.data);
                            }break;

                        }


                    },

                    function(response){
                        switch (response.status) {

                            case 401:alert('Credentials Incorrect');break;
                            case 422:{

                                alert('Sorry Error Happened');
                                $state.go('forums');

                            }break;

                        }
                    }
                );
            }

            $scope.getmyprofile=function(){

                $scope.myprofileinfo=accountServices.getmyprofile(
                    function(response) {
                        switch (response.status) {

                            case 401:alert('Credentials Incorrect');break;
                            case 200:{
                                $scope.myprofileinfo_error=0;
                                $scope.myprofileinfo=response.data.data;
                                  console.log(response.data.data);
                            }break;

                        }


                    },
                    function(response){
                    switch (response.status) {

                        case 401:alert('Credentials Incorrect');break;
                        case 422:{

                            alert('Sorry Error Happened');
                            $scope.myprofileinfo_error=1;

                        }break;

                    }
                }
                );




            }

            $scope.initmyprofilecontr=function(){
                $scope.getmyprofile();
              //  $scope.getarticles($scope.pageparm);



            };



        }]);

})();
