var fadBlogsServices = angular.module('fadBlogsServices', [
    'LocalStorageModule'
]);

fadBlogsServices.factory('blogsService', ['$http', 'localStorageService', function($http, localStorageService,$rootScope) {


    var get_blog_posts_link='/api/v1/professional/blog/posts';
    var get_blog_post_link='/api/v1/professional/blog/get';
    var bloglink='http://fad.dev/api/v1/auth/logout';
    var get_myblog_posts_link='/api/v1/professional/me/blogs'
    var get_myblog_post_link='/api/v1/professional/me/blogpost';
    var delete_post_link='/api/v1/professional/blog/post';
    var publish_post_link='/api/v1/professional/blog/post/publish';
    var post_save_blogpost_link='/api/v1/professional/blog/editpost';
    var add_save_blogpost_link='/api/v1/professional/blog/post';

    function addblogpost(blogpost, onSuccess,onError) {

        var fd = new FormData();
        fd.append('image_url', blogpost.image_url);
        fd.append('tags', blogpost.tags);
        fd.append('title', blogpost.title);
        fd.append('content', blogpost.content_text);
        fd.append('category_id', blogpost.category_id);

        $http.post(add_save_blogpost_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});






        return;


    }

    function saveblogpost(blogpost, onSuccess,onError) {



        var fd = new FormData();
        fd.append('image_url', blogpost.image_url);
        fd.append('id', blogpost.id);
        fd.append('title', blogpost.title);
        fd.append('content', blogpost.content_text);
        fd.append('category_id', blogpost.category_id);

        $http.post(post_save_blogpost_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},function error(response){  onError(response);});


    }

    function publishblogpost(postid,status,  onSuccess,onError) {

        $http({
            url: publish_post_link,
            method: "PUT",
            params: {
                id: postid,
                status: status,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function deleteblogpost(postid,  onSuccess,onError) {

        $http({
            url: delete_post_link,
            method: "DELETE",
            params: {
                id: postid,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getmyblog(postid,  onSuccess,onError) {

        $http({
            url: get_myblog_post_link,
            method: "GET",
            params: {
                id: postid
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getmyblogs(size, poststotal,page,  onSuccess,onError) {

        $http({
            url: get_myblog_posts_link,
            method: "GET",
            params: { size: size,
                per_page: poststotal,
                page:page
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getblogs(size, poststotal, onError) {

        $http.get(get_blog_post_link,
            {
                size: size,
                poststotal: poststotal
            }).
        then(function(response) {
              alert(response.status);
            switch (response.status) {

                case 401:alert('Credentials Incorrect');break;
                case 200:return (response.data.data);
            }

          //  return response.data;
           // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getblogs2(size, poststotal,page,  onSuccess,onError) {

        $http({
            url: get_blog_posts_link,
            method: "GET",
            params: { size: size,
                      per_page: poststotal,
                      page:page
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getlatestblogposts(per_page,page,blogpost_id,sortmode,onSuccess,onError){

        $http({
            url:get_blog_posts_link,
            method: "GET",
            params: {
                per_page: per_page,
                page:page,
                mode:sortmode,
                current_blog_id:blogpost_id
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });
    }

    function getblogspost(postid, onSuccess,onError) {
        $http({
            url:get_blog_post_link,
            method: "GET",
            params: {
                 id: postid,

            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });

    }


    return {
        getblogs2:getblogs2,
        getblogspost:getblogspost,
        getlatestblogposts:getlatestblogposts,
        getmyblogs:getmyblogs,
        deleteblogpost:deleteblogpost,
        publishblogpost:publishblogpost,
        getmyblog:getmyblog,
        saveblogpost:saveblogpost,
        addblogpost:addblogpost
    }

}]);
