var fadClassifiedsService = angular.module('fadClassifiedsService', [
    'LocalStorageModule'
]);

fadClassifiedsService.factory('classifiedsService', ['$http', 'localStorageService', function($http, localStorageService,$rootScope) {


    var get_classifieds_link='/api/v1/professional/classifieds';
    var get_classified_link='/api/v1/professional/classifieds/classified';
    var get_classified_link_link='/api/v1/professional/me/classifieds';
    var get_my_classified_link='/api/v1/professional/me/classified';
    var delete_classified_link='/api/v1/professional/classifieds/classified';
    var put_classified_link='/api/v1/professional/classifieds/editclassified';
    var post_classified_link='/api/v1/professional/classifieds/classified';
    var publish_classified_link='/api/v1/professional/classifieds/classified/publish';

    function addclassified(classified, onSuccess,onError) {


        var fd = new FormData();
        fd.append('image_url', classified.image_url);
        fd.append('title', classified.title);
        fd.append('content', classified.content_text);
        fd.append('tags', classified.tags);
        fd.append('category_id', classified.category_id);
        fd.append('end_date', classified.end_date);

        fd.append('hideowner', classified.hideowner);


        fd.append('all_can_see', classified.all_can_see);
        fd.append('attorney_can_see', classified.attorney_can_see);
        fd.append('chiropractor_can_see', classified.chiropractor_can_see);
        fd.append('dentist_can_see', classified.dentist_can_see);

        fd.append('optometrist_can_see', classified.optometrist_can_see);
        fd.append('osteopathist_can_see', classified.osteopathist_can_see);
        fd.append('phd_can_see', classified.phd_can_see);
        fd.append('pharmacist_can_see', classified.pharmacist_can_see);

        fd.append('physician_can_see', classified.physician_can_see);
        fd.append('podiatrist_can_see', classified.podiatrist_can_see);
        fd.append('poi_can_see', classified.poi_can_see);
        fd.append('veterinarian_can_see', classified.veterinarian_can_see);



        $http.post(post_classified_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});
    }

    function getmyclassified(classifiedid,  onSuccess,onError) {

        $http({
            url: get_my_classified_link,
            method: "GET",
            params: {
                id: classifiedid
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function publishclassified(classifiedid,status,  onSuccess,onError) {

        $http({
            url: publish_classified_link,
            method: "PUT",
            params: {
                id: classifiedid,
                status: status,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function deleteclassified(classifiedid,  onSuccess,onError) {

        $http({
            url: delete_classified_link,
            method: "DELETE",
            params: {
                id: classifiedid,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }


    function getmyclassifieds(size, poststotal,page,  onSuccess,onError) {

        $http({
            url: get_classified_link_link,
            method: "GET",
            params: {
                size: size,
                per_page: poststotal,
                page:page
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }


    function getclassifieds(size, poststotal,page,  onSuccess,onError) {

        $http({
            url: get_classifieds_link,
            method: "GET",
            params: {
                      size: size,
                      per_page: poststotal,
                      page:page
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getlatestclassifieds(size, poststotal,page, blogpost_id, onSuccess,onError) {

        $http({
            url: get_classifieds_link,
            method: "GET",
            params: {
                size: size,
                per_page: poststotal,
                page:page,
                mode:'latest',
                current_blog_id:blogpost_id
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });

    }


    function getclassified(classifiedid, onSuccess,onError) {

        $http({
            url: get_classified_link,
            method: "GET",
            params: {
                id: classifiedid

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });


    }

    function saveclassified(classified, onSuccess,onError) {


        var fd = new FormData();
        fd.append('id', classified.id);
        fd.append('image_url', classified.image_url);
        fd.append('title', classified.title);
        fd.append('content', classified.content_text);
        fd.append('tags', classified.tags);
        fd.append('category_id', classified.category_id);
        fd.append('end_date', classified.end_date);

        fd.append('hideowner', classified.hideowner);


        fd.append('all_can_see', classified.all_can_see);
        fd.append('attorney_can_see', classified.attorney_can_see);
        fd.append('chiropractor_can_see', classified.chiropractor_can_see);
        fd.append('dentist_can_see', classified.dentist_can_see);

        fd.append('optometrist_can_see', classified.optometrist_can_see);
        fd.append('osteopathist_can_see', classified.osteopathist_can_see);
        fd.append('phd_can_see', classified.phd_can_see);
        fd.append('pharmacist_can_see', classified.pharmacist_can_see);

        fd.append('physician_can_see', classified.physician_can_see);
        fd.append('podiatrist_can_see', classified.podiatrist_can_see);
        fd.append('poi_can_see', classified.poi_can_see);
        fd.append('veterinarian_can_see', classified.veterinarian_can_see);



        $http.post(put_classified_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});


    }


    return {
        getclassifieds:getclassifieds,
        getclassified:getclassified,
        getmyclassifieds:getmyclassifieds,
        publishclassified:publishclassified,
        deleteclassified:deleteclassified,
        getmyclassified:getmyclassified,
        saveclassified:saveclassified,
        addclassified:addclassified,
        getlatestclassifieds:getlatestclassifieds
    }

}]);
