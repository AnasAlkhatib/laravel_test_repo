var fadProfessionalsService = angular.module('fadProfessionalsService', [
    'LocalStorageModule'
]);

fadProfessionalsService.factory('professionalsService', ['$http', 'localStorageService', function($http, localStorageService,$rootScope) {


    var get_members_link='/api/v1/professional/members';
    var get_member_link='/api/v1/professional/members/profile';
    var search_member_link='/api/v1/professional/members/search';





    function searchprofessionals( per_page,page,searchterm,  onSuccess,onError) {

        $http({
            url: search_member_link,
            method: "GET",
            params: {
                per_page: per_page,
                page:page,
                sex:searchterm.sex,
                searchtext:searchterm.searchtext
            }
        }).
        then(function(response)
            {
                onSuccess(response);

            }, function(response)
            {
                onError(response);
            }
        );

    }


    function getprofessionals(size, poststotal,page,  onSuccess,onError) {

        $http({
            url: get_members_link,
            method: "GET",
            params: { size: size,
                      per_page: poststotal,
                      page:page
            }
        }).
        then(function(response)
        {
            onSuccess(response);

        }, function(response)
        {
            onError(response);
        }
        );

    }


    function getprofessional(profileid, onSuccess,onError)  {
        $http({
            url: get_member_link,
            method: "GET",
            params: {
                id: profileid
            }
        }).
        then(function(response)
        {
            onSuccess(response);

        }, function(response)
        {
            onError(response);

        });


    }




    return {
        getprofessionals:getprofessionals,
        getprofessional:getprofessional,
        searchprofessionals:searchprofessionals
    }

}]);
