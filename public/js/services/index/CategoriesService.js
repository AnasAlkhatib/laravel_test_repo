var fadCategoriesServices = angular.module('fadCategoriesServices', [
    'LocalStorageModule'
]);

fadCategoriesServices.factory('categoriesServices', ['$http', 'localStorageService', function($http, localStorageService,$rootScope) {


    var get_categories_link='/api/v1/professional/categories';


    function getcategories(section, per_page,page, onSuccess,onError)
    {
        $http({
            url: get_categories_link,
            method: "GET",
            params:
                {  section: section,
                   per_page: per_page,
                   page:page,
                }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });

    }

    return {
        getcategories:getcategories,
    }

}]);
