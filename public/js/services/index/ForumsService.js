var fadForumsServices = angular.module('fadForumsServices', [
    'LocalStorageModule'
]);

fadForumsServices.factory('forumsService', ['$http', 'localStorageService', function($http, localStorageService,$rootScope) {


    var get_myposts_link          ='/api/v1/professional/me/forumposts';
    var get_mypost_link          ='/api/v1/professional/me/forumpost';
    var get_mytopics_link          ='/api/v1/professional/me/forumtopics';
    var get_mytopic_link          ='/api/v1/professional/me/forumtopic';
    var main_forums_route         ='/api/v1/professional/forums';
    var get_forum_statistics_link ='/api/v1/professional/forums/statistics';
    var get_forums_link           ='/api/v1/professional/forums';
    var get_forum_topics_link     ='/api/v1/professional/forums/topics';
    var get_topic_posts_link      ='/api/v1/professional/forums/topic/posts';
    var get_latest_posts_link     ='/api/v1/professional/forums/posts/latest';

    var delete_forum_topic_link='/api/v1/professional/forums/topic';
    var publish_forum_topic_link='/api/v1/professional/forums/topic/publish';

    var delete_forum_post_link='/api/v1/professional/forums/post';
    var publish_forum_post_link='/api/v1/professional/forums/post/publish';

    var get_topic_post_link      ='/api/v1/professional/forums/topic/post';

    var save_topic_link      ='/api/v1/professional/forums/topic?';

    var save_post_link      ='/api/v1/professional/forum/post?';

    var add_topic_link      ='/api/v1/professional/forums/topic';

    var add_post_link      ='/api/v1/professional/forums/post';

    var get_forums_list_link          ='/api/v1/professional/me/forumslist';

    var get_forums_topics_list_link     ='/api/v1/professional/me/forumstopicslist';

    var get_topic_link      ='/api/v1/professional/forums/topic';

    var get_forum_info_link      ='/api/v1/professional/forums/forum/info';


    function getforuminfo(forum_id,onSuccess,onError) {
        $http({
            url: get_forum_info_link,
            method: "GET",
            params: {
                id:forum_id,
            }
        }).
        then(function(response) {
            onSuccess(response);
        }, function(response) {
            onError(response);
        });
    }

    function getforumtopic(topic_id,onSuccess,onError){

        $http({
            url:get_topic_link,
            method: "GET",
            params: {
                id:topic_id,
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });
    }

    function getmyforumtopicslists(onSuccess,onError) {
        $http({
            url: get_forums_topics_list_link,
            method: "GET",
            params: {
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });
    }

    function getmyforumlists(onSuccess,onError) {
        $http({
            url: get_forums_list_link,
            method: "GET",
            params: {
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });
    }


    function savepost(post, onSuccess,onError) {

        var data = $.param({
            'id': post.id,
            'post_title': post.title,
            'post_text': post.content,
            'topic_id': post.topic_id,
        });


        $http.put(save_post_link+data).then(
            function success(response){ onSuccess(response);},function error(response){  onError(response);});


    }

    function getmyforumpost(postid,onSuccess,onError) {
        $http({
            url: get_mypost_link,
            method: "GET",
            params: {
                id: postid,

            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });
    }


    function savetopic(topic, onSuccess,onError) {


        var data = $.param({
            'id': topic.id,
            'topic_title': topic.title,
            'topic_text': topic.content,
            'tags': topic.tags,
            'forum_id': topic.forum_id

        });


        $http.put(save_topic_link+data).then(
            function success(response){ onSuccess(response);},function error(response){  onError(response);});


    }

    function addtopic(topic, onSuccess,onError) {

        var fd = new FormData();
        fd.append('topic_title', topic.title);
        fd.append('topic_text',  topic.content);
        fd.append('tags', topic.tags);
        fd.append('forum_id', topic.forum_id);

        $http.post(add_topic_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});


    }

    function addforumpost(forumpost, onSuccess,onError) {

        var fd = new FormData();
        fd.append('post_title', forumpost.title);
        fd.append('post_text',  forumpost.content);
        fd.append('topic_id', forumpost.topic_id);

        $http.post(add_post_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});


    }


    function getmyforumtopic(topicid,onSuccess,onError) {
        $http({
            url: get_mytopic_link,
            method: "GET",
            params: {
                id: topicid,

            }
        }).
        then(function(response) {
            onSuccess(response);
        }, function(response) {
            onError(response);
        });
    }

    function publishforumtopic(topicid,status,  onSuccess,onError) {

        $http({
            url: publish_forum_topic_link,
            method: "PUT",
            params: {
                id: topicid,
                status: status,

            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });

    }

    function deleteforumtopic(topicid,  onSuccess,onError) {

        $http({
            url: delete_forum_topic_link,
            method: "DELETE",
            params: {
                id: topicid,

            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });

    }

    function publishforumpost(postid,status,  onSuccess,onError) {

        $http({
            url: publish_forum_post_link,
            method: "PUT",
            params: {
                id: postid,
                status: status,

            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });

    }

    function deleteforumpost(postid,  onSuccess,onError) {

        $http({
            url: delete_forum_post_link,
            method: "DELETE",
            params: {
                id: postid,

            }
        }).
        then(function(response) {
            onSuccess(response);
        }, function(response) {
            onError(response);
        });

    }

    function getmyforumposts(per_page,page,onSuccess,onError) {
        $http({
            url: get_myposts_link,
            method: "GET",
            params: {
                per_page: per_page,
                page:page
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });
    }

    function getmyforumtopics(per_page,page,onSuccess,onError)  {
        $http({
            url: get_mytopics_link,
            method: "GET",
            params: {
                per_page: per_page,
                page:page
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });
    }

    function getforumstatistics(onSuccess,onError) {
        $http({
            url: get_forum_statistics_link,
            method: "GET",

        }).
        then(function(response) {
            onSuccess(response);
        }, function(response) {
            onError(response);
        });
    }

    function getforums(per_page,page,onSuccess,onError) {
        $http({
            url: get_forums_link,
            method: "GET",
            params: {
                per_page: per_page,
                page:page
            }
        }).
        then(function(response) {


            onSuccess(response);
             //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });
    }

    function get_forum_pagination_url(url,onSuccess,onError) {
        $http({
            url: url,
            method: "GET",
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });
    }

    function getforumpost(post_id,onSuccess,onError){


        $http({
            url:get_topic_post_link,
            method: "GET",
            params: {
                id:post_id,

            }
        }).
        then(function(response) {

            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });
    }

    function getforumtopics(forum_id,per_page,page,onSuccess,onError){


        $http({
            url:get_forum_topics_link,
            method: "GET",
            params: {
                id:forum_id,
                per_page:per_page,
                page:page
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });
    }

    function gettopicposts(topic_id,per_page,page,onSuccess,onError){


        $http({
            url:get_topic_posts_link,
            method: "GET",
            params: {
                per_page: per_page,
                page:page,
                id:topic_id
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });
    }

    function getlatestposts(per_page,page,onSuccess,onError){

        $http({
            url:get_latest_posts_link,
            method: "GET",
            params: {
                per_page: per_page,
                page:page,
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });
    }

    function posttopicpost(per_page,page,onSuccess,onError){

        $http({
            url:get_latest_posts_link,
            method: "GET",
            params: {
                per_page: per_page,
                page:page,
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });
    }

    function posttopic(per_page,page,onSuccess,onError){

        $http({
            url:get_latest_posts_link,
            method: "GET",
            params: {
                per_page: per_page,
                page:page,
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });
    }


    return {
        getforums:getforums,
        get_forum_pagination_url:get_forum_pagination_url,
        getforumtopics:getforumtopics,
        gettopicposts:gettopicposts,
        getlatestposts:getlatestposts,
        posttopicpost:posttopicpost,
        posttopic:posttopic,
        getforumstatistics:getforumstatistics,
        getmyforumposts:getmyforumposts,
        publishforumtopic:publishforumtopic,
        deleteforumtopic:deleteforumtopic,
        getforumpost:getforumpost,
        publishforumpost:publishforumpost,
        deleteforumpost:deleteforumpost,
        getmyforumtopics:getmyforumtopics,
        getmyforumtopic:getmyforumtopic,
        savetopic:savetopic,
        getmyforumpost:getmyforumpost,
        savepost:savepost,
        addtopic:addtopic,
        getmyforumlists:getmyforumlists,
        getmyforumtopicslists:getmyforumtopicslists,
        addforumpost:addforumpost,
        getforumtopic:getforumtopic,
        getforuminfo:getforuminfo,

}

}]);