var fadInboxServices = angular.module('fadInboxServices', [
    'LocalStorageModule'
]);

fadInboxServices.factory('inboxService', ['$http', 'localStorageService', function($http, localStorageService,$rootScope) {


    var get_my_threads_link='/api/v1/professional/messages/mythreads';
    var get_thread_message_link='/api/v1/professional/messages/thread';
    var get_thread_info_link='/api/v1/professional/messages/threadinfo';
    var get_message_link='/api/v1/professional/messages/';
    var post_message_link='/api/v1/professional/messages/message';
    var post_thread_link='/api/v1/professional/messages/thread';
    var check_thread_between_link='/api/v1/professional/messages/threadbetween';


    function check_thread_between(recipient_id,onSuccess,onError)    {
        $http({
            url: check_thread_between_link,
            method: "GET",
            params: {
                id:recipient_id,
            }
        }).
        then(function(response)
        {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });

    }

    function post_thread( message,recipient_id,  onSuccess,onError) {

        $http({
            url: post_thread_link,
            method: "POST",
            params: {
                subject: 'new message',
                message: message,
                recipient_id:recipient_id
            }
        }).
        then(function(response) {

            onSuccess(response);


        }, function(response) {

            onError(response);

        });

    }

    function post_message(thread_id, message,recipient_id,  onSuccess,onError) {

        $http({
            url: post_message_link,
            method: "POST",
            params: {
                thread_id:thread_id,
                message: message,
                recipient_id:recipient_id
            }
        }).
        then(function(response) {

            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function get_message(message_id, poststotal,page,  onSuccess,onError) {

        $http({
            url: get_message_link,
            method: "GET",
            params: {
                id:message_id,
                per_page: poststotal,
                page:page
            }
        }).
        then(function(response) {

            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function get_thread_info(thread_id,onSuccess,onError)    {

        $http({
            url: get_thread_info_link,
            method: "GET",
            params: {
                thread_id:thread_id,
            }
        }).
        then(function(response)
        {
            onSuccess(response);

        }, function(response)
        {
            onError(response);
        });

    }

    function get_thread_message(thread_id, poststotal,page,  onSuccess,onError)    {
        $http({
            url: get_thread_message_link,
            method: "GET",
            params: {
                thread_id:thread_id,
                per_page: poststotal,
                page:page
            }
        }).
        then(function(response)
        {
            onSuccess(response);

        }, function(response)
        {
            onError(response);
        });

    }

    function getmythreads( poststotal,page,  onSuccess,onError)    {

        $http({
            url: get_my_threads_link,
            method: "GET",
            params: {
                per_page: poststotal,
                page:page
            }
        }).
        then(function(response)
        {
            onSuccess(response);

        }, function(response)
        {
            onError(response);
        });

    }



    return {
        get_message:get_message,
        getmythreads:getmythreads,
        get_thread_message:get_thread_message,
        post_message:post_message,
        get_thread_info:get_thread_info,
        check_thread_between:check_thread_between,
        post_thread:post_thread,
    }

}]);
