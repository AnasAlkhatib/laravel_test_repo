var fadQuestionsService = angular.module('fadQuestionsService', [
    'LocalStorageModule'
]);

fadQuestionsService.factory('questionsService', ['$http', 'localStorageService', function($http, localStorageService) {


    var get_questions_link='/api/v1/professional/questions';
    var get_question_link='/api/v1/professional/question';


    function getquestions(size, per_page,page,  onSuccess,onError) {

        $http({
            url: get_questions_link,
            method: "GET",
            params: {
                      size: size,
                      per_page: per_page,
                      page:page
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });
    }

    function getquestion(questionid, onSuccess,onError) {

        $http({
            url: get_question_link,
            method: "GET",
            params: {
                id: questionid
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });
    }


    return {
        getquestions:getquestions,
        getquestion:getquestion,

    }

}]);
