var fadAccountServices = angular.module('fadAccountServices', [
    'LocalStorageModule'
]);

fadAccountServices.factory('accountServices', ['$http', 'localStorageService', function($http, localStorageService,$rootScope) {


    var get_me_main_link='/api/v1/professional/me';
    var put_basic_info_link='/api/v1/professional/me/settings/basic';
    var get_my_profile='/api/v1/professional/me/myprofile';

    var get_my_community='/api/v1/professional/me/mycommunities';


    var upload_proof_document ='/api/v1/professional/me/proofdocument';
    var get_my_proof_documents='/api/v1/professional/me/proofdocuments';
    var delete_proof_document ='/api/v1/professional/me/proofdocument';

    var register_professional='/api/v1/register';


    function registeruser(user, onSuccess,onError) {

        var fd = new FormData();
        fd.append('name', user.name);
        fd.append('email', user.email);
        fd.append('password', user.password);
        fd.append('speciality', user.speciality);
        fd.append('aboutme', user.aboutme);
        fd.append('description', user.description);
        fd.append('city', user.city);
        fd.append('country', user.country);

        $http.post(register_professional, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});

    }

    function deletedocument(documentid,  onSuccess,onError) {

        $http({
            url: delete_proof_document,
            method: "DELETE",
            params: {
                id: documentid,
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });

    }

    function getmyproofdocument( onSuccess,onError) {

        $http({
            url: get_my_proof_documents,
            method: "GET",
            params: { }
        }).
        then(function(response) {
            onSuccess(response);
        }, function(response) {
            onError(response);
        });

    }

    function uploadproofdocument(document, onSuccess,onError) {

        var fd = new FormData();
        fd.append('image_url', document.image_url);
        fd.append('title', document.title);

        $http.post(upload_proof_document, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});
        return;
    }

    function getmycommunity(section, onSuccess,onError) {

        $http({
            url: get_my_community,
            method: "GET",
            params: {
                section: section,

            }
        }).
        then(function(response) {
            onSuccess(response);
        }, function(response) {
            onError(response);
        });

    }

    function getmyprofile(onSuccess,onError) {


        $http({
            url: get_my_profile,
            method: "GET",
            params: {

            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {

            onError(response);

        });



    }

    function getme(onSuccess,onError) {

        $http({
            url: get_me_main_link,
            method: "GET",
            params: {

            }
        }).
        then(function(response) {

            //JSON.stringify(response.data.data)
            localStorage.setItem('faduser',JSON.stringify(response.data.data));
            //onSuccess(response);
            //  return response.data;
            //  onSuccess(response);

        }, function(response) {

            localStorage.removeItem('faduser');
           // onError(response);

        });

    }

    function getbasicinfo(articleid,status,  onSuccess,onError) {

        $http({
            url: publish_article_link,
            method: "PUT",
            params: {
                id: articleid,
                status: status,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function savebasicinfo(me, onSuccess,onError) {

        $http({
            url: put_basic_info_link,
            method: "PUT",
            params: {
                   name:me.name,
                   //locale:me.locale,
                   currentposition:me.currentposition,
                   description:me.description,
                   country:me.country,
                   state:me.state,
                   address:me.address,
                   aboutme:me.aboutme
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getprivacy(articleid,status,  onSuccess,onError) {

        $http({
            url: publish_article_link,
            method: "PUT",
            params: {
                id: articleid,
                status: status,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function saveprivacy(articleid,status,  onSuccess,onError) {

        $http({
            url: publish_article_link,
            method: "PUT",
            params: {
                id: articleid,
                status: status,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getbasicinfo1(articleid,status,  onSuccess,onError) {

        $http({
            url: publish_article_link,
            method: "PUT",
            params: {
                id: articleid,
                status: status,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function savebasicinfo1(articleid,status,  onSuccess,onError) {

        $http({
            url: publish_article_link,
            method: "PUT",
            params: {
                id: articleid,
                status: status,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }


    return {
        getme:getme,
        getbasicinfo:getbasicinfo,
        savebasicinfo:savebasicinfo,
        getprivacy:getprivacy,
        saveprivacy:saveprivacy,
        getmyprofile:getmyprofile,
        getmycommunity:getmycommunity,
        getmyproofdocument:getmyproofdocument,
        uploadproofdocument:uploadproofdocument,
        deletedocument:deletedocument,
        registeruser:registeruser

    }

}]);
