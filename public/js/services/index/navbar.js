angular.module('fadApp')
  .controller('NavbarCtrl', function($scope, $auth,$state, toastr,$rootScope)
  {
     $rootScope.isAuthenticated = function()
     {
              if($auth.isAuthenticated() && typeof $scope.userrole != 'undefined'  && $scope.userrole=='professional')
              {
                  return $auth.isAuthenticated();
              }
          else
              {
                  return  false;
              }
    };

    $rootScope.setname = function()
    {
          var user=localStorage.getitem('faduser');
    };

    $scope.logout= function()
    {
          if (!$auth.isAuthenticated())
          {
              return;
          }
          $auth.logout()
              .then(function ()
              {
                  var myEl = angular.element( document.querySelector( '#logintab' ) );
                  myEl.removeClass('cd-user-modal');
                  localStorage.removeItem('faduser');
                  toastr.info('You have been logged out');
                  $state.go('/', null, {reload: true});
              });
     }


  });

