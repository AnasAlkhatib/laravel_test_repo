var fadGroupsService = angular.module('fadGroupsService', [
    'LocalStorageModule'
]);

fadGroupsService.factory('groupsService', ['$http', 'localStorageService', function($http, localStorageService,$rootScope) {


    var get_groups_link='/api/v1/professional/groups';
    var get_group_link='/api/v1/professional/groups/group';
    var get_mygroups_link='/api/v1/professional/me/groups';
    var get_mygroup_link='/api/v1/professional/me/group';
    var edit_group_link='/api/v1/professional/groups/editgroup';
    var get_group_members_link='/api/v1/professional/group/members';
    var delete_group_link='/api/v1/professional/groups/group';
    var post_group_link='/api/v1/professional/groups/group';
    var publish_group_link='/api/v1/professional/groups/group/publish';

    var post_join_group_link='/api/v1/professional/group/join';
    var post_removeoin_group_link='/api/v1/professional/group/leave';

    var post_manage_group_link='/api/v1/professional/group/manage/addremoveuser';

    var get_group_content_link   ='/api/v1/professional/group/content';
    var post_group_content_link  ='/api/v1/professional/group/content/addcontent';
    var post_group_photo_link    ='/api/v1/professional/group/content/addphoto';
    var post_group_video_link    ='/api/v1/professional/group/content/addvideo';
    var delete_group_content_link  ='/api/v1/professional/group/content';


    function deletegroupcontent(contentid,  onSuccess,onError) {

        $http({
            url: delete_group_content_link,
            method: "DELETE",
            params: {
                id: contentid,
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function addgroupcontent(group, onSuccess,onError) {

        var fd = new FormData();
        fd.append('id', group.id);
        fd.append('image_url', group.image_url);
        fd.append('title', group.title);
        fd.append('content', group.content);

        $http.post(post_group_content_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});
    }

    function addgroupphoto(group, onSuccess,onError) {

        var fd = new FormData();
        fd.append('id', group.id);
        fd.append('image_url', group.image_url);
        fd.append('title', group.title);

        $http.post(post_group_photo_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});
    }

    function addgroupvideo(group, onSuccess,onError) {

        var fd = new FormData();
        fd.append('video_url', group.video_url);
        fd.append('id', group.id);
        fd.append('title', group.title);

        $http.post(post_group_video_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});
    }


    function getgroupcontent(groupid,poststotal,page,  onSuccess,onError) {

        $http({
            url: get_group_content_link,
            method: "GET",
            params: {
                id: groupid,
                per_page: poststotal,
                page:page
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }


    function approvemember(groupid,user_id, onSuccess,onError) {

        var fd = new FormData();
        fd.append('id', groupid);
        fd.append('user_id', user_id);
        fd.append('action', 'accept');

        $http.post(post_manage_group_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});
    }

    function removemember(groupid,user_id, onSuccess,onError) {

        var fd = new FormData();
        fd.append('id', groupid);
        fd.append('user_id', user_id);
        fd.append('action', 'remove');

        $http.post(post_manage_group_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});
    }

    function postremoverequest(id, onSuccess,onError) {

        var fd = new FormData();
        fd.append('id', id);

        $http.post(post_removeoin_group_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});
    }

    function postjoinrequest(id, onSuccess,onError) {

        var fd = new FormData();
        fd.append('id', id);

        $http.post(post_join_group_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});
    }

    function addgroup(group, onSuccess,onError) {

        var fd = new FormData();
        fd.append('image_url', group.image_url);
        //fd.append('id', group.id);
        fd.append('title', group.title);
        fd.append('content', group.description);
        fd.append('tags', group.tags);

        fd.append('all_can_see', group.all_can_see);
        fd.append('attorney_can_see', group.attorney_can_see);
        fd.append('chiropractor_can_see', group.chiropractor_can_see);
        fd.append('dentist_can_see', group.dentist_can_see);

        fd.append('optometrist_can_see', group.optometrist_can_see);
        fd.append('osteopathist_can_see', group.osteopathist_can_see);
        fd.append('phd_can_see', group.phd_can_see);
        fd.append('pharmacist_can_see', group.pharmacist_can_see);

        fd.append('physician_can_see', group.physician_can_see);
        fd.append('podiatrist_can_see', group.podiatrist_can_see);
        fd.append('poi_can_see', group.poi_can_see);
        fd.append('veterinarian_can_see', group.veterinarian_can_see);


        $http.post(post_group_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});
    }

    function savegroup(group, onSuccess,onError) {

        var fd = new FormData();
        fd.append('image_url', group.image_url);
        fd.append('id', group.id);
        fd.append('title', group.title);
        fd.append('description', group.description);
        fd.append('tags', group.tags);

        fd.append('all_can_see', group.all_can_see);
        fd.append('attorney_can_see', group.attorney_can_see);
        fd.append('chiropractor_can_see', group.chiropractor_can_see);
        fd.append('dentist_can_see', group.dentist_can_see);

        fd.append('optometrist_can_see', group.optometrist_can_see);
        fd.append('osteopathist_can_see', group.osteopathist_can_see);
        fd.append('phd_can_see', group.phd_can_see);
        fd.append('pharmacist_can_see', group.pharmacist_can_see);

        fd.append('physician_can_see', group.physician_can_see);
        fd.append('podiatrist_can_see', group.podiatrist_can_see);
        fd.append('poi_can_see', group.poi_can_see);
        fd.append('veterinarian_can_see', group.veterinarian_can_see);



        $http.post(edit_group_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});




    }

    function publishgroup(groupid,status,  onSuccess,onError) {

        $http({
            url: publish_group_link,
            method: "PUT",
            params: {
                id: groupid,
                status: status,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function deletegroup(groupid,  onSuccess,onError) {

        $http({
            url: delete_group_link,
            method: "DELETE",
            params: {
                id: groupid,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getmygroup(groupid,  onSuccess,onError) {

        $http({
            url: get_mygroup_link,
            method: "GET",
            params: {
                id: groupid,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getmygroups(size, poststotal,page,  onSuccess,onError) {

        $http({
            url: get_mygroups_link,
            method: "GET",
            params: { size: size,
                per_page: poststotal,
                page:page
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getgroups(size, poststotal,page,  onSuccess,onError) {

        $http({
            url: get_groups_link,
            method: "GET",
            params: { size: size,
                      per_page: poststotal,
                      page:page
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getgroup(groupid, onSuccess,onError) {


        $http({
            url: get_group_link,
            method: "GET",
            params: {
                id:groupid,
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });
    }

    function getgroupmembers(groupid,per_page,page, onSuccess,onError) {


        $http({
            url: get_group_members_link,
            method: "GET",
            params: {
                id:groupid,
                per_page: per_page,
                page:page
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });

    }



    return {
        getgroups:getgroups,
        getgroup:getgroup,
        getmygroups:getmygroups,
        getgroupmembers:getgroupmembers,
        publishgroup:publishgroup,
        deletegroup:deletegroup,
        getmygroup:getmygroup,
        savegroup:savegroup,
        addgroup:addgroup,
        postjoinrequest:postjoinrequest,
        postremoverequest:postremoverequest,
        approvemember:approvemember,
        removemember:removemember,
        getgroupcontent:getgroupcontent,
        addgroupcontent:addgroupcontent,
        addgroupphoto:addgroupphoto,
        addgroupvideo:addgroupvideo,
        deletegroupcontent:deletegroupcontent

    }

}]);
