var fadEventsService = angular.module('fadEventsService', [
    'LocalStorageModule'
]);

fadEventsService.factory('eventsService', ['$http', 'localStorageService', function($http, localStorageService,$rootScope) {


    var get_events_link='/api/v1/professional/events';
    var get_event_link='/api/v1/professional/events/event';
    var get_myevents_link='/api/v1/professional/me/events';
    var get_myevent_link='/api/v1/professional/me/event';
    var edit_event_link='/api/v1/professional/events/editevent';
    var delete_event_link='/api/v1/professional/events/event';
    var post_event_link='/api/v1/professional/events/event';
    var publish_event_link='/api/v1/professional/events/event/publish';

    function addevent(event, onSuccess,onError) {

        var fd = new FormData();
        fd.append('image_url', event.image_url);
        fd.append('event_date', event.event_date);
        fd.append('title', event.title);
        fd.append('content', event.content_text);
        fd.append('category_id', event.category_id);
        fd.append('tags', event.tags);


        fd.append('all_can_see', event.all_can_see);
        fd.append('attorney_can_see', event.attorney_can_see);
        fd.append('chiropractor_can_see', event.chiropractor_can_see);
        fd.append('dentist_can_see', event.dentist_can_see);

        fd.append('optometrist_can_see', event.optometrist_can_see);
        fd.append('osteopathist_can_see', event.osteopathist_can_see);
        fd.append('phd_can_see', event.phd_can_see);
        fd.append('pharmacist_can_see', event.pharmacist_can_see);

        fd.append('physician_can_see', event.physician_can_see);
        fd.append('podiatrist_can_see', event.podiatrist_can_see);
        fd.append('poi_can_see', event.poi_can_see);
        fd.append('veterinarian_can_see', event.veterinarian_can_see);

        fd.append('event_end_date', event.event_end_date);
        fd.append('event_type', event.event_type);
        fd.append('discipline', event.discipline);
        fd.append('subdiscipline', event.subdiscipline);

        fd.append('organization', event.organization);
        fd.append('venue', event.venue);
        fd.append('fulladdress', event.fulladdress);

        fd.append('lot', event.lot);
        fd.append('lan', event.lan);


            $http.post(post_event_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});


        return;


    }

    function saveevent(event, onSuccess,onError) {

        var fd = new FormData();
        fd.append('image_url', event.image_url);
        fd.append('id', event.id);
        fd.append('title', event.title);
        fd.append('content', event.content_text);
        fd.append('category_id', event.category_id);
        fd.append('tags', event.tags);

        fd.append('all_can_see', event.all_can_see);
        fd.append('attorney_can_see', event.attorney_can_see);
        fd.append('chiropractor_can_see', event.chiropractor_can_see);
        fd.append('dentist_can_see', event.dentist_can_see);

        fd.append('optometrist_can_see', event.optometrist_can_see);
        fd.append('osteopathist_can_see', event.osteopathist_can_see);
        fd.append('phd_can_see', event.phd_can_see);
        fd.append('pharmacist_can_see', event.pharmacist_can_see);

        fd.append('physician_can_see', event.physician_can_see);
        fd.append('podiatrist_can_see', event.podiatrist_can_see);
        fd.append('poi_can_see', event.poi_can_see);
        fd.append('veterinarian_can_see', event.veterinarian_can_see);

        $http.post(edit_event_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});


        return;


    }

    function getmyevent(eventid,  onSuccess,onError) {

        $http({
            url: get_myevent_link,
            method: "GET",
            params: {
                id: eventid,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function publishevent(eventid,status,  onSuccess,onError) {

        $http({
            url: publish_event_link,
            method: "PUT",
            params: {
                id: eventid,
                status: status,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function deleteevent(eventid,  onSuccess,onError) {

        $http({
            url: delete_event_link,
            method: "DELETE",
            params: {
                id: eventid,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }


    function getmyevents(size, poststotal,page,  onSuccess,onError) {

        $http({
            url: get_myevents_link,
            method: "GET",
            params: { size: size,
                per_page: poststotal,
                page:page
            }
        }).
        /*$http.get(get_blog_posts_link,
         {
         size: size,
         per_page: poststotal,
         page:page
         }).*/
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getevents(size, poststotal,page,  onSuccess,onError) {

        $http({
            url: get_events_link,
            method: "GET",
            params: { size: size,
                      per_page: poststotal,
                      page:page
            }
        }).
        /*$http.get(get_blog_posts_link,
            {
                size: size,
                per_page: poststotal,
                page:page
            }).*/
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }


    function getevent(eventid, onSuccess,onError) {


        $http({
            url: get_event_link,
            method: "GET",
            params: {
                id: eventid,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });


    }




    return {
        getevents:getevents,
        getevent:getevent,
        getmyevents:getmyevents,
        publishevent:publishevent,
        deleteevent:deleteevent,
        getmyevent:getmyevent,
        saveevent:saveevent,
        addevent:addevent
    }

}]);
