var fadArticlesServices = angular.module('fadArticlesServices', [
    'LocalStorageModule'
]);

fadArticlesServices.factory('articlesService', ['$http', 'localStorageService', function($http, localStorageService,$rootScope) {


    var get_articles_link='/api/v1/professional/articles';
    var get_article_link='/api/v1/professional/articles/article';
    var get_myarticles_link='/api/v1/professional/me/articles';
    var get_myarticle_link='/api/v1/professional/me/article';
    var delete_article_link='/api/v1/professional/articles/article';
    var publish_article_link='/api/v1/professional/articles/article/publish';
    var edit_article_link='/api/v1/professional/articles/editarticle';
    var add_article_link='/api/v1/professional/articles/article/post';


    function publisharticle(articleid,status,  onSuccess,onError) {

        $http({
            url: publish_article_link,
            method: "PUT",
            params: {
                id: articleid,
                status: status,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function addarticle(article, onSuccess,onError) {

        var fd = new FormData();
        fd.append('image_url', article.image_url);
        fd.append('title', article.title);
        fd.append('tags', article.tags);
        fd.append('content', article.content);
        fd.append('category_id', article.category_id);

        $http.post(add_article_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});
        return;

    }

    function savearticle(article, onSuccess,onError) {

        var fd = new FormData();
        fd.append('image_url', article.image_url);//article.image_url
        fd.append('id', article.id);
        fd.append('title', article.title);
        fd.append('content', article.content);
        fd.append('tags', article.tags);
        fd.append('category_id', article.category_id);

        $http.post(edit_article_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
           function success(response){ onSuccess(response);},
           function error(response){  onError(response);});






        return;

    }

    function deletearticle(articleid,  onSuccess,onError) {

        $http({
            url: delete_article_link,
            method: "DELETE",
            params: {
                id: articleid,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getmyarticle(articleid,  onSuccess,onError) {

        $http({
            url: get_myarticle_link,
            method: "GET",
            params: {
               id:articleid
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getmyarticles(size, poststotal,page,  onSuccess,onError) {

        $http({
            url: get_myarticles_link,
            method: "GET",
            params: {
                size: size,
                per_page: poststotal,
                page:page
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }


    function getarticles22(size, poststotal, onError) {

        $http.get(get_blog_post_link,
            {
                size: size,
                poststotal: poststotal
            }).
        then(function(response) {
              alert(response.status);
            switch (response.status) {

                case 401:alert('Credentials Incorrect');break;
                case 200:return (response.data.data);
            }

          //  return response.data;
           // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getarticles(size, poststotal,page, onSuccess,onError) {

        $http({
            url: get_articles_link,
            method: "GET",
            params:
                {  size: size,
                   per_page: poststotal,
                   page:page,
                }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });

    }

    function getlatestarticles(size, poststotal,page,article_id, onSuccess,onError)
    {
        $http({
            url: get_articles_link,
            method: "GET",
            params: { size: size,
                per_page: poststotal,
                page:page,
                mode:'latest',
                current_article_id:article_id
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });

    }


    function getarticle(articleid, onSuccess,onError) {

        $http({
            url: get_article_link,
            method: "GET",
            params: {
                id: articleid

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });



    }




    return {
        getarticles:getarticles,
        getarticle:getarticle,
        getlatestarticles:getlatestarticles,
        getmyarticles:getmyarticles,
        deletearticle:deletearticle,
        publisharticle:publisharticle,
        getmyarticle:getmyarticle,
        savearticle:savearticle,
        addarticle:addarticle
    }

}]);
