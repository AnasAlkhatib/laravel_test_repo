var fadMyprofileServices = angular.module('fadMyprofileServices', [
    'LocalStorageModule'
]);

fadMyprofileServices.factory('myprofileServices', ['$http', 'localStorageService', function($http, localStorageService,$rootScope) {


    var get_blog_posts_link='/api/v1/professional/blog/posts';
    var get_blog_post_link='/api/v1/professional/blog/get';
    var bloglink='http://fad.dev/api/v1/auth/logout';
    var get_myblog_post_link='/api/v1/professional/me/blogpost';

    var publish_post_link='/api/v1/professional/blog/post/publish';
    var post_save_blogpost_link='/api/v1/professional/blog/editpost';
    var add_save_blogpost_link='/api/v1/professional/blog/post';


    var get_myeducations_link='/api/v1/professional/me/educations';
    var delete_education_link='/api/v1/professional/me/education';
    var post_education_link='/api/v1/professional/me/education';

    var get_myexperiences_link='/api/v1/professional/me/experiences';
    var delete_experience_link='/api/v1/professional/me/experience';
    var post_experience_link='/api/v1/professional/me/experience';

    var get_mycourses_link='/api/v1/professional/me/courses';
    var delete_course_link='/api/v1/professional/me/course';
    var post_course_link='/api/v1/professional/me/course';

    var get_myprofileimage_link='/api/v1/professional/me/settings/picture';
    var put_myprofileimage_link='/api/v1/professional/me/settings/picture';

    function updateavatar(avatar, onSuccess,onError)   {
        var fd = new FormData();
        fd.append('picture_url', avatar.picture_url);


        $http.post(put_myprofileimage_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},function error(response){  onError(response);});


    }


    function getmyprofileimage(onSuccess,onError) {

        $http({
            url: get_myprofileimage_link,
            method: "GET",
            params: { }
        }).
        then(function(response)
        {
            onSuccess(response);

        }, function(response)
        {
            onError(response);
        });

    }


    function getmyeducations(onSuccess,onError) {

        $http({
            url: get_myeducations_link,
            method: "GET",
            params: { }
        }).
        then(function(response)
        {
            onSuccess(response);

        }, function(response)
        {
            onError(response);
        });

    }

    function deleteeducation(educationid,  onSuccess,onError) {

        $http({
            url: delete_education_link,
            method: "DELETE",
            params: {
                id: educationid,
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });

    }

    function saveeducation(education, onSuccess,onError)   {
        var fd = new FormData();
        fd.append('speciality', education.speciality);
        fd.append('education_level', education.education_level);
        fd.append('institute_name', education.institute_name);
        fd.append('graduation_date', education.graduation_date);


        $http.post(post_education_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},function error(response){  onError(response);});


    }


    function getmyexperiences(onSuccess,onError)   {

        $http({
            url: get_myexperiences_link,
            method: "GET",
            params: { }
        }).
        then(function(response)
        {
            onSuccess(response);

        }, function(response)
        {
            onError(response);
        });

    }

    function deleteexperience(experienceid,  onSuccess,onError) {

        $http({
            url: delete_experience_link,
            method: "DELETE",
            params: {
                id: experienceid,
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });

    }


    function saveexperience(experience, onSuccess,onError)   {
        var fd = new FormData();
        fd.append('title', experience.title);
        fd.append('sumary', experience.sumary);
        fd.append('from_date', experience.from_date);
        fd.append('to_date', experience.to_date);


        $http.post(post_experience_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},function error(response){  onError(response);});


    }


    function getmycourses(onSuccess,onError)  {

        $http({
            url: get_mycourses_link,
            method: "GET",
            params: { }
        }).
        then(function(response)
        {
            onSuccess(response);

        }, function(response)
        {
            onError(response);
        });

    }

    function deletecourse(courseid,  onSuccess,onError) {

        $http({
            url: delete_course_link,
            method: "DELETE",
            params: {
                id: courseid,
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });

    }


    function savecourse(course, onSuccess,onError)    {
        var fd = new FormData();
        fd.append('title', course.title);
        fd.append('sumary', course.sumary);


        $http.post(post_course_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},function error(response){  onError(response);});


    }



    return {
        getmyeducations:getmyeducations,
        deleteeducation:deleteeducation,
        saveeducation:saveeducation,
        getmyexperiences:getmyexperiences,
        deleteexperience:deleteexperience,
        saveexperience:saveexperience,
        getmycourses:getmycourses,
        deletecourse:deletecourse,
        savecourse:savecourse,
        getmyprofileimage:getmyprofileimage,
        updateavatar:updateavatar

    }

}]);
