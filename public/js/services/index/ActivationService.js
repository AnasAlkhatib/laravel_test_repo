var fadActivationServices = angular.module('fadActivationServices', [
    'LocalStorageModule'
]);

fadActivationServices.factory('activationServices', ['$http', 'localStorageService', function($http, localStorageService,$rootScope) {


    var activation_link='/api/v1/activatecode';

    var reset_password_email_link='/api/v1/professional/sendresetpasswordlink';

    var reset_password_link='/api/v1/professional/resetpassword';

    var resend_activation_link='/api/v1/activation/regenerate';


    function resendactivationlink(email,onSuccess,onError) {

        $http({
            url: resend_activation_link,
            method: "GET",
            params: {
                email:email,
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });

    }

    function activateaccount(email,activationcode,onSuccess,onError) {

        $http({
            url: activation_link,
            method: "GET",
            params: {
                email:email,
                activationcode:activationcode

            }
        }).
        then(function(response) {
              onSuccess(response);

        }, function(response) {

           onError(response);
        });

    }

    function resetpassword(email,onSuccess,onError) {

        $http({
            url: reset_password_email_link,
            method: "GET",
            params: {
                email:email,
                    }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });

    }

    function resetpasswordaccount(reset_password,onSuccess,onError) {

        $http({
            url: reset_password_link,
            method: "GET",
            params: {
                email:reset_password.email,
                password:reset_password.password,
                password_confirmation:reset_password.password,
                token:reset_password.token,
            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {
            onError(response);
        });

    }



    return {
        activateaccount:activateaccount,
        resetpassword:resetpassword,
        resetpasswordaccount:resetpasswordaccount,
        resendactivationlink:resendactivationlink

    }

}]);
