var fadStoreService = angular.module('fadStoreService', [
    'LocalStorageModule'
]);

fadStoreService.factory('storeService', ['$http', 'localStorageService', function($http, localStorageService,$rootScope) {


    var get_products_link='/api/v1/professional/store';
    var get_product_link='/api/v1/professional/store/product';
    var get_myproducts_link='/api/v1/professional/me/products';
    var get_myproduct_link='/api/v1/professional/me/product';
    var post_product_edit_link ='/api/v1/professional/store/editproduct';
    var delete_product_link='/api/v1/professional/store/product';
    var add_product_link='/api/v1/professional/store/product';
    var publish_product_link='/api/v1/professional/store/product/publish';

    function addproduct(product, onSuccess,onError)
    {

        var fd = new FormData();
        fd.append('image_url', product.image_url);
        //fd.append('id', product.id);
        fd.append('name', product.title);
        fd.append('description', product.content_text);
        fd.append('category_id', product.category_id);
        fd.append('tags', product.tags);

        fd.append('all_can_see', product.all_can_see);
        fd.append('attorney_can_see', product.attorney_can_see);
        fd.append('chiropractor_can_see', product.chiropractor_can_see);
        fd.append('dentist_can_see', product.dentist_can_see);

        fd.append('optometrist_can_see', product.optometrist_can_see);
        fd.append('osteopathist_can_see', product.osteopathist_can_see);
        fd.append('phd_can_see', product.phd_can_see);
        fd.append('pharmacist_can_see', product.pharmacist_can_see);

        fd.append('physician_can_see', product.physician_can_see);
        fd.append('podiatrist_can_see', product.podiatrist_can_see);
        fd.append('poi_can_see', product.poi_can_see);
        fd.append('veterinarian_can_see', product.veterinarian_can_see);

        $http.post(add_product_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});

    }

    function saveproduct(product, onSuccess,onError)
    {

        var fd = new FormData();
        fd.append('image_url', product.image_url);//article.image_url
        fd.append('id', product.id);
        fd.append('name', product.title);
        fd.append('description', product.content_text);
        fd.append('category_id', product.category_id);
        fd.append('tags', product.tags);

        fd.append('all_can_see', product.all_can_see);
        fd.append('attorney_can_see', product.attorney_can_see);
        fd.append('chiropractor_can_see', product.chiropractor_can_see);
        fd.append('dentist_can_see', product.dentist_can_see);

        fd.append('optometrist_can_see', product.optometrist_can_see);
        fd.append('osteopathist_can_see', product.osteopathist_can_see);
        fd.append('phd_can_see', product.phd_can_see);
        fd.append('pharmacist_can_see', product.pharmacist_can_see);

        fd.append('physician_can_see', product.physician_can_see);
        fd.append('podiatrist_can_see', product.podiatrist_can_see);
        fd.append('poi_can_see', product.poi_can_see);
        fd.append('veterinarian_can_see', product.veterinarian_can_see);



        $http.post(post_product_edit_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});

        return;

    }

    function publishproduct(productid,status,  onSuccess,onError)
    {

        $http({
            url: publish_product_link,
            method: "PUT",
            params: {
                id: productid,
                status: status,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function deleteproduct(productid,  onSuccess,onError)
    {

        $http({
            url: delete_product_link,
            method: "DELETE",
            params: {
                id: productid,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }


    function getmyproducts(size, poststotal,page,  onSuccess,onError)
    {

        $http({
            url: get_myproducts_link,
            method: "GET",
            params: { size: size,
                per_page: poststotal,
                page:page
            }
        }).
        then(function(response) {


            onSuccess(response);


        }, function(response) {

            onError(response);

        });

    }

    function getmyproduct(productid,  onSuccess,onError)
    {

        $http({
            url: get_myproduct_link,
            method: "GET",
            params: {
                id: productid,

            }
        }).
        then(function(response) {


            onSuccess(response);


        }, function(response) {

            onError(response);

        });

    }


    function getproducts(size, poststotal,page,  onSuccess,onError)
    {

        $http({
            url: get_products_link,
            method: "GET",
            params: { size: size,
                      per_page: poststotal,
                      page:page
            }
        }).
        then(function(response)
        {
            onSuccess(response);

        },
        function(response)
        {
            onError(response);
        });


    }

    function getlatestproducts(size, poststotal,page,product_id, onSuccess,onError)
    {

        $http({
            url: get_products_link,
            method: "GET",
            params: { size: size,
                per_page: poststotal,
                page:page,
                mode:'latest',
                current_product_id:product_id
            }
        }).
        then(function(response)
            {
                onSuccess(response);

            },
            function(response)
            {
                onError(response);
            });


    }


    function getproduct(productid, onSuccess,onError)
    {

        $http({
            url: get_product_link,
            method: "GET",
            params: {
                id: productid,

            }
        }).
        then(function(response) {

            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });



    }




    return {
        getproducts:getproducts,
        getlatestproducts:getlatestproducts,
        getproduct:getproduct,
        getmyproducts:getmyproducts,
        deleteproduct:deleteproduct,
        publishproduct:publishproduct,
        getmyproduct:getmyproduct,
        saveproduct:saveproduct,
        addproduct:addproduct

    }

}]);
