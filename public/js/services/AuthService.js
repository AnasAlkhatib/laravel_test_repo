var fadAuthServices = angular.module('fadAuthServices', [
    'LocalStorageModule'
]);

fadAuthServices.factory('userauthService', ['$http', 'localStorageService', function($http, localStorageService) {



    var logutlink='http://rider.maued.dev/api/v1/auth/logout';
    var registerlink='http:/api/v1/user/register';


    function signupuser(user, onSuccess,onError) {

        var fd = new FormData();
        fd.append('name', user.name);
        fd.append('email', user.email);
        fd.append('password', user.password);
        fd.append('city', user.city);


        $http.post(registerlink, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});






        return;

    }

    function checkIfLoggedIn() {

        if(localStorageService.get('token'))

            return true;

        else
            return false;

    }
    function getloggedinuserinfo()
    {
        var user=JSON.parse(localStorage.getItem('faduser'));
        if(user) return user.proffessional_id;


    }

   /* function getloggedinuser() {

                      if(localStorageService.get('user'))

                          return localStorageService.get('user');

                      else
                          return false;

    }*/


    function logoutuser(){

        $http.post(logutlink,
            {
                token: localStorageService.get('token'),

            }).
        then(function(response) {

            localStorageService.remove('token');
            onSuccess(response);

        }, function(response) {
            localStorageService.remove('token');
            onError(response);

        });


        $auth.logout().then(function() {

            // Remove the authenticated user from local storage
            localStorage.removeItem('user');

            // Flip authenticated to false so that we no longer
            // show UI elements dependant on the user being logged in
            $rootScope.authenticated = false;

            // Remove the current user info from rootscope
            $rootScope.currentUser = null;
        });





    }

    function logout1(){

        $http.post('http://rider.maued.dev/api/v1/auth/logout',
            {
                token: localStorageService.get('token'),

            }).
        then(function(response) {

            localStorageService.remove('token');
            onSuccess(response);

        }, function(response) {
            localStorageService.remove('token');
            onError(response);

        });



    }

    function getCurrentToken(){ alert(localStorageService.get('token'));
        return localStorageService.get('token');
    }

    return {
        checkIfLoggedIn: checkIfLoggedIn,
        signupuser: signupuser,
        logoutuser: logoutuser,
        getCurrentToken: getCurrentToken,
        getloggedinuserinfo:getloggedinuserinfo
    }

}]);
