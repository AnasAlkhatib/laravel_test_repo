var fadspalshBlogsServices = angular.module('fadspalshBlogsServices', [
    'LocalStorageModule'
]);

fadspalshBlogsServices.factory('spalshblogsService', ['$http', 'localStorageService', function($http, localStorageService,$rootScope) {


    var get_blog_posts_link='/api/v1/user/blog/posts';
    var get_blog_post_link='/api/v1/user/blog/get';


   /* function getblogs(size, poststotal, onError)
    {

        $http.get(get_blog_post_link,
            {
                size: size,
                poststotal: poststotal
            }).
        then(function(response) {
              alert(response.status);
            switch (response.status) {

                case 401:alert('Credentials Incorrect');break;
                case 200:return (response.data.data);
            }

          //  return response.data;
           // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }
   */
    function getblogs(size, poststotal,page,  onSuccess,onError) {

        $http({
            url: get_blog_posts_link,
            method: "GET",
            params: { size: size,
                      per_page: poststotal,
                      page:page
            }
        }).
        then(function(response) {
            onSuccess(response);
            // onSuccess(response);
        }, function(response) {
            onError(response);
        });

    }

    function getlatestblogposts(per_page,page,onSuccess,onError){

        $http({
            url:get_blog_posts_link,
            method: "GET",
            params: {
                per_page: per_page,
                page:page,
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });
    }

    function getblogspost(postid, onSuccess,onError) {
        $http({
            url:get_blog_post_link,
            method: "GET",
            params: {
                 id: postid,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });



    }


    return {
        getblogs:getblogs,
        getblogspost:getblogspost,
        getlatestblogposts:getlatestblogposts,
    }

}]);
