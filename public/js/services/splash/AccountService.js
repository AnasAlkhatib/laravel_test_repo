var fadAccountServices = angular.module('fadAccountServices', [
    'LocalStorageModule'
]);

fadAccountServices.factory('accountServices', ['$http', 'localStorageService', function($http, localStorageService,$rootScope) {


    var get_me_main_link='/api/v1/professional/me';
    var put_basic_info_link='/api/v1/professional/me/settings/basic';
    var get_my_profile='/api/v1/professional/me/myprofile';



    function getmyprofile(onSuccess,onError) {


        $http({
            url: get_my_profile,
            method: "GET",
            params: {

            }
        }).
        then(function(response) {
            onSuccess(response);

        }, function(response) {

            onError(response);

        });



    }


    function getme(onSuccess,onError) {

        $http({
            url: get_me_main_link,
            method: "GET",
            params: {

            }
        }).
        then(function(response) {

            //JSON.stringify(response.data.data)
            localStorage.setItem('faduser',JSON.stringify(response.data.data));
            //onSuccess(response);
            //  return response.data;
            //  onSuccess(response);

        }, function(response) {

            localStorage.removeItem('faduser');
           // onError(response);

        });

    }



    function getbasicinfo(articleid,status,  onSuccess,onError) {

        $http({
            url: publish_article_link,
            method: "PUT",
            params: {
                id: articleid,
                status: status,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }
    function savebasicinfo(me, onSuccess,onError) {

        $http({
            url: put_basic_info_link,
            method: "PUT",
            params: {
                   name:me.name,
                   //locale:me.locale,
                   currentposition:me.currentposition,
                   description:me.description,
                   country:me.country,
                   state:me.state,
                   address:me.address,
                   aboutme:me.aboutme
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getprivacy(articleid,status,  onSuccess,onError) {

        $http({
            url: publish_article_link,
            method: "PUT",
            params: {
                id: articleid,
                status: status,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }
    function saveprivacy(articleid,status,  onSuccess,onError) {

        $http({
            url: publish_article_link,
            method: "PUT",
            params: {
                id: articleid,
                status: status,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getbasicinfo1(articleid,status,  onSuccess,onError) {

        $http({
            url: publish_article_link,
            method: "PUT",
            params: {
                id: articleid,
                status: status,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }
    function savebasicinfo1(articleid,status,  onSuccess,onError) {

        $http({
            url: publish_article_link,
            method: "PUT",
            params: {
                id: articleid,
                status: status,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }


    return {
        getme:getme,
        getbasicinfo:getbasicinfo,
        savebasicinfo:savebasicinfo,
        getprivacy:getprivacy,
        saveprivacy:saveprivacy,
        getmyprofile:getmyprofile

    }

}]);
