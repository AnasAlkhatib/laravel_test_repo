var fadQuestionsServices = angular.module('fadQuestionsServices', [
    'LocalStorageModule'
]);

fadQuestionsServices.factory('questionsService', ['$http', 'localStorageService', function($http, localStorageService,$rootScope) {


    var get_myquestion_link='/api/v1/user/question';
    var get_myquestions_link='/api/v1/user/questions';
    var delete_question_link='/api/v1/user/question';
    var publish_question_link='/api/v1/user/question';
    var edit_question_link='/api/v1/user/question?';
    var add_question_link='/api/v1/user/question';


    function publishquestion(articleid,status,  onSuccess,onError) {

        $http({
            url: publish_article_link,
            method: "PUT",
            params: {
                id: articleid,
                status: status,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function postquestion(question, onSuccess,onError) {

        var fd = new FormData();
        fd.append('title', question.title);
        fd.append('tags', question.tags);
        fd.append('content', question.content);
        fd.append('speciality', question.speciality);
        fd.append('total_replies', 0);


        $http.post(add_question_link, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        }).then(
            function success(response){ onSuccess(response);},
            function error(response){  onError(response);});






        return;

    }

    function savequestion(question, onSuccess,onError) {

        var data = $.param({
            'id': question.id,
            'content': question.content,
            'speciality': question.speciality,

        });

        $http.put(edit_question_link+data).then(
            function success(response){ onSuccess(response);},function error(response){  onError(response);});

        return;

    }

    function deletequestion(questionid,  onSuccess,onError) {

        $http({
            url: delete_question_link,
            method: "DELETE",
            params: {
                id: questionid,

            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getmyquestion(questionid,  onSuccess,onError) {

        $http({
            url: get_myquestion_link,
            method: "GET",
            params: {
               id:questionid
            }
        }).
        then(function(response) {


            onSuccess(response);
            //  return response.data;
            // onSuccess(response);

        }, function(response) {

            onError(response);

        });

    }

    function getmyquestions( total,page,  onSuccess,onError) {

        $http({
            url: get_myquestions_link,
            method: "GET",
            params: {
                per_page: total,
                page:page
            }
        }).
        then(function(response) {      onSuccess(response);}, function(response) { onError(response);});

    }







    return {
        postquestion:postquestion,
        publishquestion:publishquestion,
        savequestion:savequestion,
        deletequestion:deletequestion,
        getmyquestions:getmyquestions,
        getmyquestion:getmyquestion
    }

}]);
