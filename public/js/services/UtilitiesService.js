var fadUtilitiesServices = angular.module('fadUtilitiesServices', [ ]);

fadUtilitiesServices.factory('utilitiesServices', ['$sce',    function($sce ) {



    function cleanhtml(text) {
        return $sce.trustAsHtml(text);
    };


    function formatDate(date) {
        var m = moment(date);
        return m.isValid() ? m.format('YYYY-MM-DD') : '';
    }

    function showloginbox() {
        var myEl = angular.element( document.querySelector( '#authmodal' ) ).addClass('is-visible');
        angular.element( document.querySelector( '#cd-login' ) ).addClass('is-visible is-selected');
        angular.element( document.querySelector( '#login_switcher' ) ).addClass('is-selected selected');

    }

    function showhtmltoaster() {
        var myEl = angular.element( document.querySelector( '#authmodal' ) ).addClass('is-visible');
        angular.element( document.querySelector( '#cd-login' ) ).addClass('is-visible is-selected');
        angular.element( document.querySelector( '#login_switcher' ) ).addClass('is-selected selected');

    }
    return {

        cleanhtml:cleanhtml,
        formatDate:formatDate,
        showloginbox:showloginbox,
        showhtmltoaster:showhtmltoaster
    }

}]);
