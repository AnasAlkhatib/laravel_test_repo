/**
 * Created by anas on 4/20/17.
 */

// public/scripts/authController.js

(function() {

    'use strict';

    angular
        .module('fadsplashApp')
        .controller('FooterController', ['$scope','$sce', function($scope, $sce,toastr) {



            $scope.trusthtml = function(text) {
                return $sce.trustAsHtml(text);
            };



        }]);


})();
