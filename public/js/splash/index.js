/**
 * Created by anas on 4/20/17.
 */
// public/scripts/app.js

(function() {

    'use strict';

    angular
        .module('fadsplashApp', ['ngResource','ngRoute','ngMessages', 'ngAnimate', 'toastr', 'ui.router', 'satellizer',
                     'fadAccountServices','fadNavServices','ngMaterial'])
        .config(function($stateProvider, $urlRouterProvider, $authProvider,$httpProvider, $provide,$locationProvider) {

          //  $locationProvider.html5Mode(true);

            /**
             * Helper auth functions
             */
            var skipIfLoggedIn = function($q, $auth) {
                var deferred = $q.defer();
                if ($auth.isAuthenticated()) {
                    deferred.reject();
                } else {
                    deferred.resolve();
                }
                return deferred.promise;
            };

            var loginRequired = function($q, $location, $auth) {
                var deferred = $q.defer();
                if ($auth.isAuthenticated()) {
                    deferred.resolve();
                } else {
                    alert('Sorry you need to login to view this section');
                    $state.go('/')
                    //$location.path('/login');
                    //$location.path('/accessdenied');
                }
                return deferred.promise;
            };

            // use the HTML5 History API
            /*$locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });
            */
            // enable html5Mode for pushstate ('#'-less URLs)
           // $locationProvider.html5Mode(true);
            //$locationProvider.hashPrefix('!');

            function redirectWhenLoggedOut($q, $injector) {



                return {

                    responseError: function(rejection) {

                        // Need to use $injector.get to bring in $state or else we get
                        // a circular dependency error
                        var $state = $injector.get('$state');

                        // Instead of checking for a status code of 400 which might be used
                        // for other reasons in Laravel, we check for the specific rejection
                        // reasons to tell us if we need to redirect to the login state
                        var rejectionReasons = ['token_not_provided', 'token_expired', 'token_absent', 'token_invalid'];

                        // Loop through each rejection reason and redirect to the login
                        // sate if one is encountered
                        angular.forEach(rejectionReasons, function(value, key) {

                            if(rejection.data.error === value) {

                                // If we get a rejection corresponding to one of the reasons
                                // in our array, we know we need to authenticate the user so
                                // we can remove the current user from local storage
                                localStorage.removeItem('user');

                               // $state.go('/', null, {reload: true});
                                // Send the user to the auth state so they can login
                                 $state.go('auth');
                            }
                        });

                        return $q.reject(rejection);
                    }
                }
            }

            // Setup for the $httpInterceptor
            $provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);

            // Push the new factory onto the $http interceptor array
            $httpProvider.interceptors.push('redirectWhenLoggedOut');


            // Satellizer configuration that specifies which API
            // route the JWT should be retrieved from
            $authProvider.loginUrl = 'api/v1/auth/auth';

            // Redirect to the auth state if any other states
            // are requested other than users
            $urlRouterProvider.otherwise('/');

            // Controller
            /*$rootScope.isAuthenticated = function() {
                return $auth.isAuthenticated();
            };

             if (!isAuthenticated())  $state.go('/');*/

            $stateProvider
                .state('/', {
                    url: '/',
                    templateUrl: '../views-splash/indexView.html',
                    controller: 'IndexController as index'
                })
                .state('profile', {
                    url: '/profile/:profileid',
                    templateUrl: '../views/profileView.html',
                    controller: 'profileController as profile',
                    //controller: function($stateParams){ alert($stateParams.profileId);
                     //   $stateParams.profileId
                      //  $stateParams.itemId //*** Exists! ***//
                    //},
                    resolve: {

                        loginRequired: loginRequired
                    },
                  /*  params: {
                        profileId: {
                            dynamic: true,
                            value: null,
                        }
                    }*/
                });

        })
        .run(function($rootScope, $state,$window) {

            var faduser=JSON.parse(localStorage.getItem('faduser'));
            if(faduser)
            {
                $window.location.href = '/expert';
            }

            var normalfaduser= JSON.parse(localStorage.getItem('fadnormaluser'));
            if(normalfaduser)
            {
                $window.location.href = '/user';
            }



           // if($state.current.name === 'profile') alert('asdas');

            // $stateChangeStart is fired whenever the state changes. We can use some parameters
            // such as toState to hook into details about the state as it is changing
            $rootScope.$on('$stateChangeStart', function(event, toState) {

                var faduser=JSON.parse(localStorage.getItem('faduser'));
                if(faduser)
                {
                    $window.location.href = '/expert';
                }

                var normalfaduser= JSON.parse(localStorage.getItem('fadnormaluser'));
                if(normalfaduser)
                {
                    $window.location.href = '/user';
                }


            });
        });
})();