
/**
 * Created by anas on 4/20/17.
 */

// public/scripts/authController.js

(function() {

    'use strict';

    angular
        .module('fadsplashApp')
        .controller('AuthController',  function($scope, $state, $auth, toastr,accountServices ) {

            $scope.login = function() {

                var credentials = {
                    login: $scope.email,
                    password: $scope.password
                }


                $auth.login(credentials)
                    .then(function() {

                         var myEl = angular.element( document.querySelector( '#logintab' ) );
                        myEl.removeClass('is-visible');

                        var myEl = angular.element( document.querySelector( '#logintab' ) );
                        myEl.addClass('cd-user-modal');

                        var myEl = angular.element( document.querySelector( '#authmodal' ) );
                        myEl.removeClass('is-visible');

                        accountServices.getme();


                      //  toastr.success('You have successfully signed in!');

                        $state.go($state.current, null, {reload: true});
                        //$state.go('/');
                    })
                    .catch(function(error) {
                        toastr.error(error.data.message, error.status);
                    });
            };
            $scope.authenticate = function(provider) {
                $auth.authenticate(provider)
                    .then(function() {
                        toastr.success('You have successfully signed in with ' + provider + '!');
                        $state.go('/');
                    })
                    .catch(function(error) {
                        if (error.message) {
                            // Satellizer promise reject error.
                            toastr.error(error.message);
                        } else if (error.data) {
                            // HTTP response error from server
                            toastr.error(error.data.message, error.status);
                        } else {
                            toastr.error(error);
                        }
                    });
            };
        });


})();

