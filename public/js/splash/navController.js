/**
 * Created by anas on 4/20/17.
 */

// public/scripts/authController.js

(function() {

    'use strict';

    angular
        .module('fadsplashApp')
        .controller('NavController', ['$location','navService','$rootScope','$auth',
                               function ($location,navService,$scope,$auth){


            var vm=this;

            $scope.email = "";
            $scope.pwd = "";
            $scope.loggedIn = false;
            $scope.loggingIn = false;

            $scope.showLogin = function () {
                $scope.loggingIn = true;
            };

            $scope.logout = function () {
                // do your logout logic
                $scope.user = null;
                $scope.loggedIn = false;
            };

            $scope.login = function () {
                // do your login logic
                $scope.loggingIn = false;
                $scope.loggedIn = true;
            };


           if(!$auth.isAuthenticated())
           {

               var myEl = angular.element( document.querySelector( '#logintab' ) );
               myEl.removeClass('cd-user-modal');
           }
            else  if($auth.isAuthenticated())
           {
               var myEl = angular.element( document.querySelector( '#logintab' ) );
               myEl.addClass('cd-user-modal');
           }



            vm.getactivestate=function(currentlocation){

              return navService.activeitem(currentlocation,$location.path());
            }

        }])
        .directive("aative", function() {
        return {
            template : "<h1>Made by a directive!</h1>"
        };
        })
        .directive('loginbox', function () {
            return {
                restrict: 'E',
                replace: true,
                //templateUrl:'../views/authView.html',
                 template:'<div class="col-sm-4 col-xs-4 clearfix">' +
                '  <i class="fa fa-search search-btn pull-right"></i>' +
                '  <ul class="topbar-list topbar-log_reg pull-right visible-sm-block visible-md-block visible-lg-block">' +
                '  <li class="cd-log_reg home"><a class="cd-signin"  href="javascript:void(0);">Login1</a></li>' +
                '  <li class="cd-log_reg"><a class="cd-signup" href="javascript:void(0);">Register1</a></li> ' +
                '   </ul>' +
                '</div > ',
               /* template:' <li class="cd-log_reg home"><a class="cd-signin"  href="javascript:void(0);">Login</a></li>'+
                         ' <li class="cd-log_reg"><a class="cd-signup" href="javascript:void(0);">Register</a></li>',*/
                controller: function ($scope) {

                    $scope.submit = function() {
                        $scope.login();
                        $("#loginModal").modal('hide');
                    };

                    $scope.cancel = function() {
                        $scope.loggingIn = false;
                        $("#loginModal").modal('hide');
                    };

                    $scope.$watch('loggingIn', function() {
                        if ($scope.loggingIn) {
                            $("#loginModal").modal('show');
                        };
                    });
                }
            };
        });




    /*function NavController($location,navService) {
       // navService.activeitem();
        navService.activeitem('/',$location.path());
alert($location.path());
       //var tt=  navService.activeitem('/');
      // alert(tt);
        var vm=this;

        vm.getactivestate=function(currentlocation){
            navService.activeitem(currentlocation,$location.path());
        }


    }*/

})();
