<?php

namespace Framework\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmail extends Mailable
{
    use Queueable, SerializesModels;

    private $data_array;
    private $email_template;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data_array,$email_template)
    {
        $this->data_array=$data_array;
        $this->email_template=$email_template;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this ->subject($this->title)
                     ->view($this->email_template)
                     ->with([
                              'data' => $this->data_array,
                            ]);

    }
}
