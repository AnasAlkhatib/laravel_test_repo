<?php

namespace Framework\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegistrationEmail extends Mailable
{
    use Queueable, SerializesModels;

    private $title;
    private $activation_link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($title,$activation_link)
    {
        $this->title=$title;
        $this->activation_link=$activation_link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('emails.test.welcome');
        return $this ->subject($this->title)
                     ->view('emails.account.registrationlink')
                     ->with([
                              'activation_link' => $this->activation_link,
                            ]);

    }
}
