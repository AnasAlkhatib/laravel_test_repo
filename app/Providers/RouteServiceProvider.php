<?php

namespace Framework\Providers;

use Dingo\Api\Routing\Router as DingoApiRouter;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Framework\Http\Controllers';

    /**
     * instance of the Dingo Api router.
     *
     * @var \Dingo\Api\Routing\Router
     */
    //public $apiRouter;


    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //
      // $this->apiRouter = app(DingoApiRouter::class);
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();

       // $this->mapApiRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/web.php');
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
     /*   Route::group([
            'middleware' => 'api',
            'namespace' => $this->namespace,
            'prefix' => 'api',
        ], function ($router) {
            require base_path('routes/api.php');
        });
     */
        //ading dingo  API configuration

        $this->apiRouter->version('v1', function ($router) {

            $router->group([
                'domain' => 'rider.maued.dev',
                'namespace'  => 'App\Services\Api\Http\Controllers',      // Routes Namespace
                'middleware' => 'api',                       // Enable: API Rate Limiting
                'limit'      => env('API_LIMIT'),            // The API limit time.
                'expires'    => env('API_LIMIT_EXPIRES'),  // The API limit expiry time.
                'prefix'    =>'api',
            ], function ($router) {
                require base_path('routes/api.php');
                
                $router->any('/', function () {
                    return response()->json(['Welcome to ' . env('API_NAME') . ' Rider API.']);
                });
                require base_path('routes/api.php');
                // require app_path('Http/rider-api-routes.php');
            });

        });

    }
}
