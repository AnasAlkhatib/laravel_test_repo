<?php

namespace Framework\Providers;

use Dingo\Api\Routing\Router as DingoApiRouter;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as LaravelRouteServiceProvider;
use Illuminate\Routing\Router as LaravelRouter;

class ApiRouteServiceProvider extends LaravelRouteServiceProvider
{
    /**
     * instance of the Dingo Api router.
     *
     * @var \Dingo\Api\Routing\Router
     */
    public $apiRouter;

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param \Illuminate\Routing\Router $router
     */
    public function boot( )
    {
        $this->apiRouter = app(DingoApiRouter::class);
        parent::boot( );
    }

    /**
     * Define the routes for the application.
     */
    public function map()
    {
        $this->apiRouter->version('v1', function ($router) {

            $router->group([
                'domain' => 'fad.dev',
                'namespace'  => 'App\Services\Api\Http\Controllers',      // Routes Namespace
                'middleware' => 'api.throttle',         // Enable: API Rate Limiting
                'limit'      => env('API_LIMIT'),            // The API limit time.
                'expires'    => env('API_LIMIT_EXPIRES'),  // The API limit expiry time.
                'prefix'    =>'api',
            ], function ($router) {
                $router->any('/', function () {
                    return response()->json(['Welcome to ' . env('API_NAME') . ' Laravel Test  API.']);
                });
                //require base_path('routes/api.php');
                require base_path('src/Services/Api/Http/routes.php');
            });

        });
    }
}