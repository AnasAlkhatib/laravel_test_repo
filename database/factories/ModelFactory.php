<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Data\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'phone_number' => str_random(10),
        'device_token' => str_random(10),
    ];
});


//Admin Factory
$factory->define(App\Data\Models\Professional::class, function (Faker\Generator $faker) {
    return [
        'name'     => $faker->name,
        'email'    => $faker->safeEmail,         
        'password' => bcrypt('1234567'),
        'picture_url' => $faker->imageUrl(200, 200),
        'locale' => 'en',
        'timezone' => 'Asia/Riyadh',
        'remember_token' => str_random(10),
    ];
});

