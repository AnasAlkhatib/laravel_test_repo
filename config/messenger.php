<?php

return [

    'user_model' => App\Data\Models\User\User::class,

    'flip_model' => App\Data\Models\Flip\Flip::class,

    'message_model' => Cmgmyr\Messenger\Models\Message::class,

    'participant_model' => Cmgmyr\Messenger\Models\Participant::class,

    'thread_model' => Cmgmyr\Messenger\Models\Thread::class,

    'flipthreads_model' => App\Data\Models\User\FlipThread::class,

    /**
     * Define custom database table names - without prefixes.
     */
    'messages_table' => 'messenger_messages',

    'participants_table' => 'messenger_participants',

    'threads_table' => 'messenger_threads',

    'flipthreads_table' => 'messenger_flipthreads',

    'flipparticipants_table' => 'messenger_flipparticipants',
];
