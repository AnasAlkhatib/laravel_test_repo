<?php

namespace App\Data\Contracts\User\API;

use Prettus\Repository\Contracts\RepositoryInterface;

interface UserRepositoryInterface extends RepositoryInterface
{

}