<?php

namespace App\Data\Transformers\User\API;

use App\Data\Models\User\User;
use App\Foundation\Base\Transformer\Abstracts\Transformer;

/**
 * Class UserTransformer
 * @package  App\Data\Transformers\User\API
 */
class UserTransformer extends Transformer
{
    /**
     * @param User $user
     * @return array
     */



    public function transform(User $user)
    {

        
        return [
            'id' => $user->id,
            'name' => $user->name,
            'token' => $user->token,
            'email' => $user->email,
            'phone' => $user->phone_number,
            'picture_url' =>  ($user->picture_url!='')? $user->picture_url: "assets/img/team/img0.jpg",
            'latitude' => $user->latitude,
            'longitude' => $user->longitude,
            'device_token' => $user->device_token,
            'device_type' => $user->device_type,
            'created_at' => (string)$user->created_at,
            'updated_at' => (string)$user->updated_at,
        ];
    }
}
