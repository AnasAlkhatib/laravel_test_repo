<?php

namespace App\Data\Repositories\Eloquent\Proffessional\API;


use App\Data\Contracts\Proffessional\API\RolesRepositoryInterface;
use App\Foundation\Services\Authorization\Models\Role;
use App\Foundation\Base\Repository\Abstracts\Repository;

/**
 * Class RoleRepository
 * @package App\Data\Repositories\Eloquent\Proffessional\API
 */
class RoleRepository extends Repository implements RolesRepositoryInterface
{

    /**
     * @var array
     */
    protected $fieldSearchable = [

        'name'  => 'like',

    ];

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return Role::class;
    }
}