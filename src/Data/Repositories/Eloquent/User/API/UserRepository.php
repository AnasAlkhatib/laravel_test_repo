<?php

namespace App\Data\Repositories\Eloquent\User\API;


use App\Data\Contracts\User\API\UserRepositoryInterface;
use App\Data\Models\User\User;
use App\Foundation\Base\Repository\Abstracts\Repository;

/**
 * Class UserRepository
 * @package App\Data\Repositories\Eloquent\User\API
 */
class UserRepository extends Repository implements UserRepositoryInterface
{

    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'  => 'like',
        'email' => '=',
    ];

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }
}