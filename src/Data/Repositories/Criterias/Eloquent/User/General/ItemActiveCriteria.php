<?php

namespace App\Data\Repositories\Criterias\Eloquent\User\General;

use App\Foundation\Base\Repository\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class ItemActiveCriteria extends Criteria
{

    private $term;
    private $activevalue;

    /**
     * ItemActiveCriteria constructor.
     * @param $uuid
     */
    public function __construct($term,$activevalue)
    {
       $this->term=$term;
       $this->activevalue=$activevalue;

    }

    /**
     * @param $model
     * @param PrettusRepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->where($this->term,'=',$this->activevalue);


    }

}