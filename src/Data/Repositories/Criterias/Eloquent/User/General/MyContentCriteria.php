<?php

namespace App\Data\Repositories\Criterias\Eloquent\User\General;

use App\Foundation\Base\Repository\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class MyContentCriteria extends Criteria
{

    private $author_field;
    private $author_id;

    /**
     * MyBlogPostsCriteria constructor.
     * @param $uuid
     */
    public function __construct($author_field,$author_id)
    {
        $this->author_field =$author_field ;
        $this->author_id =$author_id ;

    }

    /**
     * @param $model
     * @param PrettusRepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->where($this->author_field, '=', $this->author_id);

    }

}