<?php

namespace App\Data\Repositories\Criterias\Eloquent\User\General;

use App\Foundation\Base\Repository\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class WhereCriteria extends Criteria
{

    private $filedname;
    private $filedvalue;

    /**
     * SearchByCriteria constructor.
     * @param $uuid
     */
    public function __construct($filedname,$filedvalue)
    {
       $this->filedname=$filedname;
       $this->filedvalue=$filedvalue;

    }

    /**
     * @param $model
     * @param PrettusRepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->where($this->filedname, '=', $this->filedvalue);


    }

}