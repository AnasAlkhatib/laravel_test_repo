<?php

namespace App\Data\Repositories\Criterias\Eloquent\User\General;

use App\Foundation\Base\Repository\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class OrderByCriteria extends Criteria
{

    private $orderterm;
    private $ordertype;

    /**
     * SearchByCriteria constructor.
     * @param $uuid
     */
    public function __construct($orderterm,$ordertype='desc')
    {
       $this->orderterm=$orderterm;
       $this->ordertype=$ordertype;

    }

    /**
     * @param $model
     * @param PrettusRepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->orderBy($this->orderterm,$this->ordertype);


    }

}