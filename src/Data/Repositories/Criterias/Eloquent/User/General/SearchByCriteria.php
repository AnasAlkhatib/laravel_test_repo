<?php

namespace App\Data\Repositories\Criterias\Eloquent\User\General;

use App\Foundation\Base\Repository\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

class SearchByCriteria extends Criteria
{

    private $searchterm;
    private $searchvalue;

    /**
     * SearchByCriteria constructor.
     * @param $uuid
     */
    public function __construct($searchterm,$searchvalue)
    {
       $this->searchterm=$searchterm;
       $this->searchvalue=$searchvalue;

    }

    /**
     * @param $model
     * @param PrettusRepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->where($this->searchterm, '=', $this->searchvalue);
    }

}