<?php
/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Laravel Test APIs",
 *     version="1.0.0",
 *   )
 * )
 * @SWG\SecurityScheme(
 *   securityDefinition="jwt",
 *   type="apiKey",
 *   in="header",
 *   name="token"
 * )
 */
