<?php

namespace App\Foundation\Services\Authentication;


use Auth;
use Exception;
use App\Foundation\Services\Authentication\Exceptions\AuthenticationFailedException;
use App\Foundation\Services\Authentication\Exceptions\MissingTokenException;

/**
 * Class RiderApiAuthService
 * @package App\Services\Authentication\Rider
 */
class RiderApiAuthService
{

    private $jwtGuard = null;

    /**
     * RiderApiAuthService constructor.
     */
    public function __construct()
    {
        $this->jwtGuard = 'riders.api';
    }

    /**
     * @param $email
     * @param $password
     * @return mixed
     */
    public function login($loginfield,$login, $password)
    {
        try {
            $token = Auth::guard($this->jwtGuard)->attempt([
                $loginfield => $login,
                'password' => $password,
            ]);

            if (!$token) {
                throw new AuthenticationFailedException();
            }
        } catch (Exception $e) {
            // something went wrong whilst attempting to encode the token
            throw (new AuthenticationFailedException())->debug($e);
        }

        return $token;
    }

    /**
     * @param $user
     * @return mixed
     */
    public function loginFromObject($user)
    {
        $token = $this->generateTokenFromObject($user);
        // inject the token on the model
        $user = $user->injectToken($token);

        return $user;
    }

    /**
     * @param null $token
     * @return mixed
     */
    public function getAuthenticatedUser($token = null)
    {
        if (!$user = Auth::guard($this->jwtGuard)->user()) {
            throw new AuthenticationFailedException('User is not logged in.');
        }

        return $token ? $user->injectToken($token) : $user;
    }

    /**
     * @param $authorizationHeader
     * @return bool
     */
    public function logout($authorizationHeader)
    {

        // remove the `Bearer` string from the header and keep only the token
        $token = str_replace('Bearer', '', $authorizationHeader);

        $ok = Auth::guard($this->jwtGuard)->setToken($token)->invalidate(false);

        if (!$ok) {
            throw new MissingTokenException();
        }

        return true;
    }

    /**
     * @param $user
     * @return mixed
     */
    public function generateTokenFromObject($user)
    {

        try {
            $token = Auth::guard($this->jwtGuard)
                ->login($user);
        } catch (Exception $e) {
            throw (new AuthenticationFailedException())->debug($e);
        }

        return $token;
    }

    /**
     * @param $token
     * @return mixed
     */
    public function invalidateToken($token)
    {
        return Auth::guard($this->jwtGuard)
            ->setToken($token)
            ->invalidate(false);
    }

    /**
     * @return string
     */
    public function refreshToken()
    {
        return Auth::guard($this->jwtGuard)
            ->refresh();
    }
}
