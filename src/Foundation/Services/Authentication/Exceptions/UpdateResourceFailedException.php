<?php

namespace App\Foundation\Services\Authentication\Exceptions;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UpdateResourceFailedException
 * @package App\Services\Authentication\Exceptions
 */
class UpdateResourceFailedException extends ApiException
{

    public $httpStatusCode = Response::HTTP_EXPECTATION_FAILED;

    public $message = 'Failed to Update.';
}
