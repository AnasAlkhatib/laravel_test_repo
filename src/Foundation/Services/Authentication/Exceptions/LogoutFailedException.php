<?php

namespace App\Foundation\Services\Authentication\Exceptions;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class LogoutFailedException
 * @package Sto\Services\Authentication\Exceptions
 */
class LogoutFailedException extends ApiException
{

    public $httpStatusCode = Response::HTTP_BAD_REQUEST;

    public $message = 'Failed to logout!';
}
