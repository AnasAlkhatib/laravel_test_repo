<?php

namespace App\Foundation\Services\Authentication\Exceptions;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AuthenticationFailedException
 * @package Sto\Services\Authentication\Exceptions
 */
class AuthenticationFailedException extends ApiException
{

    public $httpStatusCode = Response::HTTP_UNAUTHORIZED;

    public $message = 'Credentials Incorrect.';
}
