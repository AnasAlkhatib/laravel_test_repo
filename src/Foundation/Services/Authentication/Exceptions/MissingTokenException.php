<?php

namespace App\Foundation\Services\Authentication\Exceptions;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class MissingTokenException
 * @package Sto\Services\Authentication\Exceptions
 */
class MissingTokenException extends ApiException
{

    public $httpStatusCode = Response::HTTP_BAD_REQUEST;

    public $message = 'Token is required.';
}
