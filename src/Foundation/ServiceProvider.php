<?php
namespace App\Foundation;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

 

class ServiceProvider extends BaseServiceProvider
{
    public function register()
    {
        // Register the service providers of your Services here.
        // $this->app->register('full namespace here')
        $this->app->register('App\Services\Api\Providers\ApiServiceProvider');
        $this->app->register('App\Services\Web\Providers\WebServiceProvider');


        //custom service providers
        $this->app->register('App\Foundation\ServiceProvider\HomeServiceProvider');

    }
}
