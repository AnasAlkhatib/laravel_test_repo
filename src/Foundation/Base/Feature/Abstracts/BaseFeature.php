<?php

namespace  App\Foundation\Base\Feature\Abstracts;

use Lucid\Foundation\Feature;
use Dingo\Api\Routing\Helpers as DingoApiHelper;
/**
 * Class BaseFeature
 * @package App\Base\Controller\Abstracts
 */
abstract class BaseFeature extends Feature
{
    use DingoApiHelper;
}
