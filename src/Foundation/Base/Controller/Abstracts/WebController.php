<?php

namespace App\Foundation\Base\Controller\Abstracts;

use App\Foundation\Base\Controller\Contracts\WebControllerInterface;

/**
 * Class WebController
 * @package App\Base\Controller\Abstracts
 */
abstract class WebController extends BaseController implements WebControllerInterface
{

}
