<?php

namespace  App\Foundation\Base\Controller\Abstracts;

use Lucid\Foundation\ServesFeaturesTrait;
use Framework\Http\Controllers\Controller as LaravelController;

/**
 * Class BaseController
 * @package App\Base\Controller\Abstracts
 */
abstract class BaseController extends LaravelController
{
   use ServesFeaturesTrait;
}
