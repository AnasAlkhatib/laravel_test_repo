<?php

namespace App\Foundation\Base\Controller\Abstracts;


use Dingo\Api\Routing\Helpers as DingoApiHelper;
use App\Foundation\Base\Controller\Contracts\ApiControllerInterface;

/**
 * Class ApiController
 * @package App\Base\Controller\Abstracts
 */
abstract class ApiController extends BaseController implements ApiControllerInterface
{

    use DingoApiHelper;
}
