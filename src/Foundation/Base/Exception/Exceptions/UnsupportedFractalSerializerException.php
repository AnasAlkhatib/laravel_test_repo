<?php

namespace App\Foundation\Base\Exception\Exceptions;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

/**
 * Class UnsupportedFractalSerializerException
 * @package App\Base\Exception\Exceptions
 */
class UnsupportedFractalSerializerException extends ApiException
{

    public $httpStatusCode = SymfonyResponse::HTTP_INTERNAL_SERVER_ERROR;
}
