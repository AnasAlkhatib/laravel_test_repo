<?php

namespace App\Foundation\Base\Exception\Exceptions;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

/**
 * Class AuthorizationFailedException
 * @package App\Base\Exception\Exceptions
 */
class AuthorizationFailedException extends ApiException
{

    public $httpStatusCode = SymfonyResponse::HTTP_FORBIDDEN;

    public $message = 'Forbidden';


}
