<?php

namespace App\Foundation\Base\Repository\Abstracts;

use Prettus\Repository\Contracts\CriteriaInterface as PrettusCriteriaInterface;

/**
 * Class Criteria
 * @package App\Base\Repository\Abstracts
 */
abstract class Criteria implements PrettusCriteriaInterface
{

}
