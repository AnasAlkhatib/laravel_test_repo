<?php

namespace App\Foundation\Base\Repository\Criterias\Eloquent;

use App\Foundation\Base\Repository\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

/**
 * Class ThisUserCriteria
 * @package App\Base\Repository\Criterias\Eloquent
 */
class ThisUserCriteria extends Criteria
{

    /**
     * @var $userId int
     */
    private $userId;

    /**
     * ThisUserCriteria constructor.
     * @param $userId
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @param $model
     * @param PrettusRepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->where('user_id', '=', $this->userId);
    }
}
