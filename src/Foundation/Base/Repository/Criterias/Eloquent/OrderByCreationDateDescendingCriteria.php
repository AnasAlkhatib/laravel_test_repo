<?php

namespace App\Foundation\Base\Repository\Criterias\Eloquent;

use App\Foundation\Base\Repository\Abstracts\Criteria;
use Prettus\Repository\Contracts\RepositoryInterface as PrettusRepositoryInterface;

/**
 * Class OrderByCreationDateDescendingCriteria
 * @package App\Base\Repository\Criterias\Eloquent
 */
class OrderByCreationDateDescendingCriteria extends Criteria
{
    /**
     * @param $model
     * @param PrettusRepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, PrettusRepositoryInterface $repository)
    {
        return $model->orderBy('created_at', 'desc');
    }
}
