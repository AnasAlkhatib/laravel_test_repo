<?php

namespace App\Foundation\Base\Model\Abstracts;

use Illuminate\Database\Eloquent\Model as LaravelEloquentModel;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

/**
 * Class Model
 * @package App\Base\Model\Abstracts
 */
abstract class MediaModel extends LaravelEloquentModel implements HasMediaConversions
{

}
