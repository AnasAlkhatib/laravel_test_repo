<?php

namespace App\Foundation\Base\Model\Abstracts;

use Illuminate\Database\Eloquent\Model as LaravelEloquentModel;

/**
 * Class Model
 * @package App\Base\Model\Abstracts
 */
abstract class MediaBaseModel extends LaravelEloquentModel
{

}
