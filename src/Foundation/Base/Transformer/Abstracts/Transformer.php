<?php

namespace App\Foundation\Base\Transformer\Abstracts;

use League\Fractal\TransformerAbstract as FractalTransformerAbstract;

/**
 * Class Transformer
 * @package App\Base\Transformer\Abstracts
 */
abstract class Transformer extends FractalTransformerAbstract
{

}
