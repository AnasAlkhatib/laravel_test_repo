<?php

namespace  App\Foundation\Base\Jobs\Abstracts;

use Lucid\Foundation\Job;
use Illuminate\Bus\Queueable;

/**
 * Class BaseJob
 * @package App\Base\Controller\Abstracts
 */
abstract class BaseJob extends Job
{
    use Queueable;
}
