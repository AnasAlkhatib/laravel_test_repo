<?php
namespace App\Foundation\ServiceProvider;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use App\Data\Repositories\Eloquent\User\API\UserRepository;
use App\Data\Contracts\User\API\UserRepositoryInterface;




class HomeServiceProvider extends BaseServiceProvider
{
    public function register()
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
    }
}
