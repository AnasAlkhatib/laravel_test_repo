<?php
namespace App\Services\Api\Http\Controllers\Professional\Blogs;

use App\Foundation\Base\Controller\Abstracts\ApiController as Controller;
use App\Services\Api\Features\HomePage\GetHomePageFeature;

class GetHomePageController extends Controller
{
    /**
     * @SWG\Get(
     *      path="/home/",
     *      summary="Get Home Page ",
     *      tags={"Home Page "},
     *      description="Get Home Page ",
     *      produces={"application/json"},
     *               @SWG\Parameter(
     *                  name="per_page",
     *                  description="per_page",
     *                  type="integer",
     *                  in="query",
     *                  default="3",
     *              ),
     *      @SWG\Response(
     *          response=200,
     *          description="OK.",
     *      ),
     *     @SWG\Response(
     *         response="401",
     *         description="bad credentials.",
     *     )
     * )
     */

    /**
     * @return \Dingo\Api\Http\Response
     */

    public function handle()
    {
       return $this->serve(GetHomePageFeature::class);

    }


}
