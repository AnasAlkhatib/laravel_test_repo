<?php

/*
|--------------------------------------------------------------------------
| Service Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for this service.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


$router->group(['prefix' => 'api/v1','middleware' => ['api']],
    function ($router) {

    $router->post('/auth/login', [
        'uses' => 'Professional\Auth\LoginController@handle',
    ]);

    });

