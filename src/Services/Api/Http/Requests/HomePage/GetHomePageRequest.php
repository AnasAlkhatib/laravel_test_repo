<?php
namespace App\Services\Api\Http\Requests\HomePage;

use App\Foundation\Base\Request\Abstracts\Request;

/**
 * Class GetHomePageRequest
 * @package App\Services\Api\Requests
 */
class GetHomePageRequest extends Request
{

    public function rules()
    {
        return [

        ];
    }

    public function authorize()
    {
        return true;
    }
}
