<?php
/**
 * @SWG\Definition(
 *      definition="ProfessionalSuccessResponse",
 *      required={"name", "email", "password", "phone_number","proffession"},
 *      @SWG\Property(
 *          property="professional_id",
 *          description="professional_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="proffession",
 *          description="proffession",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="token",
 *          description="token",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="Created at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="Updated at",
 *          type="string",
 *          format="date-time"
 *      ),
 * )
 */