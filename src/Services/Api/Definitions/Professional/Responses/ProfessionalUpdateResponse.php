<?php
/**
 * @SWG\Definition(
 *      definition="ProfessionalUpdateResponse",
 *      @SWG\Property(
 *          property="professional_id",
 *          description="professional_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="locale",
 *          description="locale",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="picture_url",
 *          description="picture_url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="Created at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="Updated at",
 *          type="string",
 *          format="date-time"
 *      ),
 * )
 */