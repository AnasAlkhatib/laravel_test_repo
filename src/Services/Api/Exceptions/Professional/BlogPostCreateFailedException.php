<?php

namespace App\Services\Api\Exceptions\Professional;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BlogPostCreateFailedException.
 *
 * 
 */
class BlogPostCreateFailedException extends ApiException
{

    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed creating new Blog Post.';
}
