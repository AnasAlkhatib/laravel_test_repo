<?php

namespace App\Services\Api\Exceptions\Professional\Blogs;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BlogPostDeleteFailedException.
 *
 * 
 */
class BlogPostDeleteFailedException extends ApiException
{
    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed Deleting Blog Post.';
}
