<?php

namespace App\Services\Api\Exceptions\Professional\Blogs;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class BlogPostupdateFailedException.
 *
 * 
 */
class BlogPostupdateFailedException extends ApiException
{
    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed Updating Blog Post.';
}
