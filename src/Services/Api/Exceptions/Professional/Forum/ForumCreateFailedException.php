<?php

namespace App\Services\Api\Exceptions\Professional\Forum;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ForumCreateFailedException.
 *
 * 
 */
class ForumCreateFailedException extends ApiException
{

    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed creating new Forum.';
}
