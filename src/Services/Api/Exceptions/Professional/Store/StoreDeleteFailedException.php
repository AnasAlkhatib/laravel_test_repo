<?php

namespace App\Services\Api\Exceptions\Professional\Store;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GroupDeleteFailedException.
 *
 * 
 */
class StoreDeleteFailedException extends ApiException
{
    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed Deleting Product.';
}
