<?php

namespace App\Services\Api\Exceptions\Professional\Events;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class EventupdateFailedException.
 *
 * 
 */
class EventupdateFailedException extends ApiException
{
    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed Updating  Event.';
}
