<?php

namespace App\Services\Api\Exceptions\Professional\Account\Experience;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ExperienceCreateFailedException.
 *
 * 
 */
class ExperienceCreateFailedException extends ApiException
{

    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed creating new Experience.';
}
