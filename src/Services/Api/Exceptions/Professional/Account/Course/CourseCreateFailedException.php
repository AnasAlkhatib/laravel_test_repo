<?php

namespace App\Services\Api\Exceptions\Professional\Account\Course;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CourseCreateFailedException.
 *
 * 
 */
class CourseCreateFailedException extends ApiException
{

    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed creating new Course.';
}
