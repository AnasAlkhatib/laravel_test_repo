<?php

namespace App\Services\Api\Exceptions\Professional\Account\Education;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class EducationCreateFailedException.
 *
 * 
 */
class EducationCreateFailedException extends ApiException
{

    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed creating new Education.';
}
