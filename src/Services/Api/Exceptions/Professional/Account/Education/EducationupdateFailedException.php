<?php

namespace App\Services\Api\Exceptions\Professional\Account\Education;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class EducationupdateFailedException.
 *
 * 
 */
class EducationupdateFailedException extends ApiException
{
    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed Updating Education.';
}
