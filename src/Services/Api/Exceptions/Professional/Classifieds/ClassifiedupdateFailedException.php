<?php

namespace App\Services\Api\Exceptions\Professional\Classifieds;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ClassifiedupdateFailedException.
 *
 * 
 */
class ClassifiedupdateFailedException extends ApiException
{
    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed Updating  Classified.';
}
