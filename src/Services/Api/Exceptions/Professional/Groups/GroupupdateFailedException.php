<?php

namespace App\Services\Api\Exceptions\Professional\Groups;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GroupupdateFailedException.
 *
 * 
 */
class GroupupdateFailedException extends ApiException
{
    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed Updating  Group.';
}
