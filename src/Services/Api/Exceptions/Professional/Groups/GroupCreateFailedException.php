<?php

namespace App\Services\Api\Exceptions\Professional\Groups;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GroupCreateFailedException.
 *
 * 
 */
class GroupCreateFailedException extends ApiException
{

    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed creating new Group.';
}
