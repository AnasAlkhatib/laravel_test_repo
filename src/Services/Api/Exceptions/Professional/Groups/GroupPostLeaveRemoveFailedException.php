<?php

namespace App\Services\Api\Exceptions\Professional\Groups;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class GroupPostJoinRemoveFailedException.
 *
 * 
 */
class GroupPostLeaveRemoveFailedException extends ApiException
{

    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed   Removing Group Join.';
}
