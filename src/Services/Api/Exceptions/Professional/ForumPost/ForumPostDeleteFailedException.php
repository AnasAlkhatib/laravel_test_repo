<?php

namespace App\Services\Api\Exceptions\Professional\ForumPost;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ForumPostDeleteFailedException.
 *
 * 
 */
class ForumPostDeleteFailedException extends ApiException
{
    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed Deleting Post.';
}
