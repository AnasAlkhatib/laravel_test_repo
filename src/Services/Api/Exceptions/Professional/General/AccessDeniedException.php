<?php

namespace App\Services\Api\Exceptions\Professional\General;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AccessDeniedException.
 *
 * 
 */
class AccessDeniedException extends ApiException
{

    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'You Are Not Allowed to do this action.';
}
