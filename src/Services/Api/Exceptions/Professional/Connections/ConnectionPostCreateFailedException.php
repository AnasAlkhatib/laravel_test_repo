<?php

namespace App\Services\Api\Exceptions\Professional\Connections;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ConnectionPostCreateFailedException.
 *
 * 
 */
class ConnectionPostCreateFailedException extends ApiException
{

    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed creating new Connection.';
}
