<?php

namespace App\Services\Api\Exceptions\Professional\Articles;

use App\Foundation\Base\Exception\Abstracts\ApiException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ArticleDeleteFailedException.
 *
 * 
 */
class ArticleDeleteFailedException extends ApiException
{
    public $httpStatusCode = Response::HTTP_CONFLICT;

    public $message = 'Failed Deleting Article.';
}
