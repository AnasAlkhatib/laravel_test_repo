<?php
namespace App\Services\Web\Providers;

use View;
use Lang;
use Illuminate\Support\ServiceProvider;
use App\Services\Web\Providers\RouteServiceProvider;
use Illuminate\Translation\TranslationServiceProvider;

class WebServiceProvider extends ServiceProvider
{
    /**
    * Register the Web service provider.
    *
    * @return void
    */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        $this->registerResources();
    }

    /**
     * Register the Web service resource namespaces.
     *
     * @return void
     */
    protected function registerResources()
    {
        // Translation must be registered ahead of adding lang namespaces
        $this->app->register(TranslationServiceProvider::class);

        Lang::addNamespace('web', realpath(__DIR__.'/../resources/lang'));

        View::addNamespace('web', base_path('resources/views/vendor/web'));
        View::addNamespace('web', realpath(__DIR__.'/../resources/views'));
    }
}
