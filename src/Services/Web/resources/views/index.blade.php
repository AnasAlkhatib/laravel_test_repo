<html lang="en" ng-app="riderApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="http://rider.maued.dev/web">
    <title>Rider Maued Application</title>

    <link href="/bower_resources/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="/bower_resources/angular/angular.min.js"></script>
    <script src="/bower_resources/lodash/lodash.js"></script>
    <script src="/bower_resources/angular-route/angular-route.min.js"></script>
    <script src="/bower_resources/angular-local-storage/dist/angular-local-storage.min.js"></script>
    <script src="/bower_resources/restangular/dist/restangular.min.js"></script>

    <script src="/js/app.js"></script>
    <script src="/js/controllers.js"></script>
    <script src="/js/services.js"></script>

    <style>

        li {
            padding-bottom: 8px;
        }

    </style>
</head>

<body>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Welocme to Maued App</h1>
        </div>
    </div>

    <div ng-view></div>
</div>

<script src="/bower_resources/jquery/dist/jquery.min.js"></script>
<script src="/bower_resources/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>
