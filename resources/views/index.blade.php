<!DOCTYPE html >
<!--[if IE 8]> <html lang="en" class="ie8"   > <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"   > <![endif]-->
<!--[if !IE]><!--> <html lang="en"   > <!--<![endif]-->
<head >
    <title>Laravel Test</title>
   <base href="{{ url('/') }}">

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport"    content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    @include('angular.general.header-script')

</head>

<body   class="header-fixed header-fixed-space-v2">

<div class="wrapper">


    <!--=== Header v8 ===-->
    <div class="header-v8 header-sticky">
        <!-- Topbar blog -->
        <div class="blog-topbar">
            <div class="topbar-search-block">
                <div class="container">
                    <form action="">
                        <input type="text" class="form-control" placeholder="Search">
                        <div class="search-close"><i class="icon-close"></i></div>
                    </form>
                </div>
            </div>
            <div class="container">
                <div class="row"  >
                    <div class="col-sm-8 col-xs-8">
                        <div class="topbar-toggler"><span class="fa fa-angle-down"></span></div>
                        <ul class="topbar-list topbar-menu">
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>







                </div><!--/end row-->
            </div><!--/end container-->
        </div>
        <!-- End Topbar blog -->

        <!-- Navbar -->
        <div class="navbar mega-menu" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="res-container">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <div class="navbar-brand">
                        <a href="#">

                        </a>
                    </div>
                </div><!--/end responsive container-->

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-responsive-collapse"  >
                    <div class="res-container">
                        <ul class="nav navbar-nav">


                            <li ng-class="{ active: $nav.getactivestate('/')}"><a ui-sref="/">Home</a></li>

                            <!-- End Main Menu -->
                        </ul>
                    </div><!--/responsive container-->
                </div><!--/navbar-collapse-->
            </div><!--/end contaoner-->
        </div>
        <!-- End Navbar -->
    </div>
    <!--=== End Header v8 ===-->



    <div ui-view ngCloak></div>


@include('angular.layout.footer')






<!-- JS Global Compulsory -->

<script src="/assets/plugins/jquery/jquery-2.2.4.min.js"></script>
<script src="/assets/plugins/jquery/jquery-migrate.min.js"></script>
<script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>

<script src="/assets/js/node_js_modules/loading-overlay/loadingoverlay.min.js"></script>
<script src="/assets/js/node_js_modules/loading-overlay/loadingoverlay_progress.min.js"></script>

<!-- JS Implementing Plugins -->
<script src="/assets/plugins/back-to-top.js"></script>
<script src="/assets/plugins/smoothScroll.js"></script>
<script src="/assets/plugins/counter/waypoints.min.js"></script>
<script src="/assets/plugins/counter/jquery.counterup.min.js"></script>
<script src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script src="/assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script src="/assets/plugins/master-slider/masterslider/masterslider.js"></script>
<script src="/assets/plugins/master-slider/masterslider/jquery.easing.min.js"></script>
<script src="/assets/plugins/modernizr.js"></script>
<script src="/assets/plugins/login-signup-modal-window/js/main.js"></script> <!-- Gem jQuery -->
<!-- JS Customization -->
<script src="/assets/js/custom.js"></script>
<!--[if lt IE 9]>
<script src="/assets/plugins/respond.js"></script>
<script src="/assets/plugins/html5shiv.js"></script>
<script src="/assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->



<!-- Application Dependencies -->
<!-- Node modules   -->
<script src="/node_modules/angular/angular.js"></script>
<script src="/node_modules/angular-ui-router/release/angular-ui-router.js"></script>
<script src="/node_modules/angular-aria/angular-aria.js"></script>
<script src="/node_modules/angular-animate/angular-animate.js"></script>
<script src="/node_modules/angular-messages/angular-messages.js"></script>
<script src="/node_modules/angular-material/angular-material.js"></script>
<script src="/node_modules/angular-local-storage/dist/angular-local-storage.js"></script>
<script src="/node_modules/angular-route/angular-route.js"></script>



<script src="/node_modules/vendor/satellizer.js"></script>
<script src="/node_modules/vendor/angular-resource.js"></script>
<script src="/node_modules/vendor/angular-toastr.tpls.js"></script>
<script src="/node_modules/vendor/angular-sanitize.js"></script>




<script src="/node_modules/ckeditor/ckeditor.js"></script>
<script src="/bower_components/ng-ckeditor/ng-ckeditor.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script> -->




    <!-- JS Page Level -->
    <script src="/assets/js/app.js"></script>
    <script src="/assets/js/plugins/fancy-box.js"></script>
    <script src="/assets/js/plugins/owl-carousel.js"></script>
    <script src="/assets/js/plugins/master-slider-showcase1.js"></script>
    <script src="/assets/js/plugins/master-slider-showcase3.js"></script>




<!-- Application Scripts -->







</body>
</html>