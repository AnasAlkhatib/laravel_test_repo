<!--=== Footer v8 ===-->
<div class="footer-v8"  >
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 column-one md-margin-bottom-50">
                    <a href="#"><img class="footer-logo" src="assets/img/logo-footer.png" alt=""></a>
                    <p class="margin-bottom-20">Welcome toLaravel Test .</p>
                </div>

                <div class="col-md-3 col-sm-6 md-margin-bottom-50">
                    <h2>Main Links</h2>
                    <!-- Footer Lists -->
                    <ul class="footer-lists">
                        <li><i class="fa fa-angle-right"></i><a href="#">Our Features</a></li>
                        <li><i class="fa fa-angle-right"></i><a href="#">Site Map</a></li>
                        <li><i class="fa fa-angle-right"></i><a href="#">Terms and Condition</a></li>
                        <li><i class="fa fa-angle-right"></i><a href="#">Privacy And Policy</a></li>
                        <li><i class="fa fa-angle-right"></i><a href="#">Disclaimer</a></li>
                        <li><i class="fa fa-angle-right"></i><a href="#">Contact us</a>
                        </li>



                    </ul>
                    <!-- End Footer Lists -->
                </div>

                <div class="col-md-3 col-sm-6 md-margin-bottom-50" >







                    <!-- End Latest News -->
                </div>

                <div class="col-md-3 col-sm-6">

                    <h2>Social Networts</h2>
                    <p><strong>Follow Us</strong> If you want to be kept up to date about what’s going on, minute by minute, then search for For All Doctors and give us a follow!</p><br>

                    <!-- Social Icons -->
                    <ul class="social-icon-list margin-bottom-20">
                        <li><a  href="#"><i class="rounded-x fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="rounded-x fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="rounded-x fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="rounded-x fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="rounded-x fa fa-dribbble"></i></a></li>
                    </ul>
                    <!-- End Social Icons -->
                </div>
            </div><!--/end row-->
        </div><!--/end container-->
    </footer>

    <footer class="copyright">
        <div class="container">
            <ul class="list-inline terms-menu">
                <li>Copyright &copy; 2010 - 2018   All Rights Reserved.</li>
            </ul>
        </div><!--/end container-->
    </footer>
</div>
<!--=== End Footer v8 ===-->


</div><!--/wrapper-->



