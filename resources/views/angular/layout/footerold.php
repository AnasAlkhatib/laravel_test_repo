<!--=== Footer v8 ===-->
<div class="footer-v8" ng-controller="FooterController" ng-init="getposts()">
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 column-one md-margin-bottom-50">
                    <a href="#"><img class="footer-logo" src="assets/img/logo-footer.png" alt=""></a>
                    <p class="margin-bottom-20">Welcome to ForAllDoctors.com the largest, secure, private, and FREE professional and social networking website for medical.</p>
                </div>

                <div class="col-md-3 col-sm-6 md-margin-bottom-50">
                    <h2>Main Links</h2>
                    <!-- Footer Lists -->
                    <ul class="footer-lists">
                        <li><i class="fa fa-angle-right"></i><a href="#">Our Features</a></li>
                        <li><i class="fa fa-angle-right"></i><a href="#">Site Map</a></li>
                        <li><i class="fa fa-angle-right"></i><a href="#">Terms and Condition</a></li>
                        <li><i class="fa fa-angle-right"></i><a href="#">Privacy And Policy</a></li>
                        <li><i class="fa fa-angle-right"></i><a href="#">Disclaimer</a></li>
                        <li><i class="fa fa-angle-right"></i><a ui-serf="ContactUs">Contact us</a></li>
                    </ul>
                    <!-- End Footer Lists -->
                </div>

                <div class="col-md-3 col-sm-6 md-margin-bottom-50" >
                    <h2>Latest Blogs</h2>
                    <!-- Latest News -->


                    <div class="latest-news margin-bottom-20"  ng-repeat-start="x in posts">
                        <img src="assets/img/thumb/img-thumb1.jpg" alt="">
                        <h3><a href="#">{{x.title}}</a></h3>
                        <p ng-bind-html="trusthtml(x.content)"></p>
                    </div>

                    <hr ng-repeat-end>



                    <!-- End Latest News -->
                </div>

                <div class="col-md-3 col-sm-6">

                    <h2>Social Networts</h2>
                    <p><strong>Follow Us</strong> If you want to be kept up to date about what’s going on, minute by minute, then search for For All Doctors and give us a follow!</p><br>

                    <!-- Social Icons -->
                    <ul class="social-icon-list margin-bottom-20">
                        <li><a href="#"><i class="rounded-x fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="rounded-x fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="rounded-x fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="rounded-x fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="rounded-x fa fa-dribbble"></i></a></li>
                    </ul>
                    <!-- End Social Icons -->
                </div>
            </div><!--/end row-->
        </div><!--/end container-->
    </footer>

    <footer class="copyright">
        <div class="container">
            <ul class="list-inline terms-menu">
                <li>Copyright &copy; 2010 - 2017 ForAllDoctors.com. Patent Pending. All Rights Reserved.</li>
            </ul>
        </div><!--/end container-->
    </footer>
</div>
<!--=== End Footer v8 ===-->

<div class="cd-user-modal" id="authmodal"> <!-- this is the entire modal form, including the background -->
    <div class="cd-user-modal-container"> <!-- this is the container wrapper -->
        <ul class="cd-switcher">
            <li><a href="javascript:void(0);">Login</a></li>
            <li><a href="javascript:void(0);">Register</a></li>
        </ul>

        <div id="cd-login"   ng-controller="AuthController" > <!-- log in form -->
            <form class="cd-form">

                <p class="fieldset">
                    <label class="image-replace cd-email" for="signin-email">Email</label>
                    <input ng-model="email" class="full-width has-padding has-border" id="signin-email" type="email" placeholder="Email">
                    <span class="cd-error-message">Error message here!</span>
                </p>

                <p class="fieldset">
                    <label class="image-replace cd-password" for="signin-password">Password</label>
                    <input ng-model="password" class="full-width has-padding has-border" id="signin-password" type="text"  placeholder="Password">
                    <a href="javascript:void(0);" class="hide-password">Hide</a>
                    <span class="cd-error-message" ng-if="auth.loginError">Error  </span>
                </p>

                <p class="fieldset">
                    <input type="checkbox" id="remember-me" checked>
                    <label for="remember-me">Remember me</label>
                </p>

                <p class="fieldset">
                    <input class="full-width" ng-click="login()" type="submit" value="Login">
                </p>
            </form>

            <p class="cd-form-bottom-message"><a href="javascript:void(0);" class="margin-right-10">Forgot your password?</a> <a href="javascript:void(0);" class="margin-left-10">Forgot your username?</a></p>
            <!-- <a href="javascript:void(0);" class="cd-close-form">Close</a> -->
        </div> <!-- cd-login -->

        <div id="cd-signup"> <!-- sign up form -->
            <form class="cd-form">

                <p class="fieldset">
                    <label class="image-replace cd-username" for="signup-username">Name</label>
                    <input class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Name">
                    <span class="cd-error-message">Error message here!</span>
                </p>

                <p class="fieldset">
                    <label class="image-replace cd-email" for="signup-email">Username</label>
                    <input class="full-width has-padding has-border" id="signup-email" type="email" placeholder="Username">
                    <span class="cd-error-message">Error message here!</span>
                </p>

                <p class="fieldset">
                    <label class="image-replace cd-email" for="signup-email">E-mail</label>
                    <input class="full-width has-padding has-border" id="signup-email" type="email" placeholder="E-mail">
                    <span class="cd-error-message">Error message here!</span>
                </p>

                <p class="fieldset">
                    <label class="image-replace cd-email" for="signup-email">Confirm E-mail</label>
                    <input class="full-width has-padding has-border" id="signup-email" type="email" placeholder="Confirm E-mail">
                    <span class="cd-error-message">Error message here!</span>
                </p>

                <p class="fieldset">
                    <label class="image-replace cd-password" for="signup-password">Password</label>
                    <input class="full-width has-padding has-border" id="signup-password" type="text"  placeholder="Password">
                    <a href="javascript:void(0);" class="hide-password">Hide</a>
                    <span class="cd-error-message">Error message here!</span>
                </p>

                <p class="fieldset">
                    <label class="image-replace cd-password" for="signup-password">Confirm Password</label>
                    <input class="full-width has-padding has-border" id="signup-password" type="text"  placeholder="Confirm Password">
                    <a href="javascript:void(0);" class="hide-password">Hide</a>
                    <span class="cd-error-message">Error message here!</span>
                </p>

                <p class="fieldset">
                    <input type="checkbox" id="accept-terms">
                    <label for="accept-terms">I agree to the <a href="javascript:void(0);">Terms</a></label>
                </p>

                <p class="fieldset">
                    <input class="full-width has-padding" type="submit" value="Create account">
                </p>
            </form>

            <!-- <a href="javascript:void(0);" class="cd-close-form">Close</a> -->
        </div> <!-- cd-signup -->

        <div id="cd-reset-password"> <!-- reset password form -->
            <p class="cd-form-message">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>

            <form class="cd-form">
                <p class="fieldset">
                    <label class="image-replace cd-email" for="reset-email">E-mail</label>
                    <input class="full-width has-padding has-border" id="reset-email" type="email" placeholder="E-mail">
                    <span class="cd-error-message">Error message here!</span>
                </p>

                <p class="fieldset">
                    <input class="full-width has-padding" type="submit" value="Reset password">
                </p>
            </form>

            <p class="cd-form-bottom-message"><a href="javascript:void(0);">Back to log-in</a></p>
        </div> <!-- cd-reset-password -->
        <a href="javascript:void(0);" class="cd-close-form">Close</a>
    </div> <!-- cd-user-modal-container -->
</div> <!-- cd-user-modal -->
</div><!--/wrapper-->



