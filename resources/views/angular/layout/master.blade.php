<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title>STO iHR - International Human Resources Co.</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="/favicon.ico">

    <!-- Web Fonts -->
    <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="/assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="/assets/css/headers/header-default.css">
    <link rel="stylesheet" href="/assets/css/footers/footer-v3.css">
    <link rel="stylesheet" href="/assets/css/pages/page_log_reg_v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="/assets/plugins/animate.css">
    <link rel="stylesheet" href="/assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="/assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/plugins/image-hover/css/img-hover.css">

    <!-- CSS Page Style -->
    <link rel="stylesheet" href="../assets/css/pages/page_job.css">

    <!-- CSS Theme -->
    <link rel="stylesheet" href="../assets/css/theme-colors/red.css" id="style_color">
    <link rel="stylesheet" href="../assets/css/theme-skins/dark.css">

    <!-- CSS Customization -->
      <link rel="stylesheet" href="../assets/css/pages/aboutus.css">
    <link rel="stylesheet" href="../assets/css/custom.css">
    <script src="/js/vue.js"></script>
  <script src="/js/vue-resource.js"></script>
</head>

<body class="header-fixed header-fixed-space-default">
  
<div class="wrapper">
   @yield('app')
</div><!--/End Wrapepr-->

<!-- JS Global Compulsory -->
<script type="text/javascript" src="../assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="../assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="../assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="../assets/plugins/smoothScroll.js"></script>
<script type="text/javascript" src="../assets/plugins/image-hover/js/touch.js"></script>
<script type="text/javascript" src="../assets/plugins/image-hover/js/modernizr.js"></script>
<!-- JS Customization -->
<script type="text/javascript" src="../assets/js/custom.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="../assets/js/app.js"></script>
<script type="text/javascript">
   jQuery(document).ready(function() {
        App.init();

        
                      $(".fa-star-o").click(function(){
               $(this).css({"color": "#4dacf5"}).removeClass('fa-star-o').addClass('fa-star').addClass('bluebg');
               $("#fav").addClass("active");
               });
    });
</script>
@stack('scripts')


</body>
</html>