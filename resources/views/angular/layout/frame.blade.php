@extends('angulr.layout.master')

@section('app')


    @include('angulr.layout.parts.header')

    @include('angulr.layout.parts.content')

    @include('angulr.layout.parts.footer')

@endsection