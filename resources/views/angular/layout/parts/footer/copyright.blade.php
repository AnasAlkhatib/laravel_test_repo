<div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>
                            Copyright © {{date('Y')}} STO iHR. All Rights Reserved.
                        </p>
                    </div>
                    <!-- Social Links -->
                    <div class="col-md-6">
                        <ul class="social-icons pull-right">
                            <li><a href="https://www.linkedin.com/company/sto-international" target="_blank" data-original-title="Linkedin" class="rounded-x social_linkedin"></a></li>
                            <li><a href="https://www.facebook.com/stosaudi" target="_blank" data-original-title="Facebook" class="rounded-x social_facebook"></a></li>
                            <li><a href="https://twitter.com/sto_sa" target="_blank" data-original-title="Twitter" class="rounded-x social_twitter"></a></li>
                            <li><a href="https://www.instagram.com/sto_sa/" target="_blank" data-original-title="Instagram" class="rounded-x social_instagram"></a></li>
                            <li><a href="https://www.youtube.com/channel/UCFJi6F2rb7MadA_rgUh0fzg" target="_blank" data-original-title="Youtube" class="rounded-x social_youtube"></a></li>
							<li><a href="https://www.snapchat.com/add/sto_sa" target="_blank" data-original-title="Snapchat" class="rounded-x social_snapchat"></a></li>
                        </ul>
                    </div>
                    <!-- End Social Links -->
                </div>
            </div>
        </div><!--/copyright-->