            <!--=== Footer Version 3 ===-->
    <div class="footer-v3">
        <div class="footer">
            <div class="container">
                <div class="row">
                    <!-- About -->
                    <div class="col-md-5 md-margin-bottom-40">
                        <a href="index.html"><img id="logo-footer" class="footer-logo" src="../assets/img/logo2-default.png" alt=""></a>
                        <p>STO iHR is a diversified HR company focused on the total employment life cycle, offering multi-options HR projects, Outsourcing, Employee Couching & Development and Employment services.</p>
                    </div><!--/col-md-5-->
                    <!-- End About -->

                    <div class="col-md-2 md-margin-bottom-20 margin-top-20">
                        <div class="thumb-headline"><h2><a href="index.html">Home</a></h2></div>
                        
                        <div class="thumb-headline"><h2><a href="available-jobs.html">Available Jobs</a></h2></div>
                        
                        <div class="thumb-headline"><h2><a href="search.html">Job Search</a></h2></div>
                        
                        <div class="thumb-headline"><h2><a href="news.html">News</a></h2></div>
                    </div><!--/col-md-2-->

                    <div class="col-md-2 margin-top-20">
                        <div class="thumb-headline"><h2>Company</h2></div>
                        <ul class="list-unstyled simple-list margin-bottom-20">
                            <li><a href="why-sto.html">Why STO?</a></li>
                            <li><a href="about-us.html">About Us</a></li>
                            <li><a href="contact-us.html">Contact Us</a></li>
                        </ul>
                    </div><!--/col-md-2-->

                    <div class="col-md-3 margin-top-20">
                        <div class="thumb-headline"><h2><a href="ihr-mobile-app.html"><i class="fa fa-android"></i> <i class="fa fa-apple"></i> Download iHR Mobile App</a></h2></div>
                        
                        <div class="thumb-headline margin-top-20"><h2><a href="#"><i class="fa fa-building-o"></i> Employers</a></h2></div>
                    </div><!--/col-md-3-->
                </div>
            </div>
        </div><!--/footer-->

        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>
                            Copyright © {{date('Y')}} STO iHR. All Rights Reserved.
                        </p>
                    </div>
                    <!-- Social Links -->
                    <div class="col-md-6">
                        <ul class="social-icons pull-right">
                            <li><a href="https://www.linkedin.com/company/sto-international" target="_blank" data-original-title="Linkedin" class="rounded-x social_linkedin"></a></li>
                            <li><a href="https://www.facebook.com/stosaudi" target="_blank" data-original-title="Facebook" class="rounded-x social_facebook"></a></li>
                            <li><a href="https://twitter.com/sto_sa" target="_blank" data-original-title="Twitter" class="rounded-x social_twitter"></a></li>
                            <li><a href="https://www.instagram.com/sto_sa/" target="_blank" data-original-title="Instagram" class="rounded-x social_instagram"></a></li>
                            <li><a href="https://www.youtube.com/channel/UCFJi6F2rb7MadA_rgUh0fzg" target="_blank" data-original-title="Youtube" class="rounded-x social_youtube"></a></li>
                            <li><a href="https://www.snapchat.com/add/sto_sa" target="_blank" data-original-title="Snapchat" class="rounded-x social_snapchat"></a></li>
                        </ul>
                    </div>
                    <!-- End Social Links -->
                </div>
            </div>
        </div><!--/copyright-->
    </div>