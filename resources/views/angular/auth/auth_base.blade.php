@extends('angulr.layout.master')

@section('app')
<div id="app" >
<div class="wrapper">
    <!--=== Header ===-->
    <div class="header header-sticky">
        <div class="container">
            <!-- Logo -->
            <a class="logo" href="/">
                <img src="../assets/img/logo1-default.png" alt="Logo">
            </a>
            
            <!-- Topbar as Login -->
            <div class="topbar red-color">
                <ul class="loginbar pull-right">
                    <li class="hoverSelector">
                        <i class="fa fa-globe"></i>
                        <a>English</a>
                        <ul class="languages hoverSelectorBlock">
                            <li class="active">
                                <a href="../en/index.html">English <i class="fa fa-check"></i></a>
                            </li>
                            <li><a href="../ar/index.html">Arabic</a></li>
                        </ul>
                    </li>
                    <li > <a href="/login" v-if="!checkAuth">Login</a></li>
                     <li ><a href="/registration" v-if="!checkAuth">Registration</a></li>
                    
                    <li class="hoverSelector" v-if="checkAuth">
                        <img class="rounded-x" v-if="!userInfo.data.pic_file_name" src="../assets/img/user.jpg"  alt="" style="width: 20px;">
                         <img class="rounded-x"  v-if="userInfo.data.pic_file_name" src="@{{userInfo.data.pic_file_name}}" alt="" style="width: 20px;">
                        <i class="fa fa-caret-down"></i>
                        <a class="top-bar-user-name">@{{userInfo.data.full_name}}</a>
                        <ul class="languages hoverSelectorBlock">
                            <li>
                                <a href="/profile"><i class="fa fa-user"></i> My Profile</a> 
                            </li>
                            <li>
                                <a href="/my-jobs"><i class="fa fa-briefcase"></i> My Jobs</a> 
                            </li>
                            <li>
                                <a href="/settings"><i class="fa fa-cog"></i> Settings</a> 
                            </li>
                            <li>
                                <a href="#" v-on:click="logout()"><i class="fa fa-sign-out"></i> Logout</a> 
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- End Topbar as Login -->
            
            
            <!-- Toggle get grouped for better mobile display -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="fa fa-bars"></span>
            </button>
            <!-- End Toggle -->
        </div><!--/end container-->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
            <div class="container">
                <ul class="nav navbar-nav">
                    <!-- Home -->
                    <li class="active">
                        <a href="/">
                            Home
                        </a>
                    </li>
                    <!-- End Home -->
                    
                    <!-- Available Jobs -->
                    <li>
                        <a href="/available-jobs">
                            Available Jobs
                        </a>
                    </li>
                    <!-- End Available Jobs -->
                    
                    <!-- News -->
                    <li>
                        <a href="/news">
                            News
                        </a>
                    </li>
                    <!-- End News -->                    

                   <!-- Clients Pages -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            Clients
                        </a>
                      <ul class="dropdown-menu">
                        <li><a href="availablejobsforspecific.html" class="platinum-background">Platinum Account         2</a></li>
                             <li><a href="availablejobsforspecific.html" class="gold-background">Gold Account                4</a></li>
                          
                               <li><a href="availablejobsforspecific.html" class="silver-background">Silver Account         5</a></li>
                              <li><a href="availablejobsforspecific.html">Partner Account      3</a></li>
                            </ul>
                    </li>
                    <!-- End Company Pages -->
                    
                    <!-- Employers -->
                    <li>
                        <a href="http://ihr.com.sa/employer/login">
                            <i class="fa fa-building-o"></i> Employers
                        </a>
                    </li>
                    <!-- End Employers -->
                    
                    <!-- Mobile App -->
                    <li>
                        <a href="/ihr-mobile-app">
                            <i class="fa fa-android"></i> <i class="fa fa-apple"></i> iHR Mobile App
                        </a>
                    </li>
                    <!-- End Mobile App -->

                    <!-- Search Block -->
                    <li>
                        <i class="search fa fa-search search-btn"></i>
                        <div class="search-open">
                            <div class="input-group animated fadeInDown">
                                <input type="text" class="form-control" placeholder="Job Search" v-model="query">
                                <span class="input-group-btn">
                             <button class="btn-u" type="button" @click="search()">Go</button>
                                </span>
                            </div>
                            <p class="margin-top-10"><a href="/advancedsearch">Advanced Search</a></p>
                        </div>
                    </li>
                    <!-- End Search Block -->
                </ul>
            </div><!--/end container-->
        </div><!--/navbar-collapse-->
    </div>
    <!--=== End Header ===-->

    <!--=== Job Img ===-->
  
    <!--=== End Job Img ===-->

    <!--=== Content Part ===-->
    <div class="container content">


       
            @yield('content')
        
         </div>
        <!--=== Footer Version 3 ===-->
    <div class="footer-v3">
        <div class="footer">
            <div class="container">
                <div class="row">
                    <!-- About -->
                    <div class="col-md-5 md-margin-bottom-40">
                        <a href="index.html"><img id="logo-footer" class="footer-logo" src="../assets/img/logo2-default.png" alt=""></a>
                        <p>STO iHR is a diversified HR company focused on the total employment life cycle, offering multi-options HR projects, Outsourcing, Employee Couching & Development and Employment services.</p>
                    </div><!--/col-md-5-->
                    <!-- End About -->

                    <div class="col-md-2 md-margin-bottom-20 margin-top-20">
                        <div class="thumb-headline"><h2><a href="/">Home</a></h2></div>
                        
                        <div class="thumb-headline"><h2><a href="/available-jobs">Available Jobs</a></h2></div>
                        
                        <div class="thumb-headline"><h2><a href="/search">Job Search</a></h2></div>
                        
                        <div class="thumb-headline"><h2><a href="/news">News</a></h2></div>
                    </div><!--/col-md-2-->

                    <div class="col-md-2 margin-top-20">
                        <div class="thumb-headline"><h2>Company</h2></div>
                        <ul class="list-unstyled simple-list margin-bottom-20">
                            <li><a href="/why-sto">Why STO?</a></li>
                            <li><a href="/about-us">About Us</a></li>
                            <li><a href="/contact-us">Contact Us</a></li>
                        </ul>
                    </div><!--/col-md-2-->

                    <div class="col-md-3 margin-top-20">
                        <div class="thumb-headline"><h2><a href="ihr-mobile-app"><i class="fa fa-android"></i> <i class="fa fa-apple"></i> Download iHR Mobile App</a></h2></div>
                        
                        <div class="thumb-headline margin-top-20"><h2><a href="#"><i class="fa fa-building-o"></i> Employers</a></h2></div>
                    </div><!--/col-md-3-->
                </div>
            </div>
        </div><!--/footer-->
                    @include('angulr.layout.parts.footer.copyright')
          
            
        </div>
    </div>
    </div>
@endsection
@push('scripts')

                  <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.0.1/vue-resource.min.js"></script>
<script src="https://unpkg.com/vue@2.0.3/dist/vue.js"></script>
 <script src="https://unpkg.com/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/vue.resource/1.0.3/vue-resource.min.js"></script>
 <script src="https://cdn.jsdelivr.net/vue/1.0.16/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/vue.resource/0.7.0/vue-resource.min.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.0.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <!--   Core JS Files   -->
  
<script type="text/javascript">
Vue.http.headers.common['Authorization'] = localStorage.getItem('token');
var url=localStorage.getItem('token');
new Vue({
  el: '#app',
  
  data: {
    show: false,
  vacants:[],
    authenticated: false,
    error: false,
    query: '',
    count:'',
    photo :'',
    logOut:'',
     checkAuth: '',
     userInfo:'',


  },
ready: function () {

            this.CheckAuth()
        },
  methods: {
  logout() {
     
               this.$http.post('http://api.testing.ihr.com.sa/logout?token='+ url)
        .then(result => {
         // console.warn(JSON.stringify(result.data.data));
           this.logOut = result.data;
           //console.log('http://api.testing.ihr.com.sa/logout?token='+ url);
            window.location.href = '/' ;
         })
    },
     CheckAuth() {
         this.$http.post('http://api.testing.ihr.com.sa/auth?token='+ url)
        .then(result => {
            this.checkAuth = result.data.code;
            
             this.userInfo = result.data;
             //console.log(this.userInfo.full_name)

             //console.log(this.checkAuth)
                  })
  },
profilePic: function() {
var uuid = localStorage.getItem('ID')
  
         this.$http.get('http://api.testing.ihr.com.sa/seeker/'+ uuid +'/photo', this.photo, function (data) 
        {
            this.photo = data.data.pic_url;
      // this.$set('photo', data.data)
       // console.log(data.data.ID)
       localStorage.setItem('photo', data.data);
})
},


    search: function() {
         this.$http.post('http://api.testing.ihr.com.sa/vacant/search?job_title=' + this.query).then(result => {
         // console.warn(JSON.stringify(result.data.data));
           this.vacants = result.data.data;
           this.count = result.data.count;
         })
    }
    

}

})
</script>
@endpush