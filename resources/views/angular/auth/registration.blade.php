@extends('angulr.auth.auth_base')

@section('content')
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Registration</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li class="active">Registration</li>
            </ul>
        </div><!--/container-->
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
               <div id="app">
                <form   v-on="submit:register" id="sky-form4" class="sky-form">
                <fieldset>
                    <div class="reg-header">
                        <h2>Register a new account</h2>
                        <p>Already Signed Up? Click <a href="/login" class="color-green">Sign In</a> to login your account.</p>
                    </div>

                     <div class="form-group" id="full_name-group">
                    <label>Full Name</label> <span class="color-red">*</span>
                    <input type="text" v-model="newSeeker.full_name"  name='full_name' class="form-control margin-bottom-20">
                    </div>
                     <div class="form-group" id="password-group">
                    <label>Password <span class="color-red">*</span></label>
                    <input type="password"  v-model="newSeeker.password" name='password' class="form-control margin-bottom-20">
                    </div>
                    <div class="form-group" id="mobile-group">
                    <label>Mobile Number <span class="color-red">*</span></label>
                    <input type="text" v-model="newSeeker.mobile" name='mobile' class="form-control margin-bottom-20">
                    </div>
                     <div class="form-group" id="nationality-group">
                    <label>Nationality<span class="color-red">*</span></label>
                    <input type="text" v-model="newSeeker.nationality" name='nationality'  class="form-control margin-bottom-20">
                    </div>
                    <div class="form-group" id="email-group">
                    <label>Email Address <span class="color-red">*</span></label>
                    <input type="text" v-model="newSeeker.email" name='email' class="form-control margin-bottom-20">
                    </div>
                  
                       
                            <section>
                           <div class="form-group">
                                 <label for="exampleSelect1">Gender :</label>
                                 <select class="form-control" id="exampleSelect1" v-model="newSeeker.gender" name='gender'>
                                        <option value="male">Male</option>
                                        <option value="female">Female</option>
                                 </select>
                              </div>
                            </section>
                                

                         <div class="form-group" id="birth_date-group">
                                 <label for="example-date-input" class="col-2 col-form-label">Date Of Birth</label>
                                 <div class="col-10">
                                    <div class="hero-unit">
                                       <input  type="date"  class="form-control" v-model="newSeeker.birth_date" name='birth_date' id="date">
                                    </div>
                                 </div>
                                
                              </div>


                    <div class="row">
                        <div class="col-lg-12 text-right">
                            <button class="btn-u" type="submit">Register</button>
                        </div>
                    </div>
                    </fieldset>
                </form>
                
            </div>
        </div>
      </div>
    </div><!--/container-->
@push('scripts')

 <script src="/js/register.js"></script>
  <script src="https://cdn.jsdelivr.net/vue/1.0.16/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/vue.resource/0.7.0/vue-resource.min.js"></script>
@endpush


 