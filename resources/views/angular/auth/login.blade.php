@extends('angulr.auth.auth_base')
    @section('content')
   <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Login</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li class="active">Login</li>
            </ul>
        </div><!--/container-->
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">
      <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <div id="app">
                <form class="reg-page" id="form" v-on="submit:login">
                    <div class="reg-header">
                        <h2>Login to your account</h2>
                    </div>
                      <div class="form-group" id="email-group">
                    <div class="input-group margin-bottom-20">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="text" v-model="loginData.email" name ="email" placeholder="Email" class="form-control">
                        </div>
                    </div>
                    <div class="form-group" id="password-group">
                    <div class="input-group margin-bottom-20">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input type="password" v-model="loginData.password" name='password'  placeholder="Password" class="form-control">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 checkbox">
                            <label><input type="checkbox"> Stay signed in</label>
                        </div>
                        <div class="col-md-6">
                            <input class="button btn-u pull-right" type="submit" value="Login">
                        </div>
                    </div>

                    <hr>

                    <h4>Forget your Password ?</h4>
                    <p>no worries, <a class="color-green" href="/forget-password">click here</a> to reset your password.</p>
          
          <hr>
          
          <div class="row margin-top-20">
                        <div class="col-md-12">
                            <button class="btn-u btn-u-blue col-md-12" type="submit"><a href="/registration">Not Member?... Register Now</a></button>
                        </div>
                    </div>
            
                </form>

                </div>
            </div>
        </div><!--/row-->
    </div><!--/container-->
    <!--=== End Content Part ===-->


@push('scripts')
 <script src="/js/login.js"></script>
  <script src="https://cdn.jsdelivr.net/vue/1.0.16/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/vue.resource/0.7.0/vue-resource.min.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.0.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
   <script src="https://cdn.jsdelivr.net/vue.validator/2.1.3/vue-validator.min.js"></script>

@endpush
   