@extends('angulr.auth.auth_base')
    @section('content')
<!--=== Breadcrumbs ===-->
 <div id="app">
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">Forget Password</h1>
            <ul class="pull-right breadcrumb">
                <li><a href="/">Home</a></li>
				<li><a href="/login">Login</a></li>
                <li class="active">Forget Password</li>
            </ul>
        </div><!--/container-->
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->

    <!--=== Content Part ===-->
    <div class="container content">
    	<div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <form class="reg-page">
                    <div class="reg-header">
                        <h2>Forget your Password?</h2>
                    </div>

                     <div  class="form-group" id="email-group">

                 

                    <div class="input-group margin-bottom-20" >
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="email" placeholder="Email" class="form-control" v-model="emailData.email">
                    </div>
</div>
                    <div class="row">
                        <div class="col-md-12">
                    <button type="submit" class="btn btn-danger" v-on:click="forgotPassword()">Send</button>
                        </div>
                    </div>
					
                </form>
            </div>
        </div><!--/row-->
    </div><!--/container-->
    </div>
    <!--=== End Content Part ===-->
@push('scripts')
 <script src="https://cdn.jsdelivr.net/vue/1.0.16/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/vue.resource/0.7.0/vue-resource.min.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.0.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
   <script src="https://cdn.jsdelivr.net/vue.validator/2.1.3/vue-validator.min.js"></script>
<script type="text/javascript">
Vue.config.debug = true;
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;

var add = new Vue({


el: '#app',

data: {
   authenticated: false,
  users: [],
  items: [],
  emailData: {
    email: '',
  },
  validation: {
    email: true,
  }
},
http: {
      //root: 'http://api.testing.ihr.com.sa/login',
      headers: {
        Authorization: 'Basic YXBpOnBhc3N3b3Jk'
      }
    },
// validation 
filters: {
  emailValidator: {
    write: function (val) {
      this.validation.email = !!val
      return val
    }
  },
  passwordValidator: {
    write: function (val) {
      this.validation.password = !!val
      return val
    }
  }
},
 
// computed property for form validation state
computed: {
  isValid: function () {
    var valid = true
    for (var key in this.validation) {
      if (!this.validation[key]) {
        valid = false
      }
    }
    return valid
  }
},

// methods
methods: {
  forgotPassword: function () {
   if (this.isValid) {
      console.log("success")
      console.log("email: " + this.emailData.email)

        this.$http.post('http://api.testing.ihr.com.sa/password/email?email=' + this.emailData.email, function (data, status, request) 
        {
   console.log('http://api.testing.ihr.com.sa/password/email?email=', this.emailData)
          console.log("post success")
 
  window.location.href = '/' ;

          }).error(function (data, status, request) {
            //console.log("post failed")
            console.log(data);
        if (! data.success) {   
     if (data.message) {
                $('#email-group').addClass('has-error'); 
                $('#email-group').append('<div class="help-block">' + data.message + '</div>'); 
            }
        } else {   
            $('form').append('<div class="alert alert-success">' + data.message + '</div>');
            alert('success'); 
        }
    })
          
   }else{
      console.log("need edit")
    }
  },
 
   
}
})


</script>
@endpush
