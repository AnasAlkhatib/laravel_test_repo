<!-- JS Global Compulsory -->
<script src="/assets/plugins/jquery/jquery.min.js"></script>
<script src="/assets/plugins/jquery/jquery-migrate.min.js"></script>
<script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script src="/assets/plugins/back-to-top.js"></script>
<script src="/assets/plugins/smoothScroll.js"></script>
<script src="/assets/plugins/counter/waypoints.min.js"></script>
<script src="/assets/plugins/counter/jquery.counterup.min.js"></script>
<script src="/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script src="/assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script src="/assets/plugins/master-slider/masterslider/masterslider.js"></script>
<script src="/assets/plugins/master-slider/masterslider/jquery.easing.min.js"></script>
<script src="/assets/plugins/modernizr.js"></script>
<script src="/assets/plugins/login-signup-modal-window/js/main.js"></script> <!-- Gem jQuery -->
<!-- JS Customization -->
<script src="/assets/js/custom.js"></script>
<!--[if lt IE 9]>
<script src="/assets/plugins/respond.js"></script>
<script src="/assets/plugins/html5shiv.js"></script>
<script src="/assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

<!-- Application Dependencies -->
<script src="/node_modules/angular/angular.js"></script>
<script src="/node_modules/angular-ui-router/release/angular-ui-router.js"></script>
<script src="/node_modules/angular-aria/angular-aria.js"></script>
<script src="/node_modules/angular-animate/angular-animate.js"></script>
<script src="/node_modules/angular-messages/angular-messages.js"></script>
<script src="/node_modules/angular-material/angular-material.js"></script>
<script src="/node_modules/vendor/satellizer.js"></script>
<script src="/node_modules/angular-local-storage/dist/angular-local-storage.js"></script>
<script src="/node_modules/vendor/angular-resource.js"></script>
<script src="/node_modules/vendor/angular-toastr.tpls.js"></script>
<script src="/node_modules/vendor/angular-sanitize.js"></script>
<script src="/node_modules/angular-route/angular-route.js"></script>








<!-- JS Page Level -->
<script src="assets/js/app.js"></script>
<script src="assets/js/plugins/fancy-box.js"></script>
<script src="assets/js/plugins/owl-carousel.js"></script>
<script src="assets/js/plugins/master-slider-showcase1.js"></script>
<script src="assets/js/plugins/master-slider-showcase3.js"></script>
<script>
    jQuery(document).ready(function() {
        App.init();
        App.initCounter();
        FancyBox.initFancybox();
        OwlCarousel.initOwlCarousel();
        OwlCarousel.initOwlCarousel2();

    });
</script>

<!-- Application Scripts -->
<script src="/js/index/index.js"></script>
<script src="/js/index/authController.js"></script>
<script src="/js/index/indexController.js"></script>
<script src="/js/index/homeController.js"></script>
<script src="/js/index/myprofileController.js"></script>
<script src="/js/index/profileController.js"></script>
<script src="/js/index/registerController.js"></script>
<script src="/js/index/userController.js"></script>
<script src="/js/index/classifiedsController.js"></script>

<script src="/js/index/articlesController.js"></script>
<script src="/js/index/articleController.js"></script>
<script src="/js/index/blogsController.js"></script>
<script src="/js/index/eventsController.js"></script>
<script src="/js/index/groupsController.js"></script>
<script src="/js/index/forumsController.js"></script>
<script src="/js/index/storeController.js"></script>
<script src="/js/index/professionalsController.js"></script>


<script src="/js/index/footerController.js"></script>

<script src="/js/index/navController.js"></script>
<script src="/js/services/NavService.js"></script>
<script src="/js/services/index/navbar.js"></script>

<!-- Application Services -->

<script src="/js/services/index/BlogsService.js"></script>
<script src="/js/services/index/ArticlesService.js"></script>
<script src="/js/services/index/ClassifiedsService.js"></script>
<script src="/js/services/index/EventsService.js"></script>
<script src="/js/services/index/GroupsService.js"></script>
<script src="/js/services/index/ProfessionalsService.js"></script>
<script src="/js/services/index/StoreService.js"></script>





