<!-- Favicon -->
<link rel="shortcut icon" href="/images/favicon.ico">

<!-- Web Fonts -->
<link rel='stylesheet' type='text/css' href="http://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700">

<!-- CSS Global Compulsory -->
<link rel="stylesheet" href="/assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/assets/css/style.css">
<link rel="stylesheet" href="/assets/css/user-custom.css">

<!-- CSS Header and Footer -->
<link rel="stylesheet" href="/assets/css/headers/header-v8.css">
<link rel="stylesheet" href="/assets/css/footers/footer-v8.css">

<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="/assets/plugins/animate.css">
<link rel="stylesheet" href="/assets/plugins/line-icons/line-icons.css">
<link rel="stylesheet" href="/assets/plugins/fancybox/source/jquery.fancybox.css">
<link rel="stylesheet" href="/assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/assets/plugins/login-signup-modal-window/css/style.css">
<link rel="stylesheet" href="/assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
<link rel="stylesheet" href="/assets/plugins/master-slider/masterslider/style/masterslider.css">
<link rel="stylesheet" href="/assets/plugins/master-slider/masterslider/skins/black-2/style.css">

<link rel="stylesheet" href="/node_modules/angular-material/angular-material.min.css" >


<link rel="stylesheet" href="/assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
<link rel="stylesheet" href="/assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">
<!--[if lt IE 9]><link rel="stylesheet" href="assets/plugins/sky-forms-pro/skyforms/css/sky-forms-ie8.css"><![endif]-->


<!-- CSS Theme -->
<link rel="stylesheet" href="/assets/css/theme-colors/green.css" id="style_color">
<link rel="stylesheet" href="/assets/css/theme-skins/dark.css">

<!-- CSS Customization -->
<link rel="stylesheet" href="/assets/css/custom.css">

<link rel="stylesheet" href="/assets/css/angular-toastr.css" />




