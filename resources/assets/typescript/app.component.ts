/**
 * Created by anas on 4/18/17.
 */

import { Component } from '@angular/core';
@Component({
    selector: 'my-app',
    template: '<h1>My First Angular App</h1>'
})
export class AppComponent { }
