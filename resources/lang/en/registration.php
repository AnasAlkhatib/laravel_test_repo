<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    //registration
    'activation_email_subject' => 'Welcome To Foralldoctors.com Activation Link',
    'account_already_active'   => 'Account is Already Active',
    'account_successfully_activated '   => 'account successfully activated',
    'activation_code_not_correct'   => 'Activation code not valid',
    'activation_code_generated'   => 'Activation Code successfully generated',


];
