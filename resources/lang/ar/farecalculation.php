<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Push Notifications   Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during sending push notifications 
    | to devices.
    |
    */

    'farecalc'   => "  سعر التوصيلة يحتسب عن طريق ضرب مسافة بالكيلومترات بالسعر للكيلومتر للسيارة حيث
     كل نوع من السيارات لديه سعر خاص يضاف الى السعر فتحة العداد",

    'faredetails' => "the total fare is based on  distance(km) *(care base price+ride base price) (:distance)*(:carbaseprice+:initprice)",

    'Taxi'        => ' سعر التوصيل للتاكسي',
    'Van'         => 'سعر التوصيل للفان',
    'Car'         => ' سعر التوصيل للسيارة',

    'baseprice'   => ' 
                         السعر الابتدائي : ',

    'init_meter'   => ' 
                       فتحة العداد :',

];



